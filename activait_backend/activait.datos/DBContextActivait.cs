﻿using activait.datos.Mapping.parametrizacon;
using activait.datos.Mapping.usuarios;
using activait.datos.Mapping.autorizaciones;
using activait.datos.Mapping.vistas;
using activait.entidades.usuarios;
using activait.entidades.parametrizacion;
using activait.entidades.autorizaciones;
using activait.entidades.vistas;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos
{
    public class DBContextActivait : DbContext
    {
        public DbSet<roles> roles { get; set; }
        public DbSet<departamentos> departamentos { get; set; }
        public DbSet<diagnosticos> diagnosticos { get; set; }
        public DbSet<factores> factores { get; set; }
        public DbSet<personas> personas { get; set; }
        public DbSet<servicios> Servicios { get; set; }
        public DbSet<tipos_monitoreo> tipos_Monitoreos { get; set; }
        public DbSet<users> usuarios { get; set; }
        public DbSet<ciudades> ciudades { get; set; }
        public DbSet<empresas> empresas { get; set; }
        public DbSet<eps> eps { get; set; }
        public DbSet<ips> ips { get; set; }
        public DbSet<sercontratados> sercontratados { get; set; }
        public DbSet<afiliados> afiliados { get; set; }
        public DbSet<siniestros> siniestros { get; set; }
        public DbSet<factoresr> factoresr { get; set; }
        public DbSet<autorizacion> autorizacion { get; set; }
        public DbSet<noaceptados> noaceptados { get; set; }
        public DbSet<monitoreos> monitoreos { get; set; }
        public DbSet<reservas> reservas { get; set; }
        public DbSet<novedades> novedades { get; set; }
        public DbSet<afp> afp { get; set; }
        public DbSet<arl> arl { get; set; }
        public DbSet<vista_personas> vista_personas { get; set; }
        public DbSet<vista_siniestros> vista_siniestros { get; set; }
        public DbSet<testigos> testigos { get; set; }
        public DbSet<vista_autorizaciones> vista_Autorizaciones { get; set; }
        public DbSet<vista_servicios> vista_Servicios { get; set; }
        public DbSet<consultaips> Consultaips { get; set; }

        public DBContextActivait(DbContextOptions<DBContextActivait> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new Rolesmap());
            modelBuilder.ApplyConfiguration(new Departamentosmap());
            modelBuilder.ApplyConfiguration(new Diagnosticosmap());
            modelBuilder.ApplyConfiguration(new Factoresmap());
            modelBuilder.ApplyConfiguration(new Personasmap());
            modelBuilder.ApplyConfiguration(new Serviciosmap());
            modelBuilder.ApplyConfiguration(new tipos_monitoreomap());
            modelBuilder.ApplyConfiguration(new Usuariosmap());
            modelBuilder.ApplyConfiguration(new Ciudadesmap());
            modelBuilder.ApplyConfiguration(new Empresasmap());
            modelBuilder.ApplyConfiguration(new Epsmap());
            modelBuilder.ApplyConfiguration(new Ipsmap());
            modelBuilder.ApplyConfiguration(new Sercontratadosmap());
            modelBuilder.ApplyConfiguration(new Afiliadosmap());
            modelBuilder.ApplyConfiguration(new Siniestrosmap());
            modelBuilder.ApplyConfiguration(new Factoresrmap());
            modelBuilder.ApplyConfiguration(new Autorizacionesmap());
            modelBuilder.ApplyConfiguration(new Noaceptadosmap());
            modelBuilder.ApplyConfiguration(new Monitoreosmap());
            modelBuilder.ApplyConfiguration(new Reservasmap());
            modelBuilder.ApplyConfiguration(new Novedadesmap());
            modelBuilder.ApplyConfiguration(new Arlmap());
            modelBuilder.ApplyConfiguration(new Afpmap());
            modelBuilder.ApplyConfiguration(new vista_personasmap());
            modelBuilder.ApplyConfiguration(new vista_siniestrosmap());
            modelBuilder.ApplyConfiguration(new Testigosmap());
            modelBuilder.ApplyConfiguration(new vista_autorizacionesmap());
            modelBuilder.ApplyConfiguration(new vista_serviciosmap());
            modelBuilder.ApplyConfiguration(new consultaipsmap());
        }
    }
}
