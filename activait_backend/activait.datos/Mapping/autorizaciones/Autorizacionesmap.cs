﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Autorizacionesmap : IEntityTypeConfiguration<autorizacion>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<autorizacion> builder)
        {
            builder.ToTable("autorizaciones2").HasKey(c => c.idautorizaciones);
        }
    }
}
