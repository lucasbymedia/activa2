﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Factoresrmap : IEntityTypeConfiguration<factoresr>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<factoresr> builder)
        {
            builder.ToTable("factoresr").HasKey(c => c.idfactoresr);
        }
    }
}
