﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Monitoreosmap : IEntityTypeConfiguration<monitoreos>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<monitoreos> builder)
        {
            builder.ToTable("monitoreos2").HasKey(c => c.idmonitoreos);
        }
    }
}
