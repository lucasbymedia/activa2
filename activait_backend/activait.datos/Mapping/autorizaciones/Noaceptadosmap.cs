﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Noaceptadosmap : IEntityTypeConfiguration<noaceptados>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<noaceptados> builder)
        {
            builder.ToTable("noaceptados").HasKey(c => c.idnoaceptados);
        }
    }
}
