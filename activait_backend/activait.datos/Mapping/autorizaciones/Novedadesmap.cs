﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;


namespace activait.datos.Mapping.autorizaciones
{
    public class Novedadesmap : IEntityTypeConfiguration<novedades>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<novedades> builder)
        {
            builder.ToTable("novedades").HasKey(c => c.idnovedades);
        }
    }
}
