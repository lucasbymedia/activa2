﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Siniestrosmap : IEntityTypeConfiguration<siniestros>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<siniestros> builder)
        {
            builder.ToTable("siniestros1").HasKey(c => c.idsieniestros);
        }
    }
}
