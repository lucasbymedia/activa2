﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Testigosmap : IEntityTypeConfiguration<testigos>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<testigos> builder)
        {
            builder.ToTable("testigos2").HasKey(c => c.idtestigos);
        }
    }
}
