﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.autorizaciones;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.autorizaciones
{
    public class Testigosmap1 : IEntityTypeConfiguration<testigos1>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<testigos1> builder)
        {
            builder.ToTable("testigos").HasKey(c => c.idtestigos);
        }
    }
}
