﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Afiliadosmap : IEntityTypeConfiguration<afiliados>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<afiliados> builder)
        {
            builder.ToTable("afiliados").HasKey(c => c.idafiliados);
        }
    }
}
