﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Afpmap : IEntityTypeConfiguration<afp>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<afp> builder)
        {
            builder.ToTable("afps").HasKey(c => c.idafps);
        }
    }
}
