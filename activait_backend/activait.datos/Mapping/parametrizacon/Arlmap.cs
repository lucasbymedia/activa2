﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Arlmap : IEntityTypeConfiguration<arl>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<arl> builder)
        {
            builder.ToTable("arl").HasKey(c => c.idarls);
        }
    }
}
