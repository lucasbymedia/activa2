﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;


namespace activait.datos.Mapping.parametrizacon
{
    public class Ciudadesmap : IEntityTypeConfiguration<ciudades>
    {
        public void Configure(EntityTypeBuilder<ciudades> builder)
        {
            builder.ToTable("ciudades")
                .HasKey(c => c.idciudades);

        }
    }
}
