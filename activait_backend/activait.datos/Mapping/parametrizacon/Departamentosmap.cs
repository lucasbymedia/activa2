﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Departamentosmap : IEntityTypeConfiguration<departamentos>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<departamentos> builder)
        {
            builder.ToTable("departamentos").HasKey(c => c.iddepartamentos);

        }
    }
}
