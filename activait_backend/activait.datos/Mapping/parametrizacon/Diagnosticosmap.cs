﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Diagnosticosmap : IEntityTypeConfiguration<diagnosticos>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<diagnosticos> builder)
        {
            builder.ToTable("diagnosticos").HasKey(c => c.iddiagnosticos);

        }
    }
}
