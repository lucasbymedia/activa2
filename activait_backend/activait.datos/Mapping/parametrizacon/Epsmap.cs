﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Epsmap : IEntityTypeConfiguration<eps>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<eps> builder)
        {
            builder.ToTable("eps").HasKey(c => c.ideps);
        }
    }
}
