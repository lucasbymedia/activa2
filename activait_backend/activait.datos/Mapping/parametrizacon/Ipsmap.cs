﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;


namespace activait.datos.Mapping.parametrizacon
{
    public class Ipsmap : IEntityTypeConfiguration<ips>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<ips> builder)
        {
            builder.ToTable("ips").HasKey(c => c.idips);
        }
    }
}
