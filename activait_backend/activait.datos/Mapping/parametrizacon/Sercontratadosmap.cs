﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class Sercontratadosmap : IEntityTypeConfiguration<sercontratados>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<sercontratados> builder)
        {
            builder.ToTable("sercontratados").HasKey(c => c.idsercontratados);
        }
    }
}
