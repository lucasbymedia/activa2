﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.parametrizacion;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.parametrizacon
{
    public class tipos_monitoreomap : IEntityTypeConfiguration<tipos_monitoreo>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<tipos_monitoreo> builder)
        {
            builder.ToTable("tipos_monitoreo").HasKey(c => c.idtipos_monitoreo);
        }
    }

}
