﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.usuarios;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.usuarios
{
    public class Usuariosmap : IEntityTypeConfiguration<users>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<users> builder)
        {
            builder.ToTable("usuarios").HasKey(c => c.idusuarios);

        }

    }
}
