﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.vistas;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.vistas
{
    public class consultaipsmap : IEntityTypeConfiguration<consultaips>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<consultaips> builder)
        {
            builder.ToTable("ips_consulta").HasKey(c => c.idips);
        }
    }

}
