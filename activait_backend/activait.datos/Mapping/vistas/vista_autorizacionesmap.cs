﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.vistas;

using System;
using System.Collections.Generic;
using System.Text;


namespace activait.datos.Mapping.vistas
{
    public class vista_autorizacionesmap : IEntityTypeConfiguration<vista_autorizaciones>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<vista_autorizaciones> builder)
        {
            builder.ToTable("vista_autorizaciones").HasKey(c => c.idautorizaciones);
        }
    }
}
