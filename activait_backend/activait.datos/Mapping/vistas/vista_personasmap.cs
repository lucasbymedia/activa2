﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.vistas;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.vistas
{
    public class vista_personasmap : IEntityTypeConfiguration<vista_personas>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<vista_personas> builder)
        {
            builder.ToTable("vista_afiliados").HasKey(c => c.idpersonas);
        }
    }
}
