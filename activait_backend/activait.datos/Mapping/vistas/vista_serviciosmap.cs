﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.vistas;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.vistas
{
    public class vista_serviciosmap : IEntityTypeConfiguration<vista_servicios>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<vista_servicios> builder)
        {
            builder.ToTable("vista_servicios").HasKey(c => c.idsercontratados);
        }
    }
}
