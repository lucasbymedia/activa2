﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using activait.entidades.vistas;

using System;
using System.Collections.Generic;
using System.Text;

namespace activait.datos.Mapping.vistas
{
    public class vista_siniestrosmap : IEntityTypeConfiguration<vista_siniestros>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<vista_siniestros> builder)
        {
            builder.ToTable("vista_siniestros").HasKey(c => c.idsieniestros);
        }
    }
}
