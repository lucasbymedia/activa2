﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.entidades.autorizaciones
{
    public class autorizacion
    {
        public int idautorizaciones { get; set; }
        [Required]
        public int siniestros_idsiniestos { get; set; }
        [Required]
        public int servicios_idservicios { get; set; }
        [Required]
        public int diagnosticos_iddiagnosticos { get; set; }
        [Required]
        public string aut_ipssolicitante { get; set; }
        [Required]
        public string aut_ipsdireccionada { get; set; }
        public int aut_cuotamoderadora { get; set; }
        public string aut_copago { get; set; }
        public string aut_valorizacion { get; set; }
        public string aut_codigoservicio { get; set; }
        public int aut_valorestimado { get; set; }
        public string aut_estado { get; set; }

        public Nullable<DateTime> aut_fechacreacion { get; set; }
        public Nullable<int> aut_usucreacion { get; set; }
        public Nullable<DateTime> aut_fechamodificacion { get; set; }
        public Nullable<int> aut_usumodificacion { get; set; }
    }
}
