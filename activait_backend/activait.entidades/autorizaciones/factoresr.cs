﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.entidades.autorizaciones
{
    public class factoresr
    {
        public int idfactoresr { get; set; }
        [Required]
        public int factores_idfactores_riesgo { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        public string fac_clase { get; set; }
        [Required]
        public string fac_descripcion { get; set; }
        [Required]
        public string fac_tiempoexpactual { get; set; }
        [Required]
        public string fac_tiempoexpanteri { get; set; }

        public Nullable<DateTime> fac_fechacreacion { get; set; }
        public Nullable<int> fac_usucreacion { get; set; }
        public Nullable<DateTime> fac_fechamodificacion { get; set; }
        public Nullable<int> fac_usumodificacion { get; set; }


    }
}
