﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.entidades.autorizaciones
{
    public class testigos
    {
        public int idtestigos { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        public string tes_tipodoc { get; set; }
        public string tes_documento { get; set; }
        public string tes_nombre { get; set; }
        public string tes_cargo { get; set; }
        public Nullable<DateTime> tes_fechacreacion { get; set; }
        public Nullable<int> tes_usucreacion { get; set; }
        public Nullable<DateTime> tes_fechamodificacion { get; set; }
        public Nullable<int> tes_usumodificacion { get; set; }

    }
}
