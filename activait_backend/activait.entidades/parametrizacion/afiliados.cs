﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.entidades.parametrizacion
{
    public class afiliados
    {
        public int idafiliados { get; set; }
        [Required]
        public int personas_idpersonas { get; set; }
        [Required]
        public int ciudades_idciudades { get; set; }
        [Required]
        public int eps_ideps { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 1)]
        public string afi_tipo { get; set; }
        [Required]
        public string afi_cronico { get; set; }
        [Required]
        public string afi_estado { get; set; }
        [Required]
        public DateTime afi_fechaafiliacion { get; set; }
        [Required]
        public DateTime afi_fechaingreso { get; set; }
        [Required]
        public int afi_ibc { get; set; }
        public string afi_tipovinculacion { get; set; }
    }
}
