﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.parametrizacion
{
    public class afp
    {
        public int idafps { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string afp_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string afp_nombre { get; set; }
    }
}
