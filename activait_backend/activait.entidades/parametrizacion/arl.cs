﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.parametrizacion
{
    public class arl
    {
        public int idarls { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string arl_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string arl_nombre { get; set; }
    }
}
