﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.parametrizacion
{
    public class empresas
    {
        public int idempresas { get; set; }
        [Required]
        public int ciudades_idciudades { get; set; }
        [Required]
        public int arl_idarl { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 1)]
        public string emp_nombre { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 9)]
        public string emp_nit { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 5)]
        public string emp_email { get; set; }
        [Required]
        public string emp_telefono { get; set; }
        public string emp_representante { get; set; }
        public string emp_direccion { get; set; }
        public string emp_actividad { get; set; }
        public string emp_codactividad { get; set; }

    }
}
