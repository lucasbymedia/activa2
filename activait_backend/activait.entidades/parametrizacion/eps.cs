﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.parametrizacion
{
    public class eps
    {
        public int ideps { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string eps_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string eps_nombre { get; set; }
    }
}
