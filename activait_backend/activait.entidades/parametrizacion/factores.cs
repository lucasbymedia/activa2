﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.parametrizacion
{
    public class factores
    {
        public int idfactoresriesgo { get; set; }
        [Required]
        [StringLength(maximumLength: 10, ErrorMessage = "Debe tener máximo 10 caracteres", MinimumLength = 1)]
        public string fac_codigo { get; set; }
    }
}
