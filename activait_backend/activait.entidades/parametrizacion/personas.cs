﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.entidades.parametrizacion
{
    public class personas
    {
        public int idpersonas { get; set; }
        [Required]
        public int empresas_idempresas { get; set; }
        [Required]
        public int afps_idafps { get; set; }
        public string per_tipoid { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string per_numeroid { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 2)]
        public string per_primernombre { get; set; }
        public string per_segundonombre { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 2)]
        public string per_primerapellido { get; set; }
        public string per_segundoapellido { get; set; }
        [Required]
        public DateTime per_nacimiento { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 2)]
        public string per_genero { get; set; }

        public string per_tipovinculacion { get; set; }
        public string per_direccion { get; set; }
        public string per_telefono { get; set; }
        public string per_cargo { get; set; }
    }
}
