﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.usuarios
{
    public class roles
    {
        public int idroles { get; set; }
        [Required]
        [StringLength(maximumLength:100,ErrorMessage ="Debe tener máximo 100 caracteres",MinimumLength =2)]
        public string rol_nombre { get; set; }
    }
}
