﻿using System.ComponentModel.DataAnnotations;

namespace activait.entidades.usuarios
{
    public class users
    {
        public int idusuarios { get; set; }
        [Required]
        public int roles_idroles { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string usu_nombre { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string usu_email { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 6)]
        public string usu_documento { get; set; }
        [StringLength(maximumLength: 12, ErrorMessage = "Debe tener máximo 12 caracteres", MinimumLength = 6)]
        public string usu_telefono { get; set; }
        [Required]
        public byte[] usu_passwordhash { get; set; }
        [Required]
        public byte[] usu_passwordsalt { get; set; }

    }
}
