﻿using System;
using System.Collections.Generic;
using System.Text;

namespace activait.entidades.vistas
{
    public class consultaips
    {
        public int idips { get; set; }
        public int idciudades { get; set; }
        public int idservicios { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
        public DateTime sec_fechainicio { get; set; }
        public DateTime sec_fechafin { get; set; }
        public int sec_valor { get; set; }
        public string sec_estado { get; set; }
        public string ser_codigo { get; set; }
        public string ser_descripcion { get; set; }
        public string ips_nit { get; set; }
        public string ips_nombre { get; set; }
        public string ips_contacto { get; set; }
        public string ips_direccion { get; set; }
        public string ips_telefono { get; set; }
        public string ips_naturaleza { get; set; }
    }
}
