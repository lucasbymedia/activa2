﻿using System;
using System.ComponentModel.DataAnnotations;

namespace activait.entidades.vistas
{
    public class vista_autorizaciones
    {
        public int idpersonas { get; set; }
        public int idciudades { get; set; }
        public int iddepartamentos { get; set; }
        public string dep_codigo { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
        public int ideps { get; set; }
        public string eps_codigo { get; set; }
        public string eps_nombre { get; set; }
        public int idempresas { get; set; }
        public string emp_nombre { get; set; }
        public string emp_nit { get; set; }
        public string per_tipoid { get; set; }
        public string per_numeroid { get; set; }
        public string per_primernombre { get; set; }
        public string per_segundonombre { get; set; }
        public string per_primerapellido { get; set; }
        public string per_segundoapellido { get; set; }
        public DateTime per_nacimiento { get; set; }
        public string per_genero { get; set; }
        public int idafiliados { get; set; }
        public string afi_tipo { get; set; }
        public string afi_cronico { get; set; }
        public string afi_estado { get; set; }
        public DateTime afi_fechaafiliacion { get; set; }
        public DateTime afi_fechaingreso { get; set; }
        public int afi_ibc { get; set; }
        public int idsieniestros { get; set; }
        public DateTime sin_fecha { get; set; }
        public string sin_tipoatep { get; set; }
        public string sin_jornada { get; set; }
        public string sin_laboral { get; set; }
        public string sin_otra { get; set; }
        public string sin_codigo { get; set; }
        public string sin_lugar { get; set; }
        public string sin_tiempolaborprev { get; set; }
        public string sin_causal { get; set; }
        public string sin_muerte { get; set; }
        public string sin_sitio { get; set; }
        public string sin_tipolesion { get; set; }
        public string sin_zona { get; set; }
        public string sin_parteafectada { get; set; }
        public string sin_agente { get; set; }
        public string sin_mecanismo { get; set; }
        public string sin_descripcion { get; set; }

        public int idautorizaciones { get; set; }
        public string aut_ipssolicitante { get; set; }
        public string aut_ipsdireccionada { get; set; }
        public int aut_cuotamoderadora { get; set; }
        public string aut_copago { get; set; }
        public string aut_valorizacion { get; set; }
        public string aut_codigoservicio { get; set; }
        public int aut_valorestimado { get; set; }
        public string aut_estado { get; set; }
        public int idservicios { get; set; }
        public string ser_codigo { get; set; }
        public string ser_descripcion { get; set; }
        public string ser_cups { get; set; }
        public string ser_cum { get; set; }
        public string ser_estado { get; set; }
        public string ser_prioridad { get; set; }
        public string ser_pbs { get; set; }

        public int iddiagnosticos { get; set; }
        public string dia_codigo { get; set; }
        public string dia_descripcion { get; set; }
        public int idips { get; set; }
        public string ips_nit { get; set; }
        public string ips_nombre { get; set; }
        public string ips_contacto { get; set; }
        public string ips_direccion { get; set; }
        public string ips_telefono { get; set; }
        public string ips_naturaleza { get; set; }

        public Nullable <int> valor_reservas { get; set; }
        public Nullable<int> valor_servicios { get; set; }
        public int idciudadeps { get; set; }
    }
}
