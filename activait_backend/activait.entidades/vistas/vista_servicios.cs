﻿using System;
using System.ComponentModel.DataAnnotations;

namespace activait.entidades.vistas
{
    public class vista_servicios
    {
        public int idsercontratados { get; set; }
        public DateTime sec_fechainicio { get; set; }
        public DateTime sec_fechafin { get; set; }
        public int sec_valor { get; set; }
        public string sec_estado { get; set; }
        public string sec_prioridadasign { get; set; }
        public string sec_prioridadcosto { get; set; }
        public int idservicios { get; set; }
        public string ser_codigo { get; set; }
        public string ser_descripcion { get; set; }
        public string ser_cups { get; set; }
        public string ser_cum { get; set; }
        public string ser_estado { get; set; }
        public string ser_prioridad { get; set; }
        public string ser_pbs { get; set; }
        public int idips { get; set; }
        public string ips_nit { get; set; }
        public string ips_nombre { get; set; }
        public string ips_contacto { get; set; }
        public string ips_direccion { get; set; }
        public string ips_telefono { get; set; }
        public string ips_naturaleza { get; set; }
        public int iddepartamentos { get; set; }
        public string dep_codigo { get; set; }
        public string dep_nombre { get; set; }
        public int idciudades { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
    }
}
