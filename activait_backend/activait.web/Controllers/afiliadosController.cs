﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.afiliados;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class afiliadosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public afiliadosController(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<AfiliadosViewModel>> Listar()
        {
            var afil = await _context.afiliados.ToListAsync();

            return afil.Select(c => new AfiliadosViewModel
            {

                idafiliados = c.idafiliados,
                personas_idpersonas = c.personas_idpersonas,
                ciudades_idciudades = c.ciudades_idciudades,
                eps_ideps = c.eps_ideps,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion
            });
        }

        // GET: api/afiliados/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var afil = await _context.afiliados.FindAsync(id);
            if (afil == null)
            {
                return NotFound();
            }
            return Ok(new AfiliadosViewModel
            {
                idafiliados = afil.idafiliados,
                personas_idpersonas = afil.personas_idpersonas,
                ciudades_idciudades = afil.ciudades_idciudades,
                eps_ideps = afil.eps_ideps,
                afi_tipo = afil.afi_tipo,
                afi_cronico = afil.afi_cronico,
                afi_estado = afil.afi_estado,
                afi_fechaafiliacion = afil.afi_fechaafiliacion,
                afi_fechaingreso = afil.afi_fechaingreso,
                afi_ibc = afil.afi_ibc,
                afi_tipovinculacion = afil.afi_tipovinculacion
            });
        }

        // PUT: api/afiliados/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] afiActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idafiliados < 0)
            {
                return BadRequest();
            }

            var afil = await _context.afiliados.FirstOrDefaultAsync(c => c.idafiliados == model.idafiliados);

            if (afil == null)
            {
                return NotFound();
            }

            afil.personas_idpersonas = model.personas_idpersonas;
            afil.ciudades_idciudades = model.ciudades_idciudades;
            afil.eps_ideps = model.eps_ideps;
            afil.afi_tipo = model.afi_tipo;
            afil.afi_cronico = model.afi_cronico;
            afil.afi_estado = model.afi_estado;
            afil.afi_fechaafiliacion = model.afi_fechaafiliacion;
            afil.afi_fechaingreso = model.afi_fechaingreso;
            afil.afi_ibc = model.afi_ibc;
            afil.afi_tipovinculacion = model.afi_tipovinculacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/afiliados/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] afiCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            afiliados afil = new afiliados
            {
                personas_idpersonas = model.personas_idpersonas,
                ciudades_idciudades = model.ciudades_idciudades,
                eps_ideps = model.eps_ideps,
                afi_tipo = model.afi_tipo,
                afi_cronico = model.afi_cronico,
                afi_estado = model.afi_estado,
                afi_fechaafiliacion = model.afi_fechaafiliacion,
                afi_fechaingreso = model.afi_fechaingreso,
                afi_ibc = model.afi_ibc,
                afi_tipovinculacion = model.afi_tipovinculacion,
            };
            _context.afiliados.Add(afil);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/ciudades/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var afil = await _context.afiliados.FindAsync(id);
            if (afil == null)
            {
                return NotFound();
            }

            _context.afiliados.Remove(afil);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(afil);
        }
        private bool ciudadesExists(int id)
        {
            return _context.ciudades.Any(e => e.idciudades == id);
        }
    }
}