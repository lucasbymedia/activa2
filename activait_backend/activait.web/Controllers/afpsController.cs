﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.afp;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class afpsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public afpsController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/afps/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<AfpViewModel>> Listar()
        {
            var af = await _context.afp.ToListAsync();

            return af.Select(c => new AfpViewModel
            {
                idafps = c.idafps,
                afp_codigo = c.afp_codigo,
                afp_nombre = c.afp_nombre
            });
        }

        // GET: api/afps/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var af = await _context.afp.FindAsync(id);
            if (af == null)
            {
                return NotFound();
            }
            return Ok(new AfpViewModel
            {
                idafps = af.idafps,
                afp_codigo = af.afp_codigo,
                afp_nombre = af.afp_nombre
            });
        }

        // PUT: api/afps/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] afpActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idafps < 0)
            {
                return BadRequest();
            }
            var af = await _context.afp.FirstOrDefaultAsync(c => c.idafps == model.idafps);
            if (af == null)
            {
                return NotFound();
            }

            af.afp_codigo = model.afp_codigo;
            af.afp_nombre = model.afp_nombre;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/afps/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] afpCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            afp af = new afp
            {
                afp_codigo = model.afp_codigo,
                afp_nombre = model.afp_nombre
            };

            _context.afp.Add(af);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/afps/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var af = await _context.afp.FindAsync(id);
            if (af == null)
            {
                return NotFound();
            }

            _context.afp.Remove(af);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(af);
        }

        private bool afpExists(int id)
        {
            return _context.afp.Any(e => e.idafps == id);
        }
    }
}