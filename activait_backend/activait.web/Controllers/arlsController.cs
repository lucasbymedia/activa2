﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.arl;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class arlsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public arlsController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/arls/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ArlViewModel>> Listar()
        {
            var ar = await _context.arl.ToListAsync();

            return ar.Select(c => new ArlViewModel
            {
                idarls = c.idarls,
                arl_codigo = c.arl_codigo,
                arl_nombre = c.arl_nombre
            });
        }

        // GET: api/arls/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ar = await _context.arl.FindAsync(id);
            if (ar == null)
            {
                return NotFound();
            }
            return Ok(new ArlViewModel
            {
                idarls = ar.idarls,
                arl_codigo = ar.arl_codigo,
                arl_nombre = ar.arl_nombre
            });
        }

        // PUT: api/arls/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] arlActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idarls < 0)
            {
                return BadRequest();
            }
            var ar = await _context.arl.FirstOrDefaultAsync(c => c.idarls == model.idarls);
            if (ar == null)
            {
                return NotFound();
            }
            ar.arl_codigo = model.arl_codigo;
            ar.arl_nombre = model.arl_nombre;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/arls/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] arlCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            arl ar = new arl
            {
                arl_codigo = model.arl_codigo,
                arl_nombre = model.arl_nombre
            };

            _context.arl.Add(ar);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/arls/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ar = await _context.arl.FindAsync(id);
            if (ar == null)
            {
                return NotFound();
            }

            _context.arl.Remove(ar);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(ar);
        }

        private bool arlExists(int id)
        {
            return _context.arl.Any(e => e.idarls == id);
        }
    }
}