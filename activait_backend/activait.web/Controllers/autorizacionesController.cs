﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.autorizaciones;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class autorizacionesController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public autorizacionesController(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<AutorizacionesViewModel>> Listar()
        {
            var aut = await _context.autorizacion.ToListAsync();

            return aut.Select(c => new AutorizacionesViewModel
            {
                idautorizaciones = c.idautorizaciones,
                siniestros_idsiniestos = c.siniestros_idsiniestos,
                servicios_idservicios = c.servicios_idservicios,
                diagnosticos_iddiagnosticos = c.diagnosticos_iddiagnosticos,
                aut_ipssolicitante = c.aut_ipssolicitante,
                aut_ipsdireccionada = c.aut_ipsdireccionada,
                aut_cuotamoderadora = c.aut_cuotamoderadora,
                aut_copago = c.aut_copago,
                aut_valorizacion = c.aut_valorizacion,
                aut_codigoservicio = c.aut_codigoservicio, 
                aut_valorestimado = c.aut_valorestimado
            });
        }

        // GET: api/autorizaciones/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var aut = await _context.autorizacion.FindAsync(id);
            if (aut == null)
            {
                return NotFound();
            }
            return Ok(new AutorizacionesViewModel
            {
                idautorizaciones = aut.idautorizaciones,
                siniestros_idsiniestos = aut.siniestros_idsiniestos,
                servicios_idservicios = aut.servicios_idservicios,
                diagnosticos_iddiagnosticos = aut.diagnosticos_iddiagnosticos,
                aut_ipssolicitante = aut.aut_ipssolicitante,
                aut_ipsdireccionada = aut.aut_ipsdireccionada,
                aut_cuotamoderadora = aut.aut_cuotamoderadora,
                aut_copago = aut.aut_copago,
                aut_valorizacion = aut.aut_valorizacion,
                aut_codigoservicio = aut.aut_codigoservicio,
                aut_valorestimado = aut.aut_valorestimado
            });
        }

        // GET: api/autorizaciones/consulta0
        [HttpGet("[action]/{idsin}")]
        public async Task<IEnumerable<AutorizacionesViewModel>> consulta0()
        {
            var aut = await _context.autorizacion
                .Where(s => s.aut_estado == "")
                .Take(5)
                .ToListAsync();

            return aut.Select(c => new AutorizacionesViewModel
            {
                idautorizaciones = c.idautorizaciones,
                siniestros_idsiniestos = c.siniestros_idsiniestos,
                servicios_idservicios = c.servicios_idservicios,
                diagnosticos_iddiagnosticos = c.diagnosticos_iddiagnosticos,
                aut_ipssolicitante = c.aut_ipssolicitante,
                aut_ipsdireccionada = c.aut_ipsdireccionada,
                aut_cuotamoderadora = c.aut_cuotamoderadora,
                aut_copago = c.aut_copago,
                aut_valorizacion = c.aut_valorizacion,
                aut_codigoservicio = c.aut_codigoservicio,
                aut_valorestimado = c.aut_valorestimado,
                aut_estado = c.aut_estado
            });
        }


        // PUT: api/autorzaciones/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] autActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idautorizaciones < 0)
            {
                return BadRequest();
            }

            var aut = await _context.autorizacion.FirstOrDefaultAsync(c => c.idautorizaciones == model.idautorizaciones);

            if (aut == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestos = aut.siniestros_idsiniestos;
            model.servicios_idservicios = aut.servicios_idservicios;
            model.diagnosticos_iddiagnosticos = aut.diagnosticos_iddiagnosticos;
            model.aut_ipssolicitante = aut.aut_ipssolicitante;
            model.aut_ipsdireccionada = aut.aut_ipsdireccionada;
            model.aut_cuotamoderadora = aut.aut_cuotamoderadora;
            model.aut_copago = aut.aut_copago;
            model.aut_valorizacion = aut.aut_valorizacion;
            model.aut_codigoservicio = aut.aut_codigoservicio;
            model.aut_valorestimado = aut.aut_valorestimado;
            model.aut_estado = aut.aut_estado;
            model.aut_fechamodificacion = aut.aut_fechamodificacion;
            model.aut_usumodificacion = aut.aut_usumodificacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/autorizaciones/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] autCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            autorizacion aut = new autorizacion
            {
                siniestros_idsiniestos = model.siniestros_idsiniestos,
                servicios_idservicios = model.servicios_idservicios,
                diagnosticos_iddiagnosticos = model.diagnosticos_iddiagnosticos,
                aut_ipssolicitante = model.aut_ipssolicitante,
                aut_ipsdireccionada = model.aut_ipsdireccionada,
                aut_cuotamoderadora = model.aut_cuotamoderadora,
                aut_copago = model.aut_copago,
                aut_valorizacion = model.aut_valorizacion,
                aut_codigoservicio = model.aut_codigoservicio,
                aut_valorestimado = model.aut_valorestimado,
                aut_estado = model.aut_estado,
                aut_fechacreacion = model.aut_fechacreacion,
                aut_usucreacion = model.aut_usucreacion
            };
            _context.autorizacion.Add(aut);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/autorizaciones/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var aut = await _context.autorizacion.FindAsync(id);
            if (aut == null)
            {
                return NotFound();
            }

            _context.autorizacion.Remove(aut);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(aut);
        }

        private bool autorizacionExists(int id)
        {
            return _context.autorizacion.Any(e => e.idautorizaciones == id);
        }
    }
}