﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.ciudades;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ciudadesController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public ciudadesController(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<CiudadViewModel>> Listar()
        {
            var ciu = await _context.ciudades.Include(d => d.Departamento).ToListAsync();

            return ciu.Select(c => new CiudadViewModel
            {
                idciudades = c.idciudades,
                iddepartamentos=c.iddepartamentos,
                departamento = c.Departamento.dep_nombre,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre
            });
        }

        // GET: api/ciudades/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ciudad = await _context.ciudades.Include(d => d.Departamento).SingleOrDefaultAsync(
                c=>c.idciudades==id);
            if (ciudad == null)
            {
                return NotFound();
            }
            return Ok(new CiudadViewModel
            {
                idciudades = ciudad.idciudades,
                iddepartamentos = ciudad.iddepartamentos,
                departamento = ciudad.Departamento.dep_nombre,
                mun_codigo = ciudad.mun_codigo,
                mun_nombre = ciudad.mun_nombre
            });
        }

        // GET: api/ciudades/consulta_dep/5
        [HttpGet("[action]/{iddep}")]
        public async Task<IEnumerable<CiudadViewModel>> consulta_dep([FromRoute] int iddep)
        {
            var ciu = await _context.ciudades
               .Include(d => d.Departamento)
               .Where(s => s.iddepartamentos == iddep)
               .Take(100)
               .ToListAsync();

            return ciu.Select(c => new CiudadViewModel
            {
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                departamento = c.Departamento.dep_nombre,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre
            });
        }

        // PUT: api/ciudades/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] ciuActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idciudades < 0)
            {
                return BadRequest();
            }
            var ciu = await _context.ciudades.FirstOrDefaultAsync(c => c.idciudades == model.idciudades);
            if (ciu == null)
            {
                return NotFound();
            }
            ciu.iddepartamentos = model.iddepartamentos;
            ciu.mun_codigo = model.mun_codigo;
            ciu.mun_nombre = model.mun_nombre;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/ciudades/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] ciuCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ciudades ciudad = new ciudades
            {
                iddepartamentos = model.iddepartamentos,
                mun_codigo = model.mun_codigo,
                mun_nombre = model.mun_nombre
            };
            _context.ciudades.Add(ciudad);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/ciudades/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ciudad = await _context.ciudades.FindAsync(id);
            if (ciudad == null)
            {
                return NotFound();
            }

            _context.ciudades.Remove(ciudad);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(ciudad);
        }
        private bool ciudadesExists(int id)
        {
            return _context.ciudades.Any(e => e.idciudades == id);
        }
    }
}