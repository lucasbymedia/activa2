﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.vistas;
using activait.web.Models.vistas.consultaips;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class consultaipsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public consultaipsController(DBContextActivait context)
        {
            _context = context;
        }


        // GET: api/consultaips/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<consultaipsViewModel>> Listar()
        {
            var ips = await _context.Consultaips.ToListAsync();

            return ips.Select(c => new consultaipsViewModel
            {
                idips = c.idips,
                idciudades = c.idciudades,
                idservicios = c.idservicios,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                sec_fechainicio = c.sec_fechainicio,
                sec_fechafin = c.sec_fechafin,
                sec_valor = c.sec_valor,
                sec_estado = c.sec_estado,
                ser_codigo = c.ser_codigo,
                ser_descripcion = c.ser_descripcion,
                ips_nombre = c.ips_nombre,
                ips_nit = c.ips_nit,
                ips_contacto = c.ips_contacto,
                ips_direccion = c.ips_direccion,
                ips_naturaleza = c.ips_naturaleza,
                ips_telefono = c.ips_telefono
            });
        }

        // GET: api/consultaips/Consulta/5
        [HttpGet("[action]/{idciudad}/{idservicio}")]
        public async Task<IEnumerable<consultaipsViewModel>> Consulta(int idciudad, int idservicio)
        {
            var ips = await _context.Consultaips
               .Where(s => s.idciudades == idciudad)
               .Where(s => s.idservicios == idservicio)
               .OrderByDescending(s => s.ips_nombre)
               .Take(100)
               .ToListAsync();

            return ips.Select(c => new consultaipsViewModel
            {
                idips = c.idips,
                idciudades = c.idciudades,
                idservicios = c.idservicios,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                sec_fechainicio = c.sec_fechainicio,
                sec_fechafin = c.sec_fechafin,
                sec_valor = c.sec_valor,
                sec_estado = c.sec_estado,
                ser_codigo = c.ser_codigo,
                ser_descripcion = c.ser_descripcion,
                ips_nombre = c.ips_nombre,
                ips_nit = c.ips_nit,
                ips_contacto = c.ips_contacto,
                ips_direccion = c.ips_direccion,
                ips_naturaleza = c.ips_naturaleza,
                ips_telefono = c.ips_telefono
            });
        }

        private bool consultaipsExists(int id)
        {
            return _context.Consultaips.Any(e => e.idips == id);
        }
    }
}