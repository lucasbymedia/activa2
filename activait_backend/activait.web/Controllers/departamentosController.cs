﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.departamentos;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class departamentosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public departamentosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/departamentos/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<DepartamentosVierwModel>> Listar()
        {
            var departamento = await _context.departamentos.ToListAsync();

            return departamento.Select(c => new DepartamentosVierwModel
            {
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                dep_nombre=c.dep_nombre
            });
        }

        // GET: api/departamentos/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var departamento = await _context.departamentos.FindAsync(id);
            if (departamento == null)
            {
                return NotFound();
            }
            return Ok(new DepartamentosVierwModel
            {
                iddepartamentos = departamento.iddepartamentos,
                dep_codigo = departamento.dep_codigo,
                dep_nombre = departamento.dep_nombre
            });
        }

        // PUT: api/departamentos/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] depActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.iddepartamentos < 0)
            {
                return BadRequest();
            }
            var departamento = await _context.departamentos.FirstOrDefaultAsync(c => c.iddepartamentos == model.iddepartamentos);
            if (departamento == null)
            {
                return NotFound();
            }
            departamento.dep_codigo = model.dep_codigo;
            departamento.dep_nombre = model.dep_nombre;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/departamentos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] depCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            departamentos departamentos = new departamentos
            {
                dep_codigo = model.dep_codigo,
                dep_nombre= model.dep_nombre
            };

            _context.departamentos.Add(departamentos);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/departamentos/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var departamentos = await _context.departamentos.FindAsync(id);
            if (departamentos == null)
            {
                return NotFound();
            }

            _context.departamentos.Remove(departamentos);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(departamentos);
        }

        private bool departamentosExists(int id)
        {
            return _context.departamentos.Any(e => e.iddepartamentos == id);
        }
    }
}