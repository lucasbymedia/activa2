﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.diagnosticos;

namespace activait.web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class diagnosticosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public diagnosticosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/departamentos/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<DiagnosticosViewModel>> Listar()
        {
            var diagnostico = await _context.diagnosticos.ToListAsync();

            return diagnostico.Select(c => new DiagnosticosViewModel
            {
                iddiagnosticos = c.iddiagnosticos,
                dia_codigo = c.dia_codigo,
                dia_descripcion = c.dia_descripcion
            });
        }

        // GET: api/departamentos/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var diagnostico = await _context.diagnosticos.FindAsync(id);
            if (diagnostico == null)
            {
                return NotFound();
            }
            return Ok(new DiagnosticosViewModel
            {
                iddiagnosticos = diagnostico.iddiagnosticos,
                dia_codigo = diagnostico.dia_codigo,
                dia_descripcion = diagnostico.dia_descripcion
            });
        }

        // PUT: api/departamentos/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] diaActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.iddiagnosticos < 0)
            {
                return BadRequest();
            }
            var diagnostico = await _context.diagnosticos.FirstOrDefaultAsync(c => c.iddiagnosticos == model.iddiagnosticos);
            if (diagnostico == null)
            {
                return NotFound();
            }
            diagnostico.dia_codigo = model.dia_codigo;
            diagnostico.dia_descripcion = model.dia_descripcion;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/departamentos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] diaCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            diagnosticos diagnosticos = new diagnosticos
            {
                dia_codigo = model.dia_codigo,
                dia_descripcion = model.dia_descripcion
            };

            _context.diagnosticos.Add(diagnosticos);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/departamentos/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var diagnosticos = await _context.diagnosticos.FindAsync(id);
            if (diagnosticos == null)
            {
                return NotFound();
            }

            _context.diagnosticos.Remove(diagnosticos);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(diagnosticos);
        }



        private bool diagnosticosExists(int id)
        {
            return _context.diagnosticos.Any(e => e.iddiagnosticos == id);
        }
    }
}