﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.empresas;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class empresasController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public empresasController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/empresas/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<EmpresasViewModel>> Listar()
        {
            var empresa = await _context.empresas.ToListAsync();

            return empresa.Select(c => new EmpresasViewModel
            {
                idempresas = c.idempresas,
                ciudades_idciudades = c.ciudades_idciudades,
                arl_idarl = c.arl_idarl,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_direccion = c.emp_direccion,
                emp_representante = c.emp_representante, 
                emp_actividad = c.emp_actividad,
                emp_codactividad = c.emp_codactividad
            });
        }

        // GET: api/empresas/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var empresa = await _context.empresas.FindAsync(id);
            if (empresa == null)
            {
                return NotFound();
            }
            return Ok(new EmpresasViewModel
            {
                idempresas = empresa.idempresas,
                ciudades_idciudades = empresa.ciudades_idciudades,
                arl_idarl = empresa.arl_idarl,
                emp_nombre = empresa.emp_nombre,
                emp_nit = empresa.emp_nit,
                emp_email = empresa.emp_email,
                emp_telefono = empresa.emp_telefono,
                emp_direccion = empresa.emp_direccion,
                emp_representante = empresa.emp_representante,
                emp_actividad = empresa.emp_actividad,
                emp_codactividad = empresa.emp_codactividad
            });
        }

        // PUT: api/empresas/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] empActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idempresas < 0)
            {
                return BadRequest();
            }

            var empresa = await _context.empresas.FirstOrDefaultAsync(c => c.idempresas == model.idempresas);

            if (empresa == null)
            {
                return NotFound();
            }

            empresa.ciudades_idciudades = model.ciudades_idciudades;
            empresa.arl_idarl = model.arl_idarl;
            empresa.emp_nombre = model.emp_nombre;
            empresa.emp_nit = model.emp_nit;
            empresa.emp_email = model.emp_email;
            empresa.emp_telefono = model.emp_telefono;
            empresa.emp_direccion = model.emp_direccion;
            empresa.emp_representante = model.emp_representante;
            empresa.emp_actividad = model.emp_actividad;
            empresa.emp_codactividad = model.emp_codactividad;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/empresas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] empCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            empresas empresa = new empresas
            {
                ciudades_idciudades = model.ciudades_idciudades,
                arl_idarl = model.arl_idarl,
                emp_nombre = model.emp_nombre,
                emp_nit = model.emp_nit,
                emp_email = model.emp_email,
                emp_telefono = model.emp_telefono,
                emp_direccion = model.emp_direccion,
                emp_representante = model.emp_representante,
                emp_actividad = model.emp_actividad,
                emp_codactividad = model.emp_codactividad
            };
            _context.empresas.Add(empresa);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/empresas/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var empresa = await _context.empresas.FindAsync(id);
            if (empresa == null)
            {
                return NotFound();
            }

            _context.empresas.Remove(empresa);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(empresa);
        }

        private bool empresasExists(int id)
        {
            return _context.empresas.Any(e => e.idempresas == id);
        }
    }
}