﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.eps;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class epsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public epsController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/eps/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<EpsViewModel>> Listar()
        {
            var ep = await _context.eps.ToListAsync();

            return ep.Select(c => new EpsViewModel
            {
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre
            });
        }

        // GET: api/eps/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ep = await _context.eps.FindAsync(id);
            if (ep == null)
            {
                return NotFound();
            }
            return Ok(new EpsViewModel
            {
                ideps = ep.ideps,
                eps_codigo = ep.eps_codigo,
                eps_nombre = ep.eps_nombre
            });
        }

        // PUT: api/eps/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] epsActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.ideps < 0)
            {
                return BadRequest();
            }
            var ep = await _context.eps.FirstOrDefaultAsync(c => c.ideps == model.ideps);
            if (ep == null)
            {
                return NotFound();
            }
            ep.eps_codigo = model.eps_codigo;
            ep.eps_nombre = model.eps_nombre;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/eps/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] epsCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            eps ep = new eps
            {
                eps_codigo = model.eps_codigo,
                eps_nombre = model.eps_nombre
            };

            _context.eps.Add(ep);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/eps/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ep = await _context.eps.FindAsync(id);
            if (ep == null)
            {
                return NotFound();
            }

            _context.eps.Remove(ep);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(ep);
        }

        private bool epsExists(int id)
        {
            return _context.eps.Any(e => e.ideps == id);
        }
    }
}