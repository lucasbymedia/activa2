﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.factores;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class factoressController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public factoressController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/factoress/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<FactoresViewModel>> Listar()
        {
            var factor = await _context.factores.ToListAsync();

            return factor.Select(c => new FactoresViewModel
            {
                idfactoresriesgo = c.idfactoresriesgo,
                fac_codigo = c.fac_codigo
            });
        }

        // GET: api/factoress/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var factor = await _context.factores.FindAsync(id);
            if (factor == null)
            {
                return NotFound();
            }
            return Ok(new FactoresViewModel
            {
                idfactoresriesgo = factor.idfactoresriesgo,
                fac_codigo = factor.fac_codigo
            });
        }

        // PUT: api/factoress/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] facActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idfactoresriesgo < 0)
            {
                return BadRequest();
            }
            var factor = await _context.factores.FirstOrDefaultAsync(c => c.idfactoresriesgo == model.idfactoresriesgo);
            if (factor == null)
            {
                return NotFound();
            }
            factor.fac_codigo = model.fac_codigo;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/factoress/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] facCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            factores factores = new factores
            {
                fac_codigo = model.fac_codigo
            };

            _context.factores.Add(factores);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/factoress/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var factores = await _context.factores.FindAsync(id);
            if (factores == null)
            {
                return NotFound();
            }

            _context.factores.Remove(factores);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(factores);
        }



        private bool diagnosticosExists(int id)
        {
            return _context.diagnosticos.Any(e => e.iddiagnosticos == id);
        }
    }
}
