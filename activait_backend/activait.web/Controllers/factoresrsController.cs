﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.factoresr;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class factoresrsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public factoresrsController(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<FactoresrViewModel>> Listar()
        {
            var far = await _context.factoresr.ToListAsync();

            return far.Select(c => new FactoresrViewModel
            {
                idfactoresr = c.idfactoresr,
                factores_idfactores_riesgo = c.factores_idfactores_riesgo,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                fac_clase = c.fac_clase,
                fac_descripcion = c.fac_descripcion,
                fac_tiempoexpactual = c.fac_tiempoexpactual,
                fac_tiempoexpanteri = c.fac_tiempoexpanteri
            });
        }

        // GET: api/factoresr/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var far = await _context.factoresr.FindAsync(id);
            if (far == null)
            {
                return NotFound();
            }
            return Ok(new FactoresrViewModel
            {
                idfactoresr = far.idfactoresr,
                factores_idfactores_riesgo = far.factores_idfactores_riesgo,
                siniestros_idsiniestros = far.siniestros_idsiniestros,
                fac_clase = far.fac_clase,
                fac_descripcion = far.fac_descripcion,
                fac_tiempoexpactual = far.fac_tiempoexpactual,
                fac_tiempoexpanteri = far.fac_tiempoexpanteri
            });
        }

        // GET: api/factoresr/Mostrarsin/5
        [HttpGet("[action]/{idsin}")]
        public async Task<IEnumerable<FactoresrViewModel>> Mostrarsin(int idsin)
        {
            var fac = await _context.factoresr
                .Where(s => s.siniestros_idsiniestros == idsin)
                .Take(5)
                .ToListAsync();

            return fac.Select(c => new FactoresrViewModel
            {
                idfactoresr = c.idfactoresr,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                factores_idfactores_riesgo = c.factores_idfactores_riesgo,
                fac_clase = c.fac_clase,
                fac_descripcion = c.fac_descripcion,
                fac_tiempoexpactual = c.fac_tiempoexpactual,
                fac_tiempoexpanteri = c.fac_tiempoexpanteri
            });

        }



        // PUT: api/factoresr/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] farActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idfactoresr < 0)
            {
                return BadRequest();
            }

            var far = await _context.factoresr.FirstOrDefaultAsync(c => c.idfactoresr == model.idfactoresr);

            if (far == null)
            {
                return NotFound();
            }

            model.factores_idfactores_riesgo = far.factores_idfactores_riesgo;
            model.siniestros_idsiniestros = far.siniestros_idsiniestros;
            model.fac_clase = far.fac_clase;
            model.fac_descripcion = far.fac_descripcion;
            model.fac_tiempoexpactual = far.fac_tiempoexpactual;
            model.fac_tiempoexpanteri = far.fac_tiempoexpanteri;
            model.fac_fechamodificacion = far.fac_fechamodificacion;
            model.fac_usumodificacion = far.fac_usumodificacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/factoresr/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] farCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            factoresr far = new factoresr
            {
                factores_idfactores_riesgo = model.factores_idfactores_riesgo,
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                fac_clase = model.fac_clase,
                fac_descripcion = model.fac_descripcion,
                fac_tiempoexpactual = model.fac_tiempoexpactual,
                fac_tiempoexpanteri = model.fac_tiempoexpanteri,
                fac_fechacreacion = model.fac_fechacreacion,
                fac_usucreacion = model.fac_usucreacion
            };
            _context.factoresr.Add(far);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/factoresr/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var far = await _context.factoresr.FindAsync(id);
            if (far == null)
            {
                return NotFound();
            }

            _context.factoresr.Remove(far);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(far);
        }

        private bool factoresrExists(int id)
        {
            return _context.factoresr.Any(e => e.idfactoresr == id);
        }
    }
}