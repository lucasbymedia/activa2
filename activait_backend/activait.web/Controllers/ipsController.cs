﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.ips;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ipsController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public ipsController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/ips
        [HttpGet]
        public IEnumerable<ips> Getips()
        {
            return _context.ips;
        }

        // GET: api/ips/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ipsViewModel>> Listar()
        {
            var ip = await _context.ips.ToListAsync();

            return ip.Select(c => new ipsViewModel
            {
                idips = c.idips,
                ciudades_idciudades = c.ciudades_idciudades,
                ips_nombre = c.ips_nombre,
                ips_nit = c.ips_nit,
                ips_telefono = c.ips_telefono,
                ips_contacto = c.ips_contacto,
                ips_direccion = c.ips_direccion,
                ips_naturaleza = c.ips_naturaleza
            });
        }

        // GET: api/ips/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ip = await _context.ips.FindAsync(id);
            if (ip == null)
            {
                return NotFound();
            }
            return Ok(new ipsViewModel
            {
                idips = ip.idips,
                ciudades_idciudades = ip.ciudades_idciudades,
                ips_nombre = ip.ips_nombre,
                ips_nit = ip.ips_nit,
                ips_telefono = ip.ips_telefono,
                ips_contacto = ip.ips_contacto,
                ips_direccion = ip.ips_direccion,
                ips_naturaleza = ip.ips_naturaleza
            });
        }

        // PUT: api/ips/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] ipsActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idips < 0)
            {
                return BadRequest();
            }
            var ip = await _context.ips.FirstOrDefaultAsync(c => c.idips == model.idips);
            if (ip == null)
            {
                return NotFound();
            }

            ip.ciudades_idciudades = model.ciudades_idciudades;
            ip.ips_nombre = model.ips_nombre;
            ip.ips_nit = model.ips_nit;
            ip.ips_telefono = model.ips_telefono;
            ip.ips_contacto = model.ips_contacto;
            ip.ips_direccion = model.ips_direccion;
            ip.ips_naturaleza = model.ips_naturaleza;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/ips/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] ipsCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ips ip = new ips
            {
                ciudades_idciudades = model.ciudades_idciudades,
                ips_nombre = model.ips_nombre,
                ips_nit = model.ips_nit,
                ips_telefono = model.ips_telefono,
                ips_contacto = model.ips_contacto,
                ips_direccion = model.ips_direccion,
                ips_naturaleza = model.ips_naturaleza,

           };

            _context.ips.Add(ip);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/ips/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ip = await _context.ips.FindAsync(id);
            if (ip == null)
            {
                return NotFound();
            }

            _context.ips.Remove(ip);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(ip);
        }

        private bool ipsExists(int id)
        {
            return _context.ips.Any(e => e.idips == id);
        }
    }
}