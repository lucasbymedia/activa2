﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.monitoreos;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class monitoreosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public monitoreosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/monitoreos
        [HttpGet("[action]")]
        public async Task<IEnumerable<MonitoreosViewModel>> Listar()
        {
            var mon = await _context.monitoreos.ToListAsync();

            return mon.Select(c => new MonitoreosViewModel
            {
                idmonitoreos = c.idmonitoreos,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                tipos_idtipos_monitoreo = c.tipos_idtipos_monitoreo,
                mon_respuesta = c.mon_respuesta,
                mon_resultado = c.mon_resultado,
                mon_fecha = c.mon_fecha
            });
        }

        // GET: api/monitoreos/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var mon = await _context.monitoreos.FindAsync(id);
            if (mon == null)
            {
                return NotFound();
            }
            return Ok(new MonitoreosViewModel
            {
                idmonitoreos = mon.idmonitoreos,
                siniestros_idsiniestros = mon.siniestros_idsiniestros,
                tipos_idtipos_monitoreo = mon.tipos_idtipos_monitoreo,
                mon_respuesta = mon.mon_respuesta,
                mon_resultado = mon.mon_resultado,
                mon_fecha = mon.mon_fecha
            });
        }

        // GET: api/monitoreos/Mostrarsin/5
        [HttpGet("[action]/{idsin}")]
        public async Task<IEnumerable<MonitoreosViewModel>> Mostrarsin(int idsin)
        {
            var mon = await _context.monitoreos
                .Where(s => s.siniestros_idsiniestros == idsin)
                .Take(5)
                .ToListAsync();

            return mon.Select(c => new MonitoreosViewModel
            {
                idmonitoreos = c.idmonitoreos,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                tipos_idtipos_monitoreo = c.tipos_idtipos_monitoreo,
                mon_fecha = c.mon_fecha,
                mon_respuesta = c.mon_respuesta,
                mon_resultado = c.mon_resultado,
            });
        }


        // PUT: api/monitoreos/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] monActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idmonitoreos < 0)
            {
                return BadRequest();
            }

            var mon = await _context.monitoreos.FirstOrDefaultAsync(c => c.idmonitoreos == model.idmonitoreos);

            if (mon == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestros = mon.siniestros_idsiniestros;
            model.tipos_idtipos_monitoreo = mon.tipos_idtipos_monitoreo;
            model.mon_respuesta = mon.mon_respuesta;
            model.mon_resultado = mon.mon_resultado;
            model.mon_fecha = mon.mon_fecha;
            model.mon_fechamodificacion = mon.mon_fechamodificacion;
            model.mon_usumodificacion = mon.mon_usumodificacion;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/monitoreos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] monCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            monitoreos mon = new monitoreos
            {
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                tipos_idtipos_monitoreo = model.tipos_idtipos_monitoreo,
                mon_respuesta = model.mon_respuesta,
                mon_resultado = model.mon_resultado,
                mon_fecha = model.mon_fecha,
                mon_fechacreacion = model.mon_fechacreacion,
                mon_usucreacion = model.mon_usucreacion
            };
            _context.monitoreos.Add(mon);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/ciudades/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var mon = await _context.monitoreos.FindAsync(id);
            if (mon == null)
            {
                return NotFound();
            }

            _context.monitoreos.Remove(mon);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(mon);
        }

        private bool monitoreosExists(int id)
        {
            return _context.monitoreos.Any(e => e.idmonitoreos == id);
        }
    }
}