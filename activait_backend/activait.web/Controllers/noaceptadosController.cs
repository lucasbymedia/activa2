﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.noaceptados;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class noaceptadosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public noaceptadosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/noaceptados
        [HttpGet("[action]")]
        public async Task<IEnumerable<NoaceptadosViewModel>> Listar()
        {
            var noa = await _context.noaceptados.ToListAsync();

            return noa.Select(c => new NoaceptadosViewModel
            {
                idnoaceptados = c.idnoaceptados,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                noa_tipoaccion = c.noa_tipoaccion,
                noa_fecha = c.noa_fecha,
                noa_descripcion = c.noa_descripcion
            });
        }

        // GET: api/noaceptados/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var noa = await _context.noaceptados.FindAsync(id);
            if (noa == null)
            {
                return NotFound();
            }
            return Ok(new NoaceptadosViewModel
            {
                idnoaceptados = noa.idnoaceptados,
                siniestros_idsiniestros = noa.siniestros_idsiniestros,
                noa_tipoaccion = noa.noa_tipoaccion,
                noa_fecha = noa.noa_fecha,
                noa_descripcion = noa.noa_descripcion
            });
        }

        // GET: api/noaceptados/Mostrarsin/5
        [HttpGet("[action]/{idsin}")]
        public async Task<IEnumerable<NoaceptadosViewModel>> Mostrarsin(int idsin)
        {
            var noa = await _context.noaceptados
                .Where(s => s.siniestros_idsiniestros == idsin)
                .Take(5)
                .ToListAsync();

            return noa.Select(c => new NoaceptadosViewModel
            {
                idnoaceptados = c.idnoaceptados,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                noa_tipoaccion = c.noa_tipoaccion,
                noa_fecha = c.noa_fecha,
                noa_descripcion = c.noa_descripcion
            });
        }


        // PUT: api/noaceptados/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] noaActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idnoaceptados < 0)
            {
                return BadRequest();
            }

            var noa = await _context.noaceptados.FirstOrDefaultAsync(c => c.idnoaceptados == model.idnoaceptados);
            if (noa == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestros = noa.siniestros_idsiniestros;
            model.noa_tipoaccion = noa.noa_tipoaccion;
            model.noa_fecha = noa.noa_fecha;
            model.noa_descripcion = noa.noa_descripcion;
            model.noa_fechamodificacion = noa.noa_fechamodificacion;
            model.noa_usumodificacion = noa.noa_usumodificacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/noaceptados/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] noaCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            noaceptados noa = new noaceptados
            {
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                noa_tipoaccion = model.noa_tipoaccion,
                noa_fecha = model.noa_fecha,
                noa_descripcion = model.noa_descripcion,
                noa_fechacreacion = model.noa_fechacreacion,
                noa_usucreacion = model.noa_usucreacion
            };
            _context.noaceptados.Add(noa);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/noaceptados/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var noa = await _context.noaceptados.FindAsync(id);
            if (noa == null)
            {
                return NotFound();
            }

            _context.noaceptados.Remove(noa);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(noa);
        }

        private bool noaceptadosExists(int id)
        {
            return _context.noaceptados.Any(e => e.idnoaceptados == id);
        }
    }
}