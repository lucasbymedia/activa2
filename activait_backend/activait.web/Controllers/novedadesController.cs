﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.novedades;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class novedadesController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public novedadesController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/novedades
        public async Task<IEnumerable<NovedadesViewModel>> Listar()
        {
            var nov = await _context.novedades.ToListAsync();

            return nov.Select(c => new NovedadesViewModel
            {
                idnovedades = c.idnovedades,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                testigos_idtestigos = c.testigos_idtestigos,
                nov_codigo = c.nov_codigo,
                nov_datooriginal = c.nov_datooriginal,
                nov_datonuevo = c.nov_datonuevo
            });
        }

        // GET: api/novedades/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var nov = await _context.novedades.FindAsync(id);
            if (nov == null)
            {
                return NotFound();
            }
            return Ok(new NovedadesViewModel
            {
                idnovedades = nov.idnovedades,
                siniestros_idsiniestros = nov.siniestros_idsiniestros,
                testigos_idtestigos = nov.testigos_idtestigos,
                nov_codigo = nov.nov_codigo,
                nov_datooriginal = nov.nov_datooriginal,
                nov_datonuevo = nov.nov_datonuevo
            });
        }

        // PUT: api/novedades/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] novActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idnovedades < 0)
            {
                return BadRequest();
            }

            var nov = await _context.novedades.FirstOrDefaultAsync(c => c.idnovedades == model.idnovedades);

            if (nov == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestros = nov.siniestros_idsiniestros;
            model.testigos_idtestigos = nov.testigos_idtestigos;
            model.nov_codigo = nov.nov_codigo;
            model.nov_datooriginal = nov.nov_datooriginal;
            model.nov_datonuevo = nov.nov_datonuevo;
            model.nov_fechamodificacion = nov.nov_fechamodificacion;
            model.nov_usumodificacion = nov.nov_usumodificacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/novedades/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] novCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            novedades nov = new novedades
            {
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                testigos_idtestigos = model.testigos_idtestigos,
                nov_codigo = model.nov_codigo,
                nov_datooriginal = model.nov_datooriginal,
                nov_datonuevo = model.nov_datonuevo,
                nov_fechacreacion = model.nov_fechacreacion,
                nov_usucreacion = model.nov_usucreacion
            };
            _context.novedades.Add(nov);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/novedades/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var nov = await _context.novedades.FindAsync(id);
            if (nov == null)
            {
                return NotFound();
            }

            _context.novedades.Remove(nov);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(nov);
        }

        private bool novedadesExists(int id)
        {
            return _context.novedades.Any(e => e.idnovedades == id);
        }
    }
}