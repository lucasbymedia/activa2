﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.personas;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class personasController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public personasController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/personas/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<PersonasViewModel>> Listar()
        {
            var persona = await _context.personas.ToListAsync();

            return persona.Select(c => new PersonasViewModel
            {
                idpersonas = c.idpersonas,
                empresas_idempresas = c.empresas_idempresas,
                afps_idafps = c.afps_idafps,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                per_tipovinculacion = c.per_tipovinculacion,
                per_direccion = c.per_direccion,
                per_telefono = c.per_telefono,
                per_cargo = c.per_cargo
            });
        }

        // GET: api/personas/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var persona = await _context.personas.FindAsync(id);
            if (persona == null)
            {
                return NotFound();
            }
            return Ok(new PersonasViewModel
            {
                idpersonas = persona.idpersonas,
                empresas_idempresas = persona.empresas_idempresas,
                afps_idafps = persona.afps_idafps,
                per_tipoid = persona.per_tipoid,
                per_numeroid = persona.per_numeroid,
                per_primernombre = persona.per_primernombre,
                per_segundonombre = persona.per_segundonombre,
                per_primerapellido = persona.per_primerapellido,
                per_segundoapellido = persona.per_segundoapellido,
                per_nacimiento = persona.per_nacimiento,
                per_genero = persona.per_genero,
                per_tipovinculacion = persona.per_tipovinculacion,
                per_direccion = persona.per_direccion,
                per_telefono = persona.per_telefono,
                per_cargo = persona.per_cargo
            });
        }

        // GET: api/personas/Consulta_emp/5
        [HttpGet("[action]/{idemp}")]
        public async Task<IEnumerable<PersonasViewModel>> Consulta_emp([FromRoute] int idemp)
        {
            var persona = await _context.personas
               .Where(s => s.empresas_idempresas == idemp)
               .Take(100)
               .ToListAsync();

            return persona.Select(c => new PersonasViewModel
            {
                idpersonas = c.idpersonas,
                empresas_idempresas = c.empresas_idempresas,
                afps_idafps = c.afps_idafps,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                per_tipovinculacion = c.per_tipovinculacion,
                per_direccion = c.per_direccion,
                per_telefono = c.per_telefono,
                per_cargo = c.per_cargo
            });
        }


        // PUT: api/personas/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] perActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idpersonas < 0)
            {
                return BadRequest();
            }
            var persona = await _context.personas.FirstOrDefaultAsync(c => c.idpersonas == model.idpersonas);
            if (persona == null)
            {
                return NotFound();
            }
            persona.per_tipoid = model.per_tipoid;
            persona.empresas_idempresas = model.empresas_idempresas;
            persona.afps_idafps = model.afps_idafps;
            persona.per_numeroid = model.per_numeroid;
            persona.per_primernombre = model.per_primernombre;
            persona.per_segundonombre = model.per_segundonombre;
            persona.per_primerapellido = model.per_primerapellido;
            persona.per_segundoapellido = model.per_segundoapellido;
            persona.per_nacimiento = model.per_nacimiento;
            persona.per_genero = model.per_genero;
            persona.per_tipovinculacion = model.per_tipovinculacion;
            persona.per_direccion = model.per_direccion;
            persona.per_telefono = model.per_telefono;
            persona.per_cargo = model.per_cargo;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/personas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] perCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            personas personas = new personas
            {
                per_tipoid = model.per_tipoid,
                empresas_idempresas = model.empresas_idempresas,
                afps_idafps = model.afps_idafps,
                per_numeroid = model.per_numeroid,
                per_primernombre = model.per_primernombre,
                per_segundonombre = model.per_segundonombre,
                per_primerapellido = model.per_primerapellido,
                per_segundoapellido = model.per_segundoapellido,
                per_nacimiento = model.per_nacimiento,
                per_genero = model.per_genero,
                per_tipovinculacion = model.per_tipovinculacion,
                per_direccion = model.per_direccion,
                per_telefono = model.per_telefono,
                per_cargo = model.per_cargo
        };

            _context.personas.Add(personas);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/personas/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var personas = await _context.personas.FindAsync(id);
            if (personas == null)
            {
                return NotFound();
            }

            _context.personas.Remove(personas);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(personas);
        }



        private bool diagnosticosExists(int id)
        {
            return _context.diagnosticos.Any(e => e.iddiagnosticos == id);
        }
    }

}
