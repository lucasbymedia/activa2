﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.reservas;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class reservasController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public reservasController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/reservas
        [HttpGet("[action]")]
        public async Task<IEnumerable<ReservasViewModel>> Listar()
        {
            var res = await _context.reservas.ToListAsync();

            return res.Select(c => new ReservasViewModel
            {
                idreservas = c.idreservas,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                res_codigo = c.res_codigo, 
                res_valor = c.res_valor,
                res_acumulado = c.res_acumulado,
                res_fecha = c.res_fecha, 
                res_autorizacion=c.res_autorizacion
            });
        }

        // GET: api/roles/reservas/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var res = await _context.reservas.FindAsync(id);
            if (res == null)
            {
                return NotFound();
            }
            return Ok(new ReservasViewModel
            {
                idreservas = res.idreservas,
                siniestros_idsiniestros = res.siniestros_idsiniestros,
                res_codigo = res.res_codigo,
                res_valor = res.res_valor,
                res_acumulado = res.res_acumulado,
                res_fecha = res.res_fecha,
                res_autorizacion = res.res_autorizacion,
                res_estado = res.res_estado
            });
        }

        // GET: api/reservas/Mostrarsin/5
        [HttpGet("[action]/{idsin}")]
        public async Task<IEnumerable<ReservasViewModel>> Mostrarsin(int idsin)
        {
            var res = await _context.reservas
                .Where(s => s.siniestros_idsiniestros == idsin)
                .Take(5)
                .ToListAsync();

            return res.Select(c => new ReservasViewModel
            {
                idreservas = c.idreservas,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                res_codigo = c.res_codigo,
                res_valor = c.res_valor,
                res_acumulado = c.res_acumulado,
                res_fecha = c.res_fecha,
                res_autorizacion = c.res_autorizacion,
                res_estado = c.res_estado
            });
        }


        // PUT: api/reservas/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] resActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idreservas < 0)
            {
                return BadRequest();
            }

            var res = await _context.reservas.FirstOrDefaultAsync(c => c.idreservas == model.idreservas);

            if (res == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestros = res.siniestros_idsiniestros;
            model.res_codigo = res.res_codigo;
            model.res_valor = res.res_valor;
            model.res_acumulado = res.res_acumulado;
            model.res_fecha = res.res_fecha;
            model.res_autorizacion = res.res_autorizacion;
            model.res_estado = res.res_estado;
            model.res_fechamodificacion = res.res_fechamodificacion;
            model.res_usumodificacion = res.res_usumodificacion;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/reservas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] resCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            reservas res = new reservas
            {
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                res_codigo = model.res_codigo,
                res_valor = model.res_valor,
                res_acumulado = model.res_acumulado,
                res_fecha = model.res_fecha,
                res_autorizacion = model.res_autorizacion,
                res_estado = model.res_estado,
                res_fechacreacion = model.res_fechacreacion,
                res_usucreacion = model.res_usucreacion
            };
            _context.reservas.Add(res);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/reservas/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var res = await _context.reservas.FindAsync(id);
            if (res == null)
            {
                return NotFound();
            }

            _context.reservas.Remove(res);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(res);
        }


        private bool reservasExists(int id)
        {
            return _context.reservas.Any(e => e.idreservas == id);
        }
    }
}