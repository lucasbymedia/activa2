﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.usuarios;
using activait.web.Models.usuarios.roles;
using Microsoft.AspNetCore.Authorization;

namespace activait.web.Controllers
{
//    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class rolesController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public rolesController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/roles/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<RolesViewModel>> Listar()
        {
            var rol = await _context.roles.ToListAsync();

            return rol.Select(c => new RolesViewModel
            {
                idroles = c.idroles,
                rol_nombre = c.rol_nombre

            });
        }

        // GET: api/roles/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var roles = await _context.roles.FindAsync(id);
            if (roles == null)
            {
                return NotFound();
            }
            return Ok(new RolesViewModel
            {
                idroles = roles.idroles,
                rol_nombre = roles.rol_nombre

            });
        }

        // PUT: api/roles/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idroles < 0)
            {
                return BadRequest();
            }

            var roles = await _context.roles.FirstOrDefaultAsync(c => c.idroles == model.idroles);

            if(roles == null)
            {
                return NotFound();
            }

            roles.rol_nombre = model.rol_nombre;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/roles/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            roles roles = new roles
            {
                rol_nombre = model.rol_nombre
            };

            _context.roles.Add(roles);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/roles/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var roles = await _context.roles.FindAsync(id);
            if (roles == null)
            {
                return NotFound();
            }

            _context.roles.Remove(roles);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
                    
            }
            return Ok(roles);
        }

        private bool rolesExists(int id)
        {
            return _context.roles.Any(e => e.idroles == id);
        }
    }
}