﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.sercontratados;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class sercontratadosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public sercontratadosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/sercontratados/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<SercontratadosViewModel>> Listar()
        {
            var servc = await _context.sercontratados.ToListAsync();

            return servc.Select(c => new SercontratadosViewModel
            {
                idsercontratados = c.idsercontratados,
                ips_idps = c.ips_idps,
                servicios_idservicios = c.servicios_idservicios,
                sec_fechainicio = c.sec_fechainicio,
                sec_fechafin = c.sec_fechafin,
                sec_valor = c.sec_valor,
                sec_prioridadasign = c.sec_prioridadasign,
                sec_prioridadcosto = c.sec_prioridadcosto
            });
        }

        // GET: api/sercontratados/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var servc = await _context.sercontratados.FindAsync(id);
            if (servc == null)
            {
                return NotFound();
            }
            return Ok(new SercontratadosViewModel
            {
                idsercontratados = servc.idsercontratados,
                ips_idps = servc.ips_idps,
                servicios_idservicios = servc.servicios_idservicios,
                sec_fechainicio = servc.sec_fechainicio,
                sec_fechafin = servc.sec_fechafin,
                sec_valor = servc.sec_valor,
                sec_prioridadasign = servc.sec_prioridadasign,
                sec_prioridadcosto = servc.sec_prioridadcosto
            });
        }

        // PUT: api/sercontratados/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] secActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idsercontratados < 0)
            {
                return BadRequest();
            }
            var servc = await _context.sercontratados.FirstOrDefaultAsync(c => c.idsercontratados == model.idsercontratados);
            if (servc == null)
            {
                return NotFound();
            }

            servc.ips_idps = model.ips_idps;
            servc.servicios_idservicios = model.servicios_idservicios;
            servc.sec_fechainicio = model.sec_fechainicio;
            servc.sec_fechafin = model.sec_fechafin;
            servc.sec_valor = model.sec_valor;
            servc.sec_estado = model.sec_estado;
            servc.sec_prioridadasign = model.sec_prioridadasign;
            servc.sec_prioridadcosto = model.sec_prioridadcosto;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/sercontratados/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] secCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            sercontratados servc = new sercontratados
            {
                ips_idps = model.ips_idps,
                servicios_idservicios = model.servicios_idservicios,
                sec_fechainicio = model.sec_fechainicio,
                sec_fechafin = model.sec_fechafin,
                sec_valor = model.sec_valor,
                sec_estado = model.sec_estado,
                sec_prioridadasign = model.sec_prioridadasign,
                sec_prioridadcosto = model.sec_prioridadcosto
            };

            _context.sercontratados.Add(servc);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/sercontratados/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var servc = await _context.sercontratados.FindAsync(id);
            if (servc == null)
            {
                return NotFound();
            }

            _context.sercontratados.Remove(servc);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(servc);
        }


        private bool sercontratadosExists(int id)
        {
            return _context.sercontratados.Any(e => e.idsercontratados == id);
        }
    }
}