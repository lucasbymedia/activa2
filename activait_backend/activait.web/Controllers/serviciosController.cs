﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.servicios;


namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class serviciosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public serviciosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/servicios/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ServiciosVierwModel>> Listar()
        {
            var servicio = await _context.Servicios.ToListAsync();

            return servicio.Select(c => new ServiciosVierwModel
            {
                idservicios = c.idservicios,
                ser_codigo = c.ser_codigo,
                ser_descripcion = c.ser_descripcion,
                ser_cum = c.ser_cum,
                ser_cups = c.ser_cups,
                ser_estado = c.ser_estado,
                ser_pbs = c.ser_pbs,
                ser_prioridad = c.ser_prioridad
            });
        }

        // GET: api/servicios/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var servicio = await _context.Servicios.FindAsync(id);
            if (servicio == null)
            {
                return NotFound();
            }
            return Ok(new ServiciosVierwModel
            {
                idservicios = servicio.idservicios,
                ser_codigo = servicio.ser_codigo,
                ser_descripcion = servicio.ser_descripcion,
                ser_cum = servicio.ser_cum,
                ser_cups = servicio.ser_cups,
                ser_estado = servicio.ser_estado,
                ser_pbs = servicio.ser_pbs,
                ser_prioridad = servicio.ser_prioridad
            });
        }

        // PUT: api/servicios/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] serActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idservicios < 0)
            {
                return BadRequest();
            }
            var servicio = await _context.Servicios.FirstOrDefaultAsync(c => c.idservicios == model.idservicios);
            if (servicio == null)
            {
                return NotFound();
            }
            servicio.ser_codigo = model.ser_codigo;
            servicio.ser_descripcion = model.ser_descripcion;
            servicio.ser_cups = model.ser_cups;
            servicio.ser_cum = model.ser_cum;
            servicio.ser_estado = model.ser_estado;
            servicio.ser_pbs = model.ser_pbs;
            servicio.ser_prioridad = model.ser_prioridad;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/servicios/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] serCrearVierwModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            servicios servicios = new servicios
            {
                ser_codigo = model.ser_codigo,
                ser_descripcion = model.ser_descripcion,
                ser_cups = model.ser_cups,
                ser_cum = model.ser_cum,
                ser_estado = model.ser_estado,
                ser_pbs = model.ser_pbs,
                ser_prioridad = model.ser_prioridad
            };

            _context.Servicios.Add(servicios);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/servicios/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var servicios = await _context.Servicios.FindAsync(id);
            if (servicios == null)
            {
                return NotFound();
            }

            _context.Servicios.Remove(servicios);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(servicios);
        }

        private bool diagnosticosExists(int id)
        {
            return _context.diagnosticos.Any(e => e.iddiagnosticos == id);
        }
    }
}