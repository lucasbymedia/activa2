﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.siniestros;


namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class siniestrosController : ControllerBase
    {
        private readonly DBContextActivait _context;
        public siniestrosController(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SiniestrosViewModel>> Listar()
        {
            var sin = await _context.siniestros.ToListAsync();

            return sin.Select(c => new SiniestrosViewModel
            {
                idsieniestros = c.idsieniestros,
                personas_idpersonas = c.personas_idpersonas,
                ciudades_idciudades = c.ciudades_idciudades,
                sin_fecha = c.sin_fecha.ToString(),
                sin_tipoatep = c.sin_tipoatep,
                sin_jornada = c.sin_jornada,
                sin_laboral = c.sin_laboral,
                sin_otra = c.sin_otra,
                sin_cargoresponsable = c.sin_cargoresponsable,
                sin_codigo=c.sin_codigo,
                sin_descripcion=c.sin_descripcion,
                sin_diagnosticadapor=c.sin_diagnosticadapor,
                sin_documentoresponsable=c.sin_documentoresponsable,
                sin_mecanismo=c.sin_mecanismo,
                sin_tiempolaborprev = c.sin_tiempolaborprev,
                sin_causal = c.sin_causal,
                sin_muerte = c.sin_muerte,
                sin_sitio = c.sin_sitio,
                sin_lugar=c.sin_lugar,
                sin_tipolesion = c.sin_tipolesion,
                sin_parteafectada = c.sin_parteafectada,
                sin_agente = c.sin_agente,
                sin_responsable = c.sin_responsable,
                sin_nombreresponsable = c.sin_nombreresponsable,
                sin_diagnostico = c.sin_diagnostico,
                sin_regmedico = c.sin_regmedico,
                sin_nombremedico = c.sin_nombremedico,
                sin_fechadiagnostico = c.sin_fechadiagnostico,
                sin_evalmedicapre = c.sin_evalmedicaper,
                sin_evalmedicaper = c.sin_evalmedicaper,
                sin_evalmedicaegre = c.sin_evalmedicaper, 
                sin_zona = c.sin_zona
            });
        }

        // GET: api/siniestros/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var sin = await _context.siniestros.SingleOrDefaultAsync(a=>a.idsieniestros==id);
            if (sin == null)
            {
                return NotFound();
            }
            return Ok(new SiniestrosViewModel
            {
                idsieniestros = sin.idsieniestros,
                personas_idpersonas = sin.personas_idpersonas,
                ciudades_idciudades = sin.ciudades_idciudades,
                sin_fecha = sin.sin_fecha.ToString(),
                sin_tipoatep = sin.sin_tipoatep,
                sin_jornada = sin.sin_jornada,
                sin_laboral = sin.sin_laboral,
                sin_cargoresponsable = sin.sin_cargoresponsable,
                sin_codigo = sin.sin_codigo,
                sin_descripcion = sin.sin_descripcion,
                sin_diagnosticadapor = sin.sin_diagnosticadapor,
                sin_documentoresponsable = sin.sin_documentoresponsable,
                sin_mecanismo = sin.sin_mecanismo,
                sin_otra = sin.sin_otra,
                sin_tiempolaborprev = sin.sin_tiempolaborprev,
                sin_causal = sin.sin_causal,
                sin_muerte = sin.sin_muerte,
                sin_sitio = sin.sin_sitio,
                sin_lugar=sin.sin_lugar,
                sin_tipolesion = sin.sin_tipolesion,
                sin_parteafectada = sin.sin_parteafectada,
                sin_agente = sin.sin_agente,
                sin_responsable = sin.sin_responsable,
                sin_nombreresponsable = sin.sin_nombreresponsable,
                sin_diagnostico = sin.sin_diagnostico,
                sin_regmedico = sin.sin_regmedico,
                sin_nombremedico = sin.sin_nombremedico,
                sin_fechadiagnostico = sin.sin_fechadiagnostico,
                sin_evalmedicapre = sin.sin_evalmedicaper,
                sin_evalmedicaper = sin.sin_evalmedicaper,
                sin_evalmedicaegre = sin.sin_evalmedicaper
            });
        }

        // GET: api/siniestros/Consulta/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Consulta([FromRoute] int id)
        {
            var sin = await _context.siniestros.OrderByDescending(a=>a.idsieniestros)
                .Take(1)
                .SingleOrDefaultAsync(a => a.personas_idpersonas == id);
            if (sin == null)
            {
                return NotFound();
            }
            return Ok(new SiniestrosViewModel
            {
                idsieniestros = sin.idsieniestros,
                personas_idpersonas = sin.personas_idpersonas,
                ciudades_idciudades = sin.ciudades_idciudades,
                sin_fecha = sin.sin_fecha.ToString(),
                sin_tipoatep = sin.sin_tipoatep,
                sin_jornada = sin.sin_jornada,
                sin_laboral = sin.sin_laboral,
                sin_cargoresponsable = sin.sin_cargoresponsable,
                sin_codigo = sin.sin_codigo,
                sin_descripcion = sin.sin_descripcion,
                sin_diagnosticadapor = sin.sin_diagnosticadapor,
                sin_documentoresponsable = sin.sin_documentoresponsable,
                sin_mecanismo = sin.sin_mecanismo,
                sin_otra = sin.sin_otra,
                sin_tiempolaborprev = sin.sin_tiempolaborprev,
                sin_causal = sin.sin_causal,
                sin_muerte = sin.sin_muerte,
                sin_sitio = sin.sin_sitio,
                sin_lugar = sin.sin_lugar,
                sin_tipolesion = sin.sin_tipolesion,
                sin_parteafectada = sin.sin_parteafectada,
                sin_agente = sin.sin_agente,
                sin_responsable = sin.sin_responsable,
                sin_nombreresponsable = sin.sin_nombreresponsable,
                sin_diagnostico = sin.sin_diagnostico,
                sin_regmedico = sin.sin_regmedico,
                sin_nombremedico = sin.sin_nombremedico,
                sin_fechadiagnostico = sin.sin_fechadiagnostico,
                sin_evalmedicapre = sin.sin_evalmedicaper,
                sin_evalmedicaper = sin.sin_evalmedicaper,
                sin_evalmedicaegre = sin.sin_evalmedicaper
            });
        }

        // PUT: api/siniestros/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] sinActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idsieniestros < 0)
            {
                return BadRequest();
            }

            var sin = await _context.siniestros.FirstOrDefaultAsync(c => c.idsieniestros == model.idsieniestros);

            if (sin == null)
            {
                return NotFound();
            }

            model.idsieniestros = sin.idsieniestros;
            model.personas_idpersonas = sin.personas_idpersonas;
            model.ciudades_idciudades = sin.ciudades_idciudades;
            model.sin_fecha = sin.sin_fecha;
            model.sin_tipoatep = sin.sin_tipoatep;
            model.sin_jornada = sin.sin_jornada;
            model.sin_laboral = sin.sin_laboral;
            model.sin_cargoresponsable = sin.sin_cargoresponsable;
            model.sin_codigo = sin.sin_codigo;
            model.sin_descripcion = sin.sin_descripcion;
            model.sin_diagnosticadapor = sin.sin_diagnosticadapor;
            model.sin_documentoresponsable = sin.sin_documentoresponsable;
            model.sin_mecanismo = sin.sin_mecanismo;
            model.sin_otra = sin.sin_otra;
            model.sin_tiempolaborprev = sin.sin_tiempolaborprev;
            model.sin_causal = sin.sin_causal;
            model.sin_muerte = sin.sin_muerte;
            model.sin_sitio = sin.sin_sitio;
            model.sin_lugar = sin.sin_lugar;
            model.sin_tipolesion = sin.sin_tipolesion;
            model.sin_parteafectada = sin.sin_parteafectada;
            model.sin_agente = sin.sin_agente;
            model.sin_responsable = sin.sin_responsable;
            model.sin_nombreresponsable = sin.sin_nombreresponsable;
            model.sin_diagnostico = sin.sin_diagnostico;
            model.sin_regmedico = sin.sin_regmedico;
            model.sin_nombremedico = sin.sin_nombremedico;
            model.sin_fechadiagnostico = sin.sin_fechadiagnostico;
            model.sin_evalmedicapre = sin.sin_evalmedicaper;
            model.sin_evalmedicaper = sin.sin_evalmedicaper;
            model.sin_evalmedicaegre = sin.sin_evalmedicaper;
            model.sin_zona = sin.sin_zona;

            model.sin_fechamodificacion = sin.sin_fechamodificacion;
            model.sin_usumodificacion = sin.sin_usumodificacion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/siniestros/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] sinCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fechahora = DateTime.Now;

            siniestros sin = new siniestros
            {
                personas_idpersonas = model.personas_idpersonas,
                ciudades_idciudades = model.ciudades_idciudades,
                sin_fecha = model.sin_fecha,
                sin_tipoatep = model.sin_tipoatep,
                sin_jornada = model.sin_jornada,
                sin_laboral = model.sin_laboral,
                sin_otra = model.sin_otra,
                sin_codigo = model.sin_codigo,
                sin_tiempolaborprev = model.sin_tiempolaborprev,
                sin_causal = model.sin_causal,
                sin_muerte = model.sin_muerte,
                sin_tipolesion = model.sin_tipolesion,
                sin_parteafectada = model.sin_parteafectada,
                sin_agente = model.sin_agente,
                sin_lugar = model.sin_lugar,
                sin_mecanismo = model.sin_mecanismo,
                sin_descripcion = model.sin_descripcion,
                sin_responsable = model.sin_responsable,
                sin_nombreresponsable = model.sin_nombreresponsable,
                sin_cargoresponsable = model.sin_cargoresponsable,
                sin_documentoresponsable = model.sin_documentoresponsable,
                sin_fechadiagnostico = model.sin_fechadiagnostico,
                sin_diagnosticadapor = model.sin_diagnosticadapor,
                sin_diagnostico = model.sin_diagnostico,
                sin_regmedico = model.sin_regmedico,
                sin_nombremedico = model.sin_nombremedico,
                sin_evalmedicapre = model.sin_evalmedicaper,
                sin_evalmedicaper = model.sin_evalmedicaper,
                sin_evalmedicaegre = model.sin_evalmedicaper,
                sin_zona=model.sin_zona,
                sin_sitio =model.sin_sitio,
                sin_fechacreacion = model.sin_fechacreacion,
                sin_usucreacion = model.sin_usucreacion
            };
            try
            {
                _context.siniestros.Add(sin);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/siniestros/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var sin = await _context.siniestros.FindAsync(id);
            if (sin == null)
            {
                return NotFound();
            }

            _context.siniestros.Remove(sin);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(sin);
        }

        // PUT: api/siniestros/Estado/5
        [HttpPut("[action]/{id}/{estado}/{obs}/{uau}")]
        public async Task<IActionResult> Estado([FromRoute] int id, string estado, string obs, int usu)
        {
            if (id < 0)
            {
                return BadRequest();
            }
            var sin = await _context.siniestros.FirstOrDefaultAsync(c => c.idsieniestros == id);
            if (sin == null)
            {
                return NotFound();
            }
            sin.sin_estado = estado;
            sin.sin_objecionobs = obs;
            sin.sin_fechamodificacion = DateTime.Now;
            sin.sin_usumodificacion = usu;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            return Ok();
        }

        private bool siniestrosExists(int id)
        {
            return _context.siniestros.Any(e => e.idsieniestros == id);
        }
    }
}