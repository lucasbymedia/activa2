﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.autorizaciones;
using activait.web.Models.autorizaciones.testigos;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class testigos1Controller : ControllerBase
    {
        private readonly DBContextActivait _context;

        public testigos1Controller(DBContextActivait context)
        {
            _context = context;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<TestigosViewModel>> Listar()
        {

            var tes = await _context.testigos.ToListAsync();

            return tes.Select(c => new TestigosViewModel
            {
                idtestigos = c.idtestigos,
                siniestros_idsiniestros = c.siniestros_idsiniestros,
                tes_tipodoc = c.tes_tipodoc,
                tes_documento = c.tes_documento,
                tes_nombre = c.tes_nombre,
                tes_cargo = c.tes_cargo
            });
        }

        // GET: api/testigos/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var tes = await _context.testigos.FindAsync(id);
            if (tes == null)
            {
                return NotFound();
            }
            return Ok(new TestigosViewModel
            {
                idtestigos = tes.idtestigos,
                siniestros_idsiniestros = tes.siniestros_idsiniestros,
                tes_tipodoc = tes.tes_tipodoc,
                tes_documento = tes.tes_documento,
                tes_nombre = tes.tes_nombre,
                tes_cargo = tes.tes_cargo
            });
        }

        // PUT: api/testigos/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] tesActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idtestigos < 0)
            {
                return BadRequest();
            }

            var tes = await _context.testigos.FirstOrDefaultAsync(c => c.idtestigos == model.idtestigos);

            if (tes == null)
            {
                return NotFound();
            }

            model.siniestros_idsiniestros = tes.siniestros_idsiniestros;
            model.tes_tipodoc = tes.tes_tipodoc;
            model.tes_documento = tes.tes_documento;
            model.tes_nombre = tes.tes_nombre;
            model.tes_cargo = tes.tes_cargo;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/testigos/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] tesCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            testigos tes = new testigos
            {
                siniestros_idsiniestros = model.siniestros_idsiniestros,
                tes_tipodoc = model.tes_tipodoc,
                tes_documento = model.tes_documento,
                tes_nombre = model.tes_nombre,
                tes_cargo = model.tes_cargo
            };
            _context.testigos.Add(tes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/testigos/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tes = await _context.testigos.FindAsync(id);
            if (tes == null)
            {
                return NotFound();
            }

            _context.testigos.Remove(tes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(tes);
        }

        private bool testigosExists(int id)
        {
            return _context.testigos.Any(e => e.idtestigos == id);
        }
    }
}