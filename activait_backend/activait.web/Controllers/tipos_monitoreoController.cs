﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.parametrizacion;
using activait.web.Models.parametrizacion.tipos_monitoreo;


namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tipos_mmonitoreoController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public tipos_mmonitoreoController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/tipos_monitoreo/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<tipViewModel>> Listar()
        {
            var tipo = await _context.tipos_Monitoreos.ToListAsync();

            return tipo.Select(c => new tipViewModel
            {
                idtipos_monitoreo = c.idtipos_monitoreo,
                tip_codigo = c.tip_codigo,
                tip_descripcion = c.tip_descripcion
            });
        }

        // GET: api/tipos_monitoreo/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var tipo = await _context.tipos_Monitoreos.FindAsync(id);
            if (tipo == null)
            {
                return NotFound();
            }
            return Ok(new tipViewModel
            {
                idtipos_monitoreo = tipo.idtipos_monitoreo,
                tip_codigo = tipo.tip_codigo,
                tip_descripcion = tipo.tip_descripcion
            });
        }

        // PUT: api/tipos_monitoreo/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] tipActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idtipos_monitoreo < 0)
            {
                return BadRequest();
            }
            var tipo = await _context.tipos_Monitoreos.FirstOrDefaultAsync(c => c.idtipos_monitoreo == model.idtipos_monitoreo);
            if (tipo == null)
            {
                return NotFound();
            }
            tipo.tip_codigo = model.tip_codigo;
            tipo.tip_descripcion = model.tip_descripcion;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }
            return Ok();
        }

        // POST: api/servicios/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] tipCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tipos_monitoreo tipo = new tipos_monitoreo
            {
                tip_codigo = model.tip_codigo,
                tip_descripcion = model.tip_descripcion
            };

            _context.tipos_Monitoreos.Add(tipo);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
        // DELETE: api/servicios/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tipo = await _context.tipos_Monitoreos.FindAsync(id);
            if (tipo == null)
            {
                return NotFound();
            }

            _context.tipos_Monitoreos.Remove(tipo);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
            return Ok(tipo);
        }



        private bool diagnosticosExists(int id)
        {
            return _context.diagnosticos.Any(e => e.iddiagnosticos == id);
        }
    }
}