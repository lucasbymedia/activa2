﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.usuarios;
using activait.web.Models.usuarios.users;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class usersController : ControllerBase
    {
        private readonly DBContextActivait _context;
        private string usu_email;
        private readonly IConfiguration _config;

        public usersController(DBContextActivait context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/users/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuariosViewModel>> Listar()
        {
            var usuario = await _context.usuarios.ToListAsync();

            return usuario.Select(c => new UsuariosViewModel
            {
                idusuarios = c.idusuarios,
                roles_idroles = c.roles_idroles,
                usu_nombre = c.usu_nombre,
                usu_email = c.usu_email,
                usu_documento = c.usu_documento,
                usu_telefono = c.usu_telefono,
                usu_passwordhash=c.usu_passwordhash
            });

        }

        // GET: api/users/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var usuario = await _context.usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            return Ok(new UsuariosViewModel
            {
                idusuarios = usuario.idusuarios,
                roles_idroles = usuario.roles_idroles,
                usu_nombre = usuario.usu_nombre,
                usu_email = usuario.usu_email,
                usu_documento = usuario.usu_documento,
                usu_telefono = usuario.usu_telefono,
                usu_passwordhash=usuario.usu_passwordhash
            });
        }

        // PUT: api/users/Actualizar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Actualizar([FromBody] usrActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (model.idusuarios < 0)
            {
                return BadRequest();
            }
            var usuario = await _context.usuarios.FirstOrDefaultAsync(c => c.idusuarios == model.idusuarios);

            if (usuario == null)
            {
                return NotFound();
            }
            usuario.roles_idroles = model.roles_idroles;
            usuario.usu_nombre = model.usu_nombre;
            usuario.usu_email = model.usu_email;
            usuario.usu_documento = model.usu_documento;
            usuario.usu_telefono = model.usu_telefono;


            model.act_password = true;

            if (model.act_password == true) {
                CrearPasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);
                usuario.usu_passwordhash = passwordHash;
                usuario.usu_passwordsalt = passwordSalt;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // puedo guardar en logs
                return NotFound();
            }

            return Ok();
        }

        // POST: api/users/crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] usrCrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var email = model.usu_email.ToLower();
            if (await _context.usuarios.AnyAsync(u => u.usu_email == email)) {
                return BadRequest("El email ya existe");
            }

            CrearPasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);

            users usuario = new users
            {
                roles_idroles = model.roles_idroles,
                usu_nombre = model.usu_nombre,
                usu_documento = model.usu_documento,
                usu_telefono = model.usu_telefono,
                usu_email = model.usu_email.ToLower(),
                usu_passwordhash=passwordHash,
                usu_passwordsalt=passwordSalt

            };
            _context.usuarios.Add(usuario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }


        // DELETE: api/users/Deleteusers/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Deleteusers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var usuario = await _context.usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            _context.usuarios.Remove(usuario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(usuario);
        }
        
        [HttpPost("[action]")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var email = model.email.ToLower();
            var usuario = await _context.usuarios.FirstOrDefaultAsync(u => u.usu_email == model.email);

            if (usuario == null) {
                return NotFound();
            }

            if (!VerificarPasswordHash(model.password, usuario.usu_passwordhash, usuario.usu_passwordsalt)) {
                return NotFound();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, usuario.idusuarios.ToString()),
                new Claim(ClaimTypes.Email, email),
                new Claim(ClaimTypes.Role, usuario.roles_idroles.ToString()),
                new Claim("idusuarios", usuario.idusuarios.ToString()),
                new Claim("idroles", usuario.roles_idroles.ToString()),
                new Claim("nombre", usuario.usu_nombre)
            };

            return Ok(new
            {
                token = GenerarToken(claims),
                idusuarios = usuario.idusuarios.ToString(),
                idroles = usuario.roles_idroles.ToString(),
                nombre = usuario.usu_nombre
            });

        }

        private string GenerarToken(List<Claim> claims) {

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds,
                claims: claims
            );
           
            return new JwtSecurityTokenHandler().WriteToken(token);

        }
        
        private bool VerificarPasswordHash(string password, byte[] passwordHashAlmacenado, byte[] passwordSalt) {

            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var passwordHashNuevo = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return new ReadOnlySpan<byte>(passwordHashAlmacenado).SequenceEqual(new ReadOnlySpan<byte>(passwordHashNuevo));
            }
        }

        private bool usersExists(int id)
        {
            return _context.usuarios.Any(e => e.idusuarios == id);
        }
    }
}