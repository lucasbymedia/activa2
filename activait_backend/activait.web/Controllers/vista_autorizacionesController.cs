﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.vistas;
using activait.web.Models.vistas.vista_autorizaciones;

namespace activait.web.Controllers
{
        [Route("api/[controller]")]
        [ApiController]
        public class vista_autorizacionesController : ControllerBase
        {
            private readonly DBContextActivait _context;
            private IEnumerable<object> c;

            public vista_autorizacionesController(DBContextActivait context)
            {
                _context = context;
            }

            // GET: api/vista_autorizaciones/listar
            [HttpGet("[action]")]
            public async Task<IEnumerable<vista_autorizacionesViewModel>> Listar()
            {
                var vpers = await _context.vista_Autorizaciones.ToListAsync();

                return vpers.Select(c => new vista_autorizacionesViewModel
                {
                    idpersonas = c.idpersonas,
                    idciudades = c.idciudades,
                    iddepartamentos = c.iddepartamentos,
                    dep_codigo = c.dep_codigo,
                    mun_codigo = c.mun_codigo,
                    mun_nombre = c.mun_nombre,
                    ideps = c.ideps,
                    eps_codigo = c.eps_codigo,
                    eps_nombre = c.eps_nombre,
                    idempresas = c.idempresas,
                    emp_nombre = c.emp_nombre,
                    emp_nit = c.emp_nit,
                    per_tipoid = c.per_tipoid,
                    per_numeroid = c.per_numeroid,
                    per_primernombre = c.per_primernombre,
                    per_segundonombre = c.per_segundonombre,
                    per_primerapellido = c.per_primerapellido,
                    per_segundoapellido = c.per_segundoapellido,
                    per_nacimiento = c.per_nacimiento,
                    per_genero = c.per_genero,
                    idafiliados = c.idafiliados,
                    afi_tipo = c.afi_tipo,
                    afi_cronico = c.afi_cronico,
                    afi_estado = c.afi_estado,
                    afi_fechaafiliacion = c.afi_fechaafiliacion,
                    afi_fechaingreso = c.afi_fechaingreso,
                    afi_ibc = c.afi_ibc,

                    idsieniestros = c.idsieniestros,
                    sin_fecha = c.sin_fecha,
                    sin_tipoatep = c.sin_tipoatep,
                    sin_jornada = c.sin_jornada,
                    sin_laboral = c.sin_laboral,
                    sin_otra = c.sin_otra,
                    sin_codigo = c.sin_codigo,
                    sin_lugar = c.sin_lugar,
                    sin_tiempolaborprev = c.sin_tiempolaborprev,
                    sin_causal = c.sin_causal,
                    sin_muerte = c.sin_muerte,
                    sin_sitio = c.sin_sitio,
                    sin_tipolesion = c.sin_tipolesion,
                    sin_zona = c.sin_zona,
                    sin_parteafectada = c.sin_parteafectada,
                    sin_agente = c.sin_agente,
                    sin_mecanismo = c.sin_mecanismo,
                    sin_descripcion = c.sin_descripcion,


                     idautorizaciones = c.idautorizaciones,
                     aut_ipssolicitante = c.aut_ipssolicitante,
                     aut_ipsdireccionada = c.aut_ipsdireccionada,
                     aut_cuotamoderadora = c.aut_cuotamoderadora,
                     aut_copago = c.aut_copago,
                     aut_valorizacion = c.aut_valorizacion,
                     aut_codigoservicio = c.aut_codigoservicio,
                     aut_valorestimado = c.aut_valorestimado,
                     aut_estado = c.aut_estado,
                     idservicios = c.idservicios,
                     ser_codigo = c.ser_codigo,
                     ser_descripcion = c.ser_descripcion,
                     ser_cups = c.ser_cups,
                     ser_cum = c.ser_cum,
                     ser_estado = c.ser_estado,
                     ser_prioridad = c.ser_prioridad,
                     ser_pbs = c.ser_pbs,

                     iddiagnosticos = c.iddiagnosticos,
                     dia_codigo = c.dia_codigo,
                     dia_descripcion = c.dia_descripcion,
                     idips = c.idips,
                     ips_nit = c.ips_nit,
                     ips_nombre = c.ips_nombre,
                     ips_contacto = c.ips_contacto,
                     ips_direccion = c.ips_direccion,
                     ips_telefono = c.ips_telefono,
                     ips_naturaleza = c.ips_naturaleza,
                     valor_reservas = c.valor_reservas,
                     valor_servicios = c.valor_servicios,
                     idciudadeps = c.idciudadeps


                });
            }

            // GET: api/vista_autorizaciones/Mostrar/5
            [HttpGet("[action]/{id}")]
            public async Task<IEnumerable<vista_autorizacionesViewModel>> Mostrar([FromRoute] int id)
            {
                var vpers = await _context.vista_Autorizaciones
                .Where(s => s.idautorizaciones == id)
                .Take(1)
                .ToListAsync();

                return vpers.Select(c => new vista_autorizacionesViewModel
                {
                    idpersonas = c.idpersonas,
                    idciudades = c.idciudades,
                    iddepartamentos = c.iddepartamentos,
                    dep_codigo = c.dep_codigo,
                    mun_codigo = c.mun_codigo,
                    mun_nombre = c.mun_nombre,
                    ideps = c.ideps,
                    eps_codigo = c.eps_codigo,
                    eps_nombre = c.eps_nombre,
                    idempresas = c.idempresas,
                    emp_nombre = c.emp_nombre,
                    emp_nit = c.emp_nit,
                    per_tipoid = c.per_tipoid,
                    per_numeroid = c.per_numeroid,
                    per_primernombre = c.per_primernombre,
                    per_segundonombre = c.per_segundonombre,
                    per_primerapellido = c.per_primerapellido,
                    per_segundoapellido = c.per_segundoapellido,
                    per_nacimiento = c.per_nacimiento,
                    per_genero = c.per_genero,
                    idafiliados = c.idafiliados,
                    afi_tipo = c.afi_tipo,
                    afi_cronico = c.afi_cronico,
                    afi_estado = c.afi_estado,
                    afi_fechaafiliacion = c.afi_fechaafiliacion,
                    afi_fechaingreso = c.afi_fechaingreso,
                    afi_ibc = c.afi_ibc,

                    idsieniestros = c.idsieniestros,
                    sin_fecha = c.sin_fecha,
                    sin_tipoatep = c.sin_tipoatep,
                    sin_jornada = c.sin_jornada,
                    sin_laboral = c.sin_laboral,
                    sin_otra = c.sin_otra,
                    sin_codigo = c.sin_codigo,
                    sin_lugar = c.sin_lugar,
                    sin_tiempolaborprev = c.sin_tiempolaborprev,
                    sin_causal = c.sin_causal,
                    sin_muerte = c.sin_muerte,
                    sin_sitio = c.sin_sitio,
                    sin_tipolesion = c.sin_tipolesion,
                    sin_zona = c.sin_zona,
                    sin_parteafectada = c.sin_parteafectada,
                    sin_agente = c.sin_agente,
                    sin_mecanismo = c.sin_mecanismo,
                    sin_descripcion = c.sin_descripcion,

                    idautorizaciones = c.idautorizaciones,
                    aut_ipssolicitante = c.aut_ipssolicitante,
                    aut_ipsdireccionada = c.aut_ipsdireccionada,
                    aut_cuotamoderadora = c.aut_cuotamoderadora,
                    aut_copago = c.aut_copago,
                    aut_valorizacion = c.aut_valorizacion,
                    aut_codigoservicio = c.aut_codigoservicio,
                    aut_valorestimado = c.aut_valorestimado,
                    aut_estado = c.aut_estado,
                    idservicios = c.idservicios,
                    ser_codigo = c.ser_codigo,
                    ser_descripcion = c.ser_descripcion,
                    ser_cups = c.ser_cups,
                    ser_cum = c.ser_cum,
                    ser_estado = c.ser_estado,
                    ser_prioridad = c.ser_prioridad,
                    ser_pbs = c.ser_pbs,

                    iddiagnosticos = c.iddiagnosticos,
                    dia_codigo = c.dia_codigo,
                    dia_descripcion = c.dia_descripcion,
                    idips = c.idips,
                    ips_nit = c.ips_nit,
                    ips_nombre = c.ips_nombre,
                    ips_contacto = c.ips_contacto,
                    ips_direccion = c.ips_direccion,
                    ips_telefono = c.ips_telefono,
                    ips_naturaleza = c.ips_naturaleza,

                    valor_reservas = c.valor_reservas,
                    valor_servicios = c.valor_servicios,
                    idciudadeps = c.idciudadeps


                });
            }

            // GET: api/vista_autorizaciones/consulta/estado
            [HttpGet("[action]/{estado}")]
            public async Task<IEnumerable<vista_autorizacionesViewModel>> consulta([FromRoute] string estado)
            {
                var vpers = await _context.vista_Autorizaciones
                .Where(s => s.aut_estado == estado)
                .Take(15)
                .ToListAsync();

                return vpers.Select(c => new vista_autorizacionesViewModel
                {
                    idpersonas = c.idpersonas,
                    idciudades = c.idciudades,
                    iddepartamentos = c.iddepartamentos,
                    dep_codigo = c.dep_codigo,
                    mun_codigo = c.mun_codigo,
                    mun_nombre = c.mun_nombre,
                    ideps = c.ideps,
                    eps_codigo = c.eps_codigo,
                    eps_nombre = c.eps_nombre,
                    idempresas = c.idempresas,
                    emp_nombre = c.emp_nombre,
                    emp_nit = c.emp_nit,
                    per_tipoid = c.per_tipoid,
                    per_numeroid = c.per_numeroid,
                    per_primernombre = c.per_primernombre,
                    per_segundonombre = c.per_segundonombre,
                    per_primerapellido = c.per_primerapellido,
                    per_segundoapellido = c.per_segundoapellido,
                    per_nacimiento = c.per_nacimiento,
                    per_genero = c.per_genero,
                    idafiliados = c.idafiliados,
                    afi_tipo = c.afi_tipo,
                    afi_cronico = c.afi_cronico,
                    afi_estado = c.afi_estado,
                    afi_fechaafiliacion = c.afi_fechaafiliacion,
                    afi_fechaingreso = c.afi_fechaingreso,
                    afi_ibc = c.afi_ibc,

                    idsieniestros = c.idsieniestros,
                    sin_fecha = c.sin_fecha,
                    sin_tipoatep = c.sin_tipoatep,
                    sin_jornada = c.sin_jornada,
                    sin_laboral = c.sin_laboral,
                    sin_otra = c.sin_otra,
                    sin_codigo = c.sin_codigo,
                    sin_lugar = c.sin_lugar,
                    sin_tiempolaborprev = c.sin_tiempolaborprev,
                    sin_causal = c.sin_causal,
                    sin_muerte = c.sin_muerte,
                    sin_sitio = c.sin_sitio,
                    sin_tipolesion = c.sin_tipolesion,
                    sin_zona = c.sin_zona,
                    sin_parteafectada = c.sin_parteafectada,
                    sin_agente = c.sin_agente,
                    sin_mecanismo = c.sin_mecanismo,
                    sin_descripcion = c.sin_descripcion,

                    idautorizaciones = c.idautorizaciones,
                    aut_ipssolicitante = c.aut_ipssolicitante,
                    aut_ipsdireccionada = c.aut_ipsdireccionada,
                    aut_cuotamoderadora = c.aut_cuotamoderadora,
                    aut_copago = c.aut_copago,
                    aut_valorizacion = c.aut_valorizacion,
                    aut_codigoservicio = c.aut_codigoservicio,
                    aut_valorestimado = c.aut_valorestimado,
                    aut_estado = c.aut_estado,
                    idservicios = c.idservicios,
                    ser_codigo = c.ser_codigo,
                    ser_descripcion = c.ser_descripcion,
                    ser_cups = c.ser_cups,
                    ser_cum = c.ser_cum,
                    ser_estado = c.ser_estado,
                    ser_prioridad = c.ser_prioridad,
                    ser_pbs = c.ser_pbs,

                    iddiagnosticos = c.iddiagnosticos,
                    dia_codigo = c.dia_codigo,
                    dia_descripcion = c.dia_descripcion,
                    idips = c.idips,
                    ips_nit = c.ips_nit,
                    ips_nombre = c.ips_nombre,
                    ips_contacto = c.ips_contacto,
                    ips_direccion = c.ips_direccion,
                    ips_telefono = c.ips_telefono,
                    ips_naturaleza = c.ips_naturaleza,
                    idciudadeps = c.idciudadeps

                });
            }

            // GET: api/vista_autorizaciones/consulta_fechas/fechaiicio/fechafin/79932332/1
            [HttpGet("[action]/{fechainicio}/{fechafin}/{ced}/{pagina}")]
            public async Task<IEnumerable<vista_autorizacionesViewModel>> consulta_fechas([FromRoute] DateTime fechainicio, DateTime fechafin, string ced, int pagina)
            {
                var vpers = await _context.vista_Autorizaciones
                .Where(s => s.sin_fecha >= fechainicio)
                .Where(s => s.sin_fecha <= fechafin)
//                .Skip(50 * (pagina - 1))
                .Take(15)
                .ToListAsync();

                if (ced != "0") {
                    vpers = await _context.vista_Autorizaciones
                    .Where(s => s.sin_fecha >= fechainicio)
                    .Where(s => s.sin_fecha <= fechafin)
                    .Where(s => s.per_numeroid == ced)
                    .Skip(50 * (pagina - 1))
                    .Take(50)
                    .ToListAsync();
                }

                return vpers.Select(c => new vista_autorizacionesViewModel
                {
                    idpersonas = c.idpersonas,
                    idciudades = c.idciudades,
                    iddepartamentos = c.iddepartamentos,
                    dep_codigo = c.dep_codigo,
                    mun_codigo = c.mun_codigo,
                    mun_nombre = c.mun_nombre,
                    ideps = c.ideps,
                    eps_codigo = c.eps_codigo,
                    eps_nombre = c.eps_nombre,
                    idempresas = c.idempresas,
                    emp_nombre = c.emp_nombre,
                    emp_nit = c.emp_nit,
                    per_tipoid = c.per_tipoid,
                    per_numeroid = c.per_numeroid,
                    per_primernombre = c.per_primernombre,
                    per_segundonombre = c.per_segundonombre,
                    per_primerapellido = c.per_primerapellido,
                    per_segundoapellido = c.per_segundoapellido,
                    per_nacimiento = c.per_nacimiento,
                    per_genero = c.per_genero,
                    idafiliados = c.idafiliados,
                    afi_tipo = c.afi_tipo,
                    afi_cronico = c.afi_cronico,
                    afi_estado = c.afi_estado,
                    afi_fechaafiliacion = c.afi_fechaafiliacion,
                    afi_fechaingreso = c.afi_fechaingreso,
                    afi_ibc = c.afi_ibc,

                    idsieniestros = c.idsieniestros,
                    sin_fecha = c.sin_fecha,
                    sin_tipoatep = c.sin_tipoatep,
                    sin_jornada = c.sin_jornada,
                    sin_laboral = c.sin_laboral,
                    sin_otra = c.sin_otra,
                    sin_codigo = c.sin_codigo,
                    sin_lugar = c.sin_lugar,
                    sin_tiempolaborprev = c.sin_tiempolaborprev,
                    sin_causal = c.sin_causal,
                    sin_muerte = c.sin_muerte,
                    sin_sitio = c.sin_sitio,
                    sin_tipolesion = c.sin_tipolesion,
                    sin_zona = c.sin_zona,
                    sin_parteafectada = c.sin_parteafectada,
                    sin_agente = c.sin_agente,
                    sin_mecanismo = c.sin_mecanismo,
                    sin_descripcion = c.sin_descripcion,

                    idautorizaciones = c.idautorizaciones,
                    aut_ipssolicitante = c.aut_ipssolicitante,
                    aut_ipsdireccionada = c.aut_ipsdireccionada,
                    aut_cuotamoderadora = c.aut_cuotamoderadora,
                    aut_copago = c.aut_copago,
                    aut_valorizacion = c.aut_valorizacion,
                    aut_codigoservicio = c.aut_codigoservicio,
                    aut_valorestimado = c.aut_valorestimado,
                    aut_estado = c.aut_estado,
                    idservicios = c.idservicios,
                    ser_codigo = c.ser_codigo,
                    ser_descripcion = c.ser_descripcion,
                    ser_cups = c.ser_cups,
                    ser_cum = c.ser_cum,
                    ser_estado = c.ser_estado,
                    ser_prioridad = c.ser_prioridad,
                    ser_pbs = c.ser_pbs,

                    iddiagnosticos = c.iddiagnosticos,
                    dia_codigo = c.dia_codigo,
                    dia_descripcion = c.dia_descripcion,
                    idips = c.idips,
                    ips_nit = c.ips_nit,
                    ips_nombre = c.ips_nombre,
                    ips_contacto = c.ips_contacto,
                    ips_direccion = c.ips_direccion,
                    ips_telefono = c.ips_telefono,
                    ips_naturaleza = c.ips_naturaleza,
                    valor_reservas = c.valor_reservas,
                    valor_servicios = c.valor_servicios,
                    idciudadeps = c.idciudadeps
                
                });
            }

    }
}
