﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.vistas;
using activait.web.Models.vistas.vista_personas;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class vista_personasController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public vista_personasController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/vista_personas/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<vista_personasViewModel>> Listar()
        {
            var vpers = await _context.vista_personas.ToListAsync();

            return vpers.Select(c => new vista_personasViewModel
            {
                idpersonas = c.idpersonas,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre,
                idempresas = c.idempresas,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_representante = c.emp_representante,
                emp_direccion = c.emp_direccion,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                per_tipovinculacion = c.per_tipovinculacion,
                per_direccion=c.per_direccion,
                per_cargo=c.per_cargo,
                per_telefono=c.per_telefono,
                emp_actividad = c.emp_actividad,
                emp_codactividad = c.emp_codactividad,
                idafiliados = c.idafiliados,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion,
                idafps=c.idafps,
                afp_codigo=c.afp_codigo,
                afp_nombre=c.afp_nombre,
                idarls=c.idarls,
                arl_codigo=c.arl_codigo,
                arl_nombre=c.arl_nombre
            });
        }

        // GET: api/vista_personas/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var vpers = await _context.vista_personas.FindAsync(id);
            if (vpers == null)
            {
                return NotFound();
            }
            return Ok(new vista_personasViewModel
            {
                idpersonas = vpers.idpersonas,
                idciudades = vpers.idciudades,
                iddepartamentos = vpers.iddepartamentos,
                dep_codigo = vpers.dep_codigo,
                mun_codigo = vpers.mun_codigo,
                mun_nombre = vpers.mun_nombre,
                ideps = vpers.ideps,
                eps_codigo = vpers.eps_codigo,
                eps_nombre = vpers.eps_nombre,
                idempresas = vpers.idempresas,
                emp_nombre = vpers.emp_nombre,
                emp_nit = vpers.emp_nit,
                emp_email = vpers.emp_email,
                emp_telefono = vpers.emp_telefono,
                emp_representante = vpers.emp_representante,
                emp_direccion = vpers.emp_direccion,
                per_tipoid = vpers.per_tipoid,
                per_numeroid = vpers.per_numeroid,
                per_primernombre = vpers.per_primernombre,
                per_segundonombre = vpers.per_segundonombre,
                per_primerapellido = vpers.per_primerapellido,
                per_segundoapellido = vpers.per_segundoapellido,
                per_nacimiento = vpers.per_nacimiento,
                per_genero = vpers.per_genero,
                per_tipovinculacion = vpers.per_tipovinculacion,
                per_telefono=vpers.per_telefono,
                per_cargo=vpers.per_cargo,
                per_direccion=vpers.per_direccion,
                emp_actividad = vpers.emp_actividad,
                emp_codactividad = vpers.emp_codactividad,
                idafiliados = vpers.idafiliados,
                afi_tipo = vpers.afi_tipo,
                afi_cronico = vpers.afi_cronico,
                afi_estado = vpers.afi_estado,
                afi_fechaafiliacion = vpers.afi_fechaafiliacion,
                afi_fechaingreso = vpers.afi_fechaingreso,
                afi_ibc = vpers.afi_ibc,
                afi_tipovinculacion = vpers.afi_tipovinculacion,
                idafps = vpers.idafps,
                afp_codigo = vpers.afp_codigo,
                afp_nombre = vpers.afp_nombre,
                idarls = vpers.idarls,
                arl_codigo = vpers.arl_codigo,
                arl_nombre = vpers.arl_nombre
            });
        }

        // GET: api/vista_personas/Consulta/5
        [HttpGet("[action]/{documento}")]
        public async Task<IActionResult> Consulta([FromRoute] string documento)
        {
            var vpers = await _context.vista_personas.SingleOrDefaultAsync(
                a => a.per_numeroid == documento);

            if (vpers == null)
            {
                return NotFound();
            }
            return Ok(new vista_personasViewModel
            {
                idpersonas = vpers.idpersonas,
                idciudades = vpers.idciudades,
                iddepartamentos = vpers.iddepartamentos,
                dep_codigo = vpers.dep_codigo,
                mun_codigo = vpers.mun_codigo,
                mun_nombre = vpers.mun_nombre,
                ideps = vpers.ideps,
                eps_codigo = vpers.eps_codigo,
                eps_nombre = vpers.eps_nombre,
                idempresas = vpers.idempresas,
                emp_nombre = vpers.emp_nombre,
                emp_nit = vpers.emp_nit,
                emp_email = vpers.emp_email,
                emp_telefono = vpers.emp_telefono,
                emp_representante = vpers.emp_representante,
                emp_direccion = vpers.emp_direccion,
                per_tipoid = vpers.per_tipoid,
                per_numeroid = vpers.per_numeroid,
                per_primernombre = vpers.per_primernombre,
                per_segundonombre = vpers.per_segundonombre,
                per_primerapellido = vpers.per_primerapellido,
                per_segundoapellido = vpers.per_segundoapellido,
                per_nacimiento = vpers.per_nacimiento,
                per_genero = vpers.per_genero,
                per_tipovinculacion=vpers.per_tipovinculacion,
                per_telefono = vpers.per_telefono,
                per_cargo = vpers.per_cargo,
                per_direccion = vpers.per_direccion,
                emp_actividad = vpers.emp_actividad,
                emp_codactividad = vpers.emp_codactividad,
                idafiliados = vpers.idafiliados,
                afi_tipo = vpers.afi_tipo,
                afi_cronico = vpers.afi_cronico,
                afi_estado = vpers.afi_estado,
                afi_fechaafiliacion = vpers.afi_fechaafiliacion,
                afi_fechaingreso = vpers.afi_fechaingreso,
                afi_ibc = vpers.afi_ibc,
                afi_tipovinculacion = vpers.afi_tipovinculacion,
                idafps = vpers.idafps,
                afp_codigo = vpers.afp_codigo,
                afp_nombre = vpers.afp_nombre,
                idarls = vpers.idarls,
                arl_codigo = vpers.arl_codigo,
                arl_nombre = vpers.arl_nombre
            });
        }

        // GET: api/vista_personas/Consulta2/5/5
        [HttpGet("[action]/{iddep}/{ideps}")]
        public async Task<IEnumerable<vista_personasViewModel>> Consulta2([FromRoute] int iddep, int ideps)
        {
            var vpers = await _context.vista_personas
                   .OrderByDescending(s => s.afi_fechaafiliacion)
                   .Take(100)
                   .ToListAsync();

            if (iddep > 0 && ideps > 0)
            {
                vpers = await _context.vista_personas
                   .Where(s => s.iddepartamentos == iddep)
                   .Where(s => s.ideps == ideps)
                   .OrderByDescending(s => s.afi_fechaafiliacion)
                   .Take(100)
                   .ToListAsync();
            }
            else if(iddep > 0 && ideps == 0) {
                vpers = await _context.vista_personas
                   .OrderByDescending(s => s.afi_fechaafiliacion)
                   .Where(s => s.iddepartamentos == iddep)
                   .Take(100)
                   .ToListAsync();
            }
            else if (iddep == 0 && ideps > 0)
            {
                vpers = await _context.vista_personas
                   .OrderByDescending(s => s.afi_fechaafiliacion)
                   .Where(s => s.ideps == ideps)
                   .Take(100)
                   .ToListAsync();
            }

            return vpers.Select(c => new vista_personasViewModel
            {
                idpersonas = c.idpersonas,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre,
                idempresas = c.idempresas,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_representante = c.emp_representante,
                emp_direccion = c.emp_direccion,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                per_tipovinculacion = c.per_tipovinculacion,
                per_telefono = c.per_telefono,
                per_cargo = c.per_cargo,
                per_direccion = c.per_direccion,
                emp_actividad = c.emp_actividad,
                emp_codactividad = c.emp_codactividad,
                idafiliados = c.idafiliados,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion,
                idafps = c.idafps,
                afp_codigo = c.afp_codigo,
                afp_nombre = c.afp_nombre,
                idarls = c.idarls,
                arl_codigo = c.arl_codigo,
                arl_nombre = c.arl_nombre
            });
        }


        private bool vista_personasExists(int id)
        {
            return _context.vista_personas.Any(e => e.idpersonas == id);
        }
    }
}