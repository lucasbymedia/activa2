﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.vistas;
using activait.web.Models.vistas.vista_servicios;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class vista_serviciosController : ControllerBase
    {
        private readonly DBContextActivait _context;

        public vista_serviciosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/vista_servicios/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<vista_serviciosViewModel>> Listar()
        {
            var ser = await _context.vista_Servicios.ToListAsync();

            return ser.Select(c => new vista_serviciosViewModel
            {
                idsercontratados = c.idsercontratados,
                idips = c.idips,
                idservicios = c.idservicios,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                sec_fechainicio = c.sec_fechainicio,
                sec_fechafin = c.sec_fechafin,
                sec_estado = c.sec_estado,
                sec_valor = c.sec_valor,
                sec_prioridadasign = c.sec_prioridadasign,
                sec_prioridadcosto = c.sec_prioridadcosto,
                ser_codigo = c.ser_codigo,
                ser_descripcion = c.ser_descripcion,
                ser_cups = c.ser_cups,
                ser_cum = c.ser_cum,
                ser_estado = c.ser_estado,
                ser_pbs = c.ser_pbs,
                ser_prioridad = c.ser_prioridad,
                ips_nombre = c.ips_nombre,
                ips_nit = c.ips_nit,
                ips_contacto = c.ips_contacto,
                ips_naturaleza = c.ips_naturaleza,
                ips_direccion = c.ips_direccion,
                ips_telefono = c.ips_telefono,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                dep_codigo = c.dep_codigo,
                dep_nombre = c.dep_nombre,
                
            });
        }

        // GET: api/vista_servicios/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var ser = await _context.vista_Servicios.FindAsync(id);
            if (ser == null)
            {
                return NotFound();
            }
            return Ok(new vista_serviciosViewModel
            {
                idsercontratados = ser.idsercontratados,
                idips = ser.idips,
                idservicios = ser.idservicios,
                idciudades = ser.idciudades,
                iddepartamentos = ser.iddepartamentos,
                sec_fechainicio = ser.sec_fechainicio,
                sec_fechafin = ser.sec_fechafin,
                sec_estado = ser.sec_estado,
                sec_valor = ser.sec_valor,
                sec_prioridadasign = ser.sec_prioridadasign,
                sec_prioridadcosto = ser.sec_prioridadcosto,
                ser_codigo = ser.ser_codigo,
                ser_descripcion = ser.ser_descripcion,
                ser_cups = ser.ser_cups,
                ser_cum = ser.ser_cum,
                ser_estado = ser.ser_estado,
                ser_pbs = ser.ser_pbs,
                ser_prioridad = ser.ser_prioridad,
                ips_nombre = ser.ips_nombre,
                ips_nit = ser.ips_nit,
                ips_contacto = ser.ips_contacto,
                ips_naturaleza = ser.ips_naturaleza,
                ips_direccion = ser.ips_direccion,
                ips_telefono = ser.ips_telefono,
                mun_codigo = ser.mun_codigo,
                mun_nombre = ser.mun_nombre,
                dep_codigo = ser.dep_codigo,
                dep_nombre = ser.dep_nombre
            });
        }

        // GET: api/vista_servicios/Consulta/5
        [HttpGet("[action]/{idips}")]
        public async Task<IEnumerable<vista_serviciosViewModel>> Consulta([FromRoute] int idips)
        {
            var ser = await _context.vista_Servicios
                   .OrderByDescending(s => s.sec_fechainicio)
                   .Take(100)
                   .ToListAsync();

            if (idips > 0)
            {
                ser = await _context.vista_Servicios
                   .Where(s => s.idips == idips)
                   .OrderByDescending(s => s.sec_fechainicio)
                   .Take(100)
                   .ToListAsync();
            }

            return ser.Select(c => new vista_serviciosViewModel
            {
                idsercontratados = c.idsercontratados,
                idips = c.idips,
                idservicios = c.idservicios,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                sec_fechainicio = c.sec_fechainicio,
                sec_fechafin = c.sec_fechafin,
                sec_estado = c.sec_estado,
                sec_valor = c.sec_valor,
                sec_prioridadasign = c.sec_prioridadasign,
                sec_prioridadcosto = c.sec_prioridadcosto,
                ser_codigo = c.ser_codigo,
                ser_descripcion = c.ser_descripcion,
                ser_cups = c.ser_cups,
                ser_cum = c.ser_cum,
                ser_estado = c.ser_estado,
                ser_pbs = c.ser_pbs,
                ser_prioridad = c.ser_prioridad,
                ips_nombre = c.ips_nombre,
                ips_nit = c.ips_nit,
                ips_contacto = c.ips_contacto,
                ips_naturaleza = c.ips_naturaleza,
                ips_direccion = c.ips_direccion,
                ips_telefono = c.ips_telefono,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                dep_codigo = c.dep_codigo,
                dep_nombre = c.dep_nombre
            });
        }


        private bool vista_serviciosExists(int id)
        {
            return _context.vista_Servicios.Any(e => e.idsercontratados == id);
        }
    }
}