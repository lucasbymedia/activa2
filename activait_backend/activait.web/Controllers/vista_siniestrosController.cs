﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using activait.datos;
using activait.entidades.vistas;
using activait.web.Models.vistas.vista_siniestros;

namespace activait.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class vista_siniestrosController : ControllerBase
    {
        private readonly DBContextActivait _context;
        private IEnumerable<object> vpers;

        public vista_siniestrosController(DBContextActivait context)
        {
            _context = context;
        }

        // GET: api/vista_siniestros/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<vista_siniestrosViewModel>> Listar()
        {
            var vpers = await _context.vista_siniestros.ToListAsync();

            return vpers.Select(c => new vista_siniestrosViewModel
            {
                idpersonas = c.idpersonas,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre,
                idempresas = c.idempresas,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_representante = c.emp_representante,
                emp_direccion = c.emp_direccion,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                idafiliados = c.idafiliados,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion, 

                idsieniestros = c.idsieniestros,
                sin_fecha = c.sin_fecha,
                sin_tipoatep = c.sin_tipoatep,
                sin_jornada = c.sin_jornada,
                sin_laboral = c.sin_laboral,
                sin_otra = c.sin_otra,
                sin_codigo = c.sin_codigo,
                sin_lugar = c.sin_lugar,
                sin_tiempolaborprev = c.sin_tiempolaborprev,
                sin_causal = c.sin_causal,
                sin_muerte = c.sin_muerte,
                sin_sitio = c.sin_sitio,
                sin_tipolesion = c.sin_tipolesion,
                sin_zona = c.sin_zona,
                sin_parteafectada = c.sin_parteafectada,
                sin_agente = c.sin_agente,
                sin_mecanismo = c.sin_mecanismo,
                sin_descripcion = c.sin_descripcion,
                sin_responsable = c.sin_responsable,
                sin_nombreresponsable = c.sin_nombreresponsable,
                sin_cargoresponsable = c.sin_cargoresponsable,
                sin_documentoresponsable = c.sin_documentoresponsable,
                sin_fechadiagnostico = c.sin_fechadiagnostico,
                sin_diagnosticadapor = c.sin_diagnosticadapor,
                sin_diagnostico = c.sin_diagnostico,
                sin_regmedico = c.sin_regmedico,
                sin_nombremedico = c.sin_nombremedico,
                sin_evalmedicapre = c.sin_evalmedicapre,
                sin_evalmedicaper = c.sin_evalmedicaper,
                sin_evalmedicaegre = c.sin_evalmedicaegre,
                sin_severidad = c.sin_severidad,
                sin_diasinc = c.sin_diasinc,
                sin_ipp = c.sin_ipp,
                sin_valorrta = c.sin_valorrta,
                sin_valorrti = c.sin_valorrti,
                sin_estado = c.sin_estado


    });
        }

        // GET: api/vista_siniestros/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var vpers = await _context.vista_siniestros.FindAsync(id);
            if (vpers == null)
            {
                return NotFound();
            }
            return Ok(new vista_siniestrosViewModel
            {
                idpersonas = vpers.idpersonas,
                idciudades = vpers.idciudades,
                iddepartamentos = vpers.iddepartamentos,
                dep_codigo = vpers.dep_codigo,
                mun_codigo = vpers.mun_codigo,
                mun_nombre = vpers.mun_nombre,
                ideps = vpers.ideps,
                eps_codigo = vpers.eps_codigo,
                eps_nombre = vpers.eps_nombre,
                idempresas = vpers.idempresas,
                emp_nombre = vpers.emp_nombre,
                emp_nit = vpers.emp_nit,
                emp_email = vpers.emp_email,
                emp_telefono = vpers.emp_telefono,
                emp_representante = vpers.emp_representante,
                emp_direccion = vpers.emp_direccion,
                per_tipoid = vpers.per_tipoid,
                per_numeroid = vpers.per_numeroid,
                per_primernombre = vpers.per_primernombre,
                per_segundonombre = vpers.per_segundonombre,
                per_primerapellido = vpers.per_primerapellido,
                per_segundoapellido = vpers.per_segundoapellido,
                per_nacimiento = vpers.per_nacimiento,
                per_genero = vpers.per_genero,
                idafiliados = vpers.idafiliados,
                afi_tipo = vpers.afi_tipo,
                afi_cronico = vpers.afi_cronico,
                afi_estado = vpers.afi_estado,
                afi_fechaafiliacion = vpers.afi_fechaafiliacion,
                afi_fechaingreso = vpers.afi_fechaingreso,
                afi_ibc = vpers.afi_ibc,
                afi_tipovinculacion = vpers.afi_tipovinculacion,

                idsieniestros = vpers.idsieniestros,
                sin_fecha = vpers.sin_fecha,
                sin_tipoatep = vpers.sin_tipoatep,
                sin_jornada = vpers.sin_jornada,
                sin_laboral = vpers.sin_laboral,
                sin_otra = vpers.sin_otra,
                sin_codigo = vpers.sin_codigo,
                sin_lugar = vpers.sin_lugar,
                sin_tiempolaborprev = vpers.sin_tiempolaborprev,
                sin_causal = vpers.sin_causal,
                sin_muerte = vpers.sin_muerte,
                sin_sitio = vpers.sin_sitio,
                sin_tipolesion = vpers.sin_tipolesion,
                sin_zona = vpers.sin_zona,
                sin_parteafectada = vpers.sin_parteafectada,
                sin_agente = vpers.sin_agente,
                sin_mecanismo = vpers.sin_mecanismo,
                sin_descripcion = vpers.sin_descripcion,
                sin_responsable = vpers.sin_responsable,
                sin_nombreresponsable = vpers.sin_nombreresponsable,
                sin_cargoresponsable = vpers.sin_cargoresponsable,
                sin_documentoresponsable = vpers.sin_documentoresponsable,
                sin_fechadiagnostico = vpers.sin_fechadiagnostico,
                sin_diagnosticadapor = vpers.sin_diagnosticadapor,
                sin_diagnostico = vpers.sin_diagnostico,
                sin_regmedico = vpers.sin_regmedico,
                sin_nombremedico = vpers.sin_nombremedico,
                sin_evalmedicapre = vpers.sin_evalmedicapre,
                sin_evalmedicaper = vpers.sin_evalmedicaper,
                sin_evalmedicaegre = vpers.sin_evalmedicaegre,
                sin_severidad = vpers.sin_severidad,
                sin_diasinc = vpers.sin_diasinc,
                sin_ipp = vpers.sin_ipp,
                sin_valorrta = vpers.sin_valorrta,
                sin_valorrti = vpers.sin_valorrti,
                sin_estado = vpers.sin_estado


            });
        }

        // GET: api/vista_siniestros/Consulta_estado/RESERVADO/fechaiicio/fechafin/1
        [HttpGet("[action]/{estado}/{fechainicio}/{fechafin}/{pagina}")]
        public async Task<IEnumerable<vista_siniestrosViewModel>> Consulta_estado(string estado, DateTime fechainicio, DateTime fechafin, int pagina)
        {

            var vpers = await _context.vista_siniestros
               .Where(s=>s.sin_fecha >= fechainicio)
               .Where(s=>s.sin_fecha <= fechafin)
               .Where(s=>s.sin_estado == estado)
               .OrderByDescending(s => s.sin_fecha)
               .Skip(50 * (pagina - 1))
               .Take(50)
               .ToListAsync();

            return vpers.Select(c => new vista_siniestrosViewModel
            {
                idpersonas = c.idpersonas,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre,
                idempresas = c.idempresas,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_representante = c.emp_representante,
                emp_direccion = c.emp_direccion,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                idafiliados = c.idafiliados,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion,

                idsieniestros = c.idsieniestros,
                sin_fecha = c.sin_fecha,
                sin_tipoatep = c.sin_tipoatep,
                sin_jornada = c.sin_jornada,
                sin_laboral = c.sin_laboral,
                sin_otra = c.sin_otra,
                sin_codigo = c.sin_codigo,
                sin_lugar = c.sin_lugar,
                sin_tiempolaborprev = c.sin_tiempolaborprev,
                sin_causal = c.sin_causal,
                sin_muerte = c.sin_muerte,
                sin_sitio = c.sin_sitio,
                sin_tipolesion = c.sin_tipolesion,
                sin_zona = c.sin_zona,
                sin_parteafectada = c.sin_parteafectada,
                sin_agente = c.sin_agente,
                sin_mecanismo = c.sin_mecanismo,
                sin_descripcion = c.sin_descripcion,
                sin_responsable = c.sin_responsable,
                sin_nombreresponsable = c.sin_nombreresponsable,
                sin_cargoresponsable = c.sin_cargoresponsable,
                sin_documentoresponsable = c.sin_documentoresponsable,
                sin_fechadiagnostico = c.sin_fechadiagnostico,
                sin_diagnosticadapor = c.sin_diagnosticadapor,
                sin_diagnostico = c.sin_diagnostico,
                sin_regmedico = c.sin_regmedico,
                sin_nombremedico = c.sin_nombremedico,
                sin_evalmedicapre = c.sin_evalmedicapre,
                sin_evalmedicaper = c.sin_evalmedicaper,
                sin_evalmedicaegre = c.sin_evalmedicaegre,
                sin_severidad = c.sin_severidad,
                sin_diasinc = c.sin_diasinc,
                sin_ipp = c.sin_ipp,
                sin_valorrta = c.sin_valorrta,
                sin_valorrti = c.sin_valorrti,
                sin_estado = c.sin_estado


            });
        }

        // GET: api/vista_siniestros/Muestra/id
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<vista_siniestrosViewModel>> Muestra(int id)
        {
            var vpers = await _context.vista_siniestros
               .Where(s => s.idsieniestros == id)
               .Take(2)
               .ToListAsync();

            return vpers.Select(c => new vista_siniestrosViewModel
            {
                idpersonas = c.idpersonas,
                idciudades = c.idciudades,
                iddepartamentos = c.iddepartamentos,
                dep_codigo = c.dep_codigo,
                mun_codigo = c.mun_codigo,
                mun_nombre = c.mun_nombre,
                ideps = c.ideps,
                eps_codigo = c.eps_codigo,
                eps_nombre = c.eps_nombre,
                idempresas = c.idempresas,
                emp_nombre = c.emp_nombre,
                emp_nit = c.emp_nit,
                emp_email = c.emp_email,
                emp_telefono = c.emp_telefono,
                emp_representante = c.emp_representante,
                emp_direccion = c.emp_direccion,
                per_tipoid = c.per_tipoid,
                per_numeroid = c.per_numeroid,
                per_primernombre = c.per_primernombre,
                per_segundonombre = c.per_segundonombre,
                per_primerapellido = c.per_primerapellido,
                per_segundoapellido = c.per_segundoapellido,
                per_nacimiento = c.per_nacimiento,
                per_genero = c.per_genero,
                idafiliados = c.idafiliados,
                afi_tipo = c.afi_tipo,
                afi_cronico = c.afi_cronico,
                afi_estado = c.afi_estado,
                afi_fechaafiliacion = c.afi_fechaafiliacion,
                afi_fechaingreso = c.afi_fechaingreso,
                afi_ibc = c.afi_ibc,
                afi_tipovinculacion = c.afi_tipovinculacion,

                idsieniestros = c.idsieniestros,
                sin_fecha = c.sin_fecha,
                sin_tipoatep = c.sin_tipoatep,
                sin_jornada = c.sin_jornada,
                sin_laboral = c.sin_laboral,
                sin_otra = c.sin_otra,
                sin_codigo = c.sin_codigo,
                sin_lugar = c.sin_lugar,
                sin_tiempolaborprev = c.sin_tiempolaborprev,
                sin_causal = c.sin_causal,
                sin_muerte = c.sin_muerte,
                sin_sitio = c.sin_sitio,
                sin_tipolesion = c.sin_tipolesion,
                sin_zona = c.sin_zona,
                sin_parteafectada = c.sin_parteafectada,
                sin_agente = c.sin_agente,
                sin_mecanismo = c.sin_mecanismo,
                sin_descripcion = c.sin_descripcion,
                sin_responsable = c.sin_responsable,
                sin_nombreresponsable = c.sin_nombreresponsable,
                sin_cargoresponsable = c.sin_cargoresponsable,
                sin_documentoresponsable = c.sin_documentoresponsable,
                sin_fechadiagnostico = c.sin_fechadiagnostico,
                sin_diagnosticadapor = c.sin_diagnosticadapor,
                sin_diagnostico = c.sin_diagnostico,
                sin_regmedico = c.sin_regmedico,
                sin_nombremedico = c.sin_nombremedico,
                sin_evalmedicapre = c.sin_evalmedicapre,
                sin_evalmedicaper = c.sin_evalmedicaper,
                sin_evalmedicaegre = c.sin_evalmedicaegre,
                sin_severidad = c.sin_severidad,
                sin_diasinc = c.sin_diasinc,
                sin_ipp = c.sin_ipp,
                sin_valorrta = c.sin_valorrta,
                sin_valorrti = c.sin_valorrti,
                sin_estado = c.sin_estado


            });
        }

        private bool vista_siniestrosExists(int id)
        {
            return _context.vista_siniestros.Any(e => e.idsieniestros == id);
        }
    }
}