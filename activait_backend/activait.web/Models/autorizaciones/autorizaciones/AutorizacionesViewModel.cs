﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.autorizaciones
{
    public class AutorizacionesViewModel
    {
        public int idautorizaciones { get; set; }
        public int siniestros_idsiniestos { get; set; }
        public int servicios_idservicios { get; set; }
        public int diagnosticos_iddiagnosticos { get; set; }
        public string aut_ipssolicitante { get; set; }
        public string aut_ipsdireccionada { get; set; }
        public int aut_cuotamoderadora { get; set; }
        public string aut_copago { get; set; }
        public string aut_valorizacion { get; set; }
        public string aut_codigoservicio { get; set; }
        public int aut_valorestimado { get; set; }
        public string aut_estado { get; set; }


    }
}
