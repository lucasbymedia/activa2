﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.factoresr
{
    public class FactoresrViewModel
    {
        public int idfactoresr { get; set; }
        public int factores_idfactores_riesgo { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public string fac_clase { get; set; }
        public string fac_descripcion { get; set; }
        public string fac_tiempoexpactual { get; set; }
        public string fac_tiempoexpanteri { get; set; }
    }
}
