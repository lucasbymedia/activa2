﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.web.Models.autorizaciones.factoresr
{
    public class farActualizarViewModel
    {
        public int idfactoresr { get; set; }
        [Required]
        public int factores_idfactores_riesgo { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string fac_clase { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string fac_descripcion { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string fac_tiempoexpactual { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string fac_tiempoexpanteri { get; set; }

        public Nullable<DateTime> fac_fechamodificacion { get; set; }
        public Nullable<int> fac_usumodificacion { get; set; }
    }
}
