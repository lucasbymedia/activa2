﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.monitoreos
{
    public class MonitoreosViewModel
    {
        public int idmonitoreos { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public int tipos_idtipos_monitoreo { get; set; }
        public string mon_respuesta { get; set; }
        public string mon_resultado { get; set; }
        public DateTime mon_fecha { get; set; }

        public Nullable<DateTime> mon_fechamodificacion { get; set; }
        public Nullable<int> mon_usumodificacion { get; set; }

    }
}
