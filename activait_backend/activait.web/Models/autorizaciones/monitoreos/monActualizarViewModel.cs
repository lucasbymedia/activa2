﻿using System;
using System.ComponentModel.DataAnnotations;


namespace activait.web.Models.autorizaciones.monitoreos
{
    public class monActualizarViewModel
    {
        public int idmonitoreos { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        public int tipos_idtipos_monitoreo { get; set; }
        [Required]
        [StringLength(maximumLength: 200, ErrorMessage = "Debe tener máximo 200 caracteres", MinimumLength = 1)]
        public string mon_respuesta { get; set; }
        [Required]
        [StringLength(maximumLength: 200, ErrorMessage = "Debe tener máximo 200 caracteres", MinimumLength = 1)]
        public string mon_resultado { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 1)]
        public DateTime mon_fecha { get; set; }

        public Nullable<DateTime> mon_fechamodificacion { get; set; }
        public Nullable<int> mon_usumodificacion { get; set; }

    }
}
