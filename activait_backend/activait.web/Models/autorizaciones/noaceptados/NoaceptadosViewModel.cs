﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.noaceptados
{
    public class NoaceptadosViewModel
    {
        public int idnoaceptados { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public string noa_tipoaccion { get; set; }
        public DateTime noa_fecha { get; set; }
        public string noa_descripcion { get; set; }
    }
}
