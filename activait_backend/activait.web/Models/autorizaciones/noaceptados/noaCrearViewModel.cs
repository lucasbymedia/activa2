﻿using System;
using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.autorizaciones.noaceptados
{
    public class noaCrearViewModel
    {
         int idnoaceptados { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string noa_tipoaccion { get; set; }
        [Required]
        public DateTime noa_fecha { get; set; }
        [Required]
        [StringLength(maximumLength: 200, ErrorMessage = "Debe tener máximo 200 caracteres", MinimumLength = 1)]
        public string noa_descripcion { get; set; }

        public Nullable<DateTime> noa_fechacreacion { get; set; }
        public Nullable<int> noa_usucreacion { get; set; }

    }
}
