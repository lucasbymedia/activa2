﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.novedades
{
    public class NovedadesViewModel
    {
        public int idnovedades { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public int testigos_idtestigos { get; set; }
        public string nov_codigo { get; set; }
        public int nov_datooriginal { get; set; }
        public int nov_datonuevo { get; set; }
    }
}
