﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.web.Models.autorizaciones.novedades
{
    public class novActualizarViewModel
    {
        public int idnovedades { get; set; }
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        public int testigos_idtestigos { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 1)]
        public string nov_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 200, ErrorMessage = "Debe tener máximo 200 caracteres", MinimumLength = 1)]
        public int nov_datooriginal { get; set; }
        [Required]
        [StringLength(maximumLength: 200, ErrorMessage = "Debe tener máximo 200 caracteres", MinimumLength = 1)]
        public int nov_datonuevo { get; set; }

        public Nullable<DateTime> nov_fechamodificacion { get; set; }
        public Nullable<int> nov_usumodificacion { get; set; }

    }
}
