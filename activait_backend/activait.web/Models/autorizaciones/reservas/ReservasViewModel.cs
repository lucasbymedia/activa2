﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.reservas
{
    public class ReservasViewModel
    {
        public int idreservas { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public string res_codigo { get; set; }
        public int res_valor { get; set; }
        public int res_acumulado { get; set; }
        public string res_autorizacion { get; set; }
        public DateTime res_fecha { get; set; }
        public string res_estado { get; set; }
    }
}
