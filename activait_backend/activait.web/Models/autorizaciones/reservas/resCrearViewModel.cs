﻿using System.ComponentModel.DataAnnotations;
using System;

namespace activait.web.Models.autorizaciones.reservas
{
    public class resCrearViewModel
    {
        [Required]
        public int siniestros_idsiniestros { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 1)]
        public string res_codigo { get; set; }
        [Required]
        public int res_valor { get; set; }
        [Required]
        public int res_acumulado { get; set; }
        [Required]
        public DateTime res_fecha { get; set; }
        public string res_autorizacion { get; set; }
        public string res_estado { get; set; }

        public Nullable<DateTime> res_fechacreacion { get; set; }
        public Nullable<int> res_usucreacion { get; set; }

    }
}
