﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.siniestros
{
    public class SiniestrosViewModel
    {
        public int idsieniestros { get; set; }
        public int personas_idpersonas { get; set; }
        public int ciudades_idciudades { get; set; }
        public string sin_tipoatep { get; set; }
        public string sin_fecha { get; set; }
        public string sin_jornada { get; set; }
        public string sin_laboral { get; set; }
        public string sin_otra { get; set; }
        
        public string sin_codigo { get; set; }
        public string sin_tiempolaborprev { get; set; }
        public string sin_causal { get; set; }
        public string sin_muerte { get; set; }
        public string sin_sitio { get; set; }
        public string sin_tipolesion { get; set; }
        public string sin_parteafectada { get; set; }
        public string sin_agente { get; set; }

        public string sin_lugar { get; set; }

        public string sin_mecanismo { get; set; }
        public string sin_descripcion { get; set; }
        public string sin_responsable { get; set; }
        public string sin_nombreresponsable { get; set; }

        public string sin_cargoresponsable { get; set; }
        public string sin_documentoresponsable { get; set; }
        public DateTime sin_fechadiagnostico { get; set; }
        public string sin_diagnosticadapor { get; set; }
        public string sin_diagnostico { get; set; }

        public string sin_regmedico { get; set; }
        public string sin_nombremedico { get; set; }
        public string sin_evalmedicapre { get; set; }
        public string sin_evalmedicaper { get; set; }
        public string sin_evalmedicaegre { get; set; }
        public string sin_zona { get; set; }

        public string sin_estado { get; set; }
        public string sin_objecionobs { get; set; }


    }
}
