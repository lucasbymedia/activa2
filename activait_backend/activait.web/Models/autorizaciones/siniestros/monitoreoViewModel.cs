﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.siniestros
{
    public class monitoreoViewModel
    {
        public int siniestros_idsiniestros { get; set; }
        public int tipos_idtipos_monitoreo { get; set; }
        public string mon_respuesta { get; set; }
        public string mon_resultado { get; set; }
        public DateTime mon_fecha { get; set; }

        public Nullable<DateTime> mon_fechacreacion { get; set; }
        public Nullable<int> mon_usucreacion { get; set; }

    }
}
