﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.autorizaciones.siniestros
{
    public class sinCrearViewModel
    {
        [Required]
        public int personas_idpersonas { get; set; }
        [Required]
        public int ciudades_idciudades { get; set; }
        [Required]
        [StringLength(maximumLength: 50, ErrorMessage = "Debe tener máximo 50 caracteres", MinimumLength = 1)]
        public string sin_tipoatep { get; set; }
        [Required]
        public DateTime sin_fecha { get; set; }
        public string sin_jornada { get; set; }
        public string sin_laboral { get; set; }
        public string sin_otra { get; set; }
        public string sin_codigo { get; set; }
        public string sin_tiempolaborprev { get; set; }
        public string sin_causal { get; set; }
        public string sin_muerte { get; set; }
        public string sin_sitio { get; set; }
        public string sin_lugar { get; set; }
        public string sin_tipolesion { get; set; }
        public string sin_parteafectada { get; set; }
        public string sin_agente { get; set; }

        public string sin_mecanismo { get; set; }
        public string sin_descripcion { get; set; }
        public string sin_responsable { get; set; }
        public string sin_nombreresponsable { get; set; }

        public string sin_cargoresponsable { get; set; }
        public string sin_documentoresponsable { get; set; }
        public DateTime sin_fechadiagnostico { get; set; }
        public string sin_diagnosticadapor { get; set; }
        public string sin_diagnostico { get; set; }

        public string sin_regmedico { get; set; }
        public string sin_nombremedico { get; set; }
        public string sin_evalmedicapre { get; set; }
        public string sin_evalmedicaper { get; set; }
        public string sin_evalmedicaegre { get; set; }
        public string sin_zona { get; set; }

        public Nullable<DateTime> sin_fechacreacion { get; set; }
        public Nullable<int> sin_usucreacion { get; set; }

        /*
        //Factores
        public List<factoresViewModel> factor { get; set; }
        //Testigos
        public List<testigosViewModel> testigo { get; set; }
        //Monitoreo
        public List<monitoreoViewModel> monitoreo { get; set; }*/
    }
}
