﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.siniestros
{
    public class sinEditarEstadoViewModel
    {
        public int idsieniestros { get; set; }
        public string sin_estado { get; set; }
        public string sin_objecionobs { get; set; }

        public Nullable<DateTime> sin_fechamodificacion { get; set; }
        public Nullable<int> sin_usumodificacion { get; set; }

    }
}
