﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.siniestros
{
    public class testigosViewModel
    {
        public int siniestros_idsiniestros { get; set; }
        public string tes_tipodoc { get; set; }
        public string tes_documento { get; set; }
        public string tes_nombre { get; set; }
        public string tes_cargo { get; set; }

        public Nullable<DateTime> tes_fechacreacion { get; set; }
        public Nullable<int> tes_usucreacion { get; set; }

    }
}
