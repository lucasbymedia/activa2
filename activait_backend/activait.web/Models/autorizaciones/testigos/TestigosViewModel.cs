﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.autorizaciones.testigos
{
    public class TestigosViewModel
    {
        public int idtestigos { get; set; }
        public int siniestros_idsiniestros { get; set; }
        public string tes_tipodoc { get; set; }
        public string tes_documento { get; set; }
        public string tes_nombre { get; set; }
        public string tes_cargo { get; set; }
    }
}
