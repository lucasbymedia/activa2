﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.afiliados
{
    public class AfiliadosViewModel
    {
        public int idafiliados { get; set; }
        public int personas_idpersonas { get; set; }
        public int ciudades_idciudades { get; set; }
        public int eps_ideps { get; set; }
        public string afi_tipo { get; set; }
        public string afi_cronico { get; set; }
        public string afi_estado { get; set; }
        public DateTime afi_fechaafiliacion { get; set; }
        public DateTime afi_fechaingreso { get; set; }
        public int afi_ibc { get; set; }
        public string afi_tipovinculacion { get; set; }
    }
}
