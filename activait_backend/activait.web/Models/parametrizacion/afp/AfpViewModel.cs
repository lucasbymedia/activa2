﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.afp
{
    public class AfpViewModel
    {
        public int idafps { get; set; }
        public string afp_codigo { get; set; }
        public string afp_nombre { get; set; }
    }
}
