﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.arl
{
    public class ArlViewModel
    {
        public int idarls { get; set; }
        public string arl_codigo { get; set; }
        public string arl_nombre { get; set; }
    }
}
