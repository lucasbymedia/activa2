﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.arl
{
    public class arlActualizarViewModel
    {
        public int idarls { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string arl_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string arl_nombre { get; set; }
    }
}
