﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.ciudades
{
    public class CiudadViewModel
    {
        public int idciudades { get; set; }
        public int iddepartamentos { get; set; }
        public string departamento { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
    }
}
