﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.ciudades
{
    public class ciuActualizarViewModel
    {
        [Required]
        public int idciudades { get; set; }
        [Required]
        public int iddepartamentos { get; set; }
        [Required]
        [StringLength(maximumLength: 10, ErrorMessage = "Debe tener máximo 10 caracteres", MinimumLength = 1)]
        public string mun_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string mun_nombre { get; set; }
    }
}
