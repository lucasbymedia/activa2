﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.departamentos
{
    public class DepartamentosVierwModel
    {
        public int iddepartamentos { get; set; }
        public string dep_codigo { get; set; }
        public string dep_nombre { get; set; }
    }
}
