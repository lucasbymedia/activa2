﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.departamentos
{
    public class depActualizarViewModel
    {
        [Required]
        public int iddepartamentos { get; set; }
        [Required]
        [StringLength(maximumLength: 10, ErrorMessage = "Debe tener máximo 10 caracteres", MinimumLength = 1)]
        public string dep_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string dep_nombre { get; set; }
    }
}
