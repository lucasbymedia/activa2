﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace activait.web.Models.parametrizacion.diagnosticos
{
    public class DiagnosticosViewModel
    {
        public int iddiagnosticos { get; set; }
        public string dia_codigo { get; set; }
        public string dia_descripcion { get; set; }
    }
}
