﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.diagnosticos
{
    public class diaActualizarViewModel
    {
        [Required]
        public int iddiagnosticos { get; set; }
        [Required]
        [StringLength(maximumLength: 10, ErrorMessage = "Debe tener máximo 10 caracteres", MinimumLength = 1)]
        public string dia_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string dia_descripcion { get; set; }
    }
}
