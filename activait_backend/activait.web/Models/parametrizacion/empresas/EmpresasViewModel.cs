﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.empresas
{
    public class EmpresasViewModel
    {
        public int idempresas { get; set; }
        public int ciudades_idciudades { get; set; }
        public int arl_idarl { get; set; }
        public string emp_nombre { get; set; }
        public string emp_nit { get; set; }
        public string emp_email { get; set; }
        public string emp_telefono { get; set; }
        public string emp_representante { get; set; }
        public string emp_direccion { get; set; }
        public string emp_actividad { get; set; }
        public string emp_codactividad { get; set; }
    }
}
