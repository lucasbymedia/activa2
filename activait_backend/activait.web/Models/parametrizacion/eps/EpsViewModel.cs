﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.eps
{
    public class EpsViewModel
    {
        public int ideps { get; set; }
        public string eps_codigo { get; set; }
        public string eps_nombre { get; set; }
    }
}
