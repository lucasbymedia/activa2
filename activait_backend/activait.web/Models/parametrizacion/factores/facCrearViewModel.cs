﻿using System.ComponentModel.DataAnnotations;


namespace activait.web.Models.parametrizacion.factores
{
    public class facCrearViewModel
    {
        [Required]
        [StringLength(maximumLength: 10, ErrorMessage = "Debe tener máximo 10 caracteres", MinimumLength = 1)]
        public string fac_codigo { get; set; }
    }
}
