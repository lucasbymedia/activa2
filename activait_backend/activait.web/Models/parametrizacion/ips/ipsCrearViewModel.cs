﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.ips
{
    public class ipsCrearViewModel
    {
        [Required]
        public int ciudades_idciudades { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres")]
        public string ips_nit { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string ips_nombre { get; set; }
        public string ips_contacto { get; set; }
        public string ips_direccion { get; set; }
        public string ips_telefono { get; set; }
        public string ips_naturaleza { get; set; }
    }
}
