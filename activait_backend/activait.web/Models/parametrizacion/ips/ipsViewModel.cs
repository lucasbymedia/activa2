﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.ips
{
    public class ipsViewModel
    {
        public int idips { get; set; }
        public int ciudades_idciudades { get; set; }
        public string ips_nit { get; set; }
        public string ips_nombre { get; set; }
        public string ips_contacto { get; set; }
        public string ips_direccion { get; set; }
        public string ips_telefono { get; set; }
        public string ips_naturaleza { get; set; }
    }
}
