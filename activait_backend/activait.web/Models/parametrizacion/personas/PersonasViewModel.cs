﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.personas
{
    public class PersonasViewModel
    {
        public int idpersonas { get; set; }
        public int empresas_idempresas { get; set; }
        public int afps_idafps { get; set; }
        public string per_tipoid { get; set; }
        public string per_numeroid { get; set; }
        public string per_primernombre { get; set; }
        public string per_segundonombre { get; set; }
        public string per_primerapellido { get; set; }
        public string per_segundoapellido { get; set; }
        public DateTime per_nacimiento { get; set; }
        public string per_genero { get; set; }
        public string per_tipovinculacion { get; set; }
        public string per_direccion { get; set; }
        public string per_telefono { get; set; }
        public string per_cargo { get; set; }
    }
}
