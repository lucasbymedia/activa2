﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.sercontratados
{
    public class SercontratadosViewModel
    {
        public int idsercontratados { get; set; }
        public int ips_idps { get; set; }
        public int servicios_idservicios { get; set; }
        public DateTime sec_fechainicio { get; set; }
        public DateTime sec_fechafin { get; set; }
        public int sec_valor { get; set; }
        public string sec_estado { get; set; }
        public string sec_prioridadasign { get; set; }
        public string sec_prioridadcosto { get; set; }
    }
}
