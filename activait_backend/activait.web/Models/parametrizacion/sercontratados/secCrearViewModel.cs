﻿using System;
using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.sercontratados
{
    public class secCrearViewModel
    {
        [Required]
        public int ips_idps { get; set; }
        [Required]
        public int servicios_idservicios { get; set; }
        [Required]
        public DateTime sec_fechainicio { get; set; }
        [Required]
        public DateTime sec_fechafin { get; set; }
        [Required]
        public int sec_valor { get; set; }
        [Required]
        public string sec_estado { get; set; }
        public string sec_prioridadasign { get; set; }
        public string sec_prioridadcosto { get; set; }
    }
}
