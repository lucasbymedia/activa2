﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.servicios
{
    public class ServiciosVierwModel
    {
        public int idservicios { get; set; }
        public string ser_codigo { get; set; }
        public string ser_descripcion { get; set; }
        public string ser_cups { get; set; }
        public string ser_cum { get; set; }
        public string ser_estado { get; set; }
        public string ser_prioridad { get; set; }
        public string ser_pbs { get; set; }
    }
}
