﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.servicios
{
    public class serActualizarViewModel
    {
        public int idservicios { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 2)]
        public string ser_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres")]
        public string ser_descripcion { get; set; }
        public string ser_cups { get; set; }
        public string ser_cum { get; set; }
        [Required]
        public string ser_estado { get; set; }
        public string ser_prioridad { get; set; }
        public string ser_pbs { get; set; }

    }
}
