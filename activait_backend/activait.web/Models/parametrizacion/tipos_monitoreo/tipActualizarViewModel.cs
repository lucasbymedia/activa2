﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.parametrizacion.tipos_monitoreo
{
    public class tipActualizarViewModel
    {
        public int idtipos_monitoreo { get; set; }
        [Required]
        [StringLength(maximumLength: 20, ErrorMessage = "Debe tener máximo 20 caracteres", MinimumLength = 2)]
        public string tip_codigo { get; set; }
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string tip_descripcion { get; set; }

    }
}
