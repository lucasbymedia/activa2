﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.parametrizacion.tipos_monitoreo
{
    public class tipViewModel
    {
        public int idtipos_monitoreo { get; set; }
        public string tip_codigo { get; set; }
        public string tip_descripcion { get; set; }

    }
}
