﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.usuarios.roles
{
    public class CrearViewModel
    {
        [Required]
        [StringLength(maximumLength: 100, ErrorMessage = "Debe tener máximo 100 caracteres", MinimumLength = 2)]
        public string rol_nombre { get; set; }
    }
}
