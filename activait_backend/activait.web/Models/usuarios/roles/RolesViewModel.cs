﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.usuarios.roles
{
    public class RolesViewModel
    {
        public int idroles { get; set; }
        public string rol_nombre { get; set; }
    }
}
