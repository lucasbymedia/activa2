﻿using System.ComponentModel.DataAnnotations;

namespace activait.web.Models.usuarios.users
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
    }
}
