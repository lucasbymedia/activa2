﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.usuarios.users
{
    public class UsuariosViewModel
    {
        public int idusuarios { get; set; }
        public int roles_idroles { get; set; }
        public string usu_nombre { get; set; }
        public string usu_email { get; set; }
        public string usu_documento { get; set; }
        public string usu_telefono { get; set; }
        public byte[] usu_passwordhash { get; set; }
    }
}
