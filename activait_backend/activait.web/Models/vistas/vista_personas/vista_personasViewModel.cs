﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.vistas.vista_personas
{
    public class vista_personasViewModel
    {
        public int idpersonas { get; set; } 
        public int idciudades { get; set; }
        public int iddepartamentos { get; set; }
        public string dep_codigo { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
        public int ideps { get; set; }
        public string eps_codigo { get; set; }
        public string eps_nombre { get; set; }
        public int idempresas { get; set; }
        public string emp_nombre { get; set; }
        public string emp_nit { get; set; }
        public string emp_email { get; set; }
        public string emp_telefono { get; set; }
        public string emp_representante { get; set; }
        public string emp_direccion { get; set; }
        public string per_tipoid { get; set; }
        public string per_numeroid { get; set; }
        public string per_primernombre { get; set; }
        public string per_segundonombre { get; set; }
        public string per_primerapellido { get; set; }
        public string per_segundoapellido { get; set; }
        public DateTime per_nacimiento { get; set; }
        public string per_genero { get; set; }
        public string per_tipovinculacion { get; set; }
        public string per_direccion { get; set; }
        public string per_telefono { get; set; }
        public string per_cargo { get; set; }
        public string emp_actividad { get; set; }
        public string emp_codactividad { get; set; }
        public int idafiliados { get; set; }
        public string afi_tipo { get; set; }
        public string afi_cronico { get; set; }
        public string afi_estado { get; set; }
        public DateTime afi_fechaafiliacion { get; set; }
        public DateTime afi_fechaingreso { get; set; }
        public int afi_ibc { get; set; }
        public string afi_tipovinculacion { get; set; }
        public int idafps { get; set; }
        public string afp_codigo { get; set; }
        public string afp_nombre { get; set; }
        public int idarls { get; set; }
        public string arl_codigo { get; set; }
        public string arl_nombre { get; set; }



    }
}
