﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace activait.web.Models.vistas.vista_siniestros
{
    public class vista_siniestrosViewModel
    {
        public int idpersonas { get; set; }
        public int idciudades { get; set; }
        public int iddepartamentos { get; set; }
        public string dep_codigo { get; set; }
        public string mun_codigo { get; set; }
        public string mun_nombre { get; set; }
        public int ideps { get; set; }
        public string eps_codigo { get; set; }
        public string eps_nombre { get; set; }
        public int idempresas { get; set; }
        public string emp_nombre { get; set; }
        public string emp_nit { get; set; }
        public string emp_email { get; set; }
        public string emp_telefono { get; set; }
        public string emp_representante { get; set; }
        public string emp_direccion { get; set; }
        public string per_tipoid { get; set; }
        public string per_numeroid { get; set; }
        public string per_primernombre { get; set; }
        public string per_segundonombre { get; set; }
        public string per_primerapellido { get; set; }
        public string per_segundoapellido { get; set; }
        public DateTime per_nacimiento { get; set; }
        public string per_genero { get; set; }
        public int idafiliados { get; set; }
        public string afi_tipo { get; set; }
        public string afi_cronico { get; set; }
        public string afi_estado { get; set; }
        public DateTime afi_fechaafiliacion { get; set; }
        public DateTime afi_fechaingreso { get; set; }
        public int afi_ibc { get; set; }
        public string afi_tipovinculacion { get; set; }

        public int idsieniestros { get; set; }
        public DateTime sin_fecha { get; set; }
        public string sin_tipoatep { get; set; }
        public string sin_jornada { get; set; }
        public string sin_laboral { get; set; }
        public string sin_otra { get; set; }
        public string sin_codigo { get; set; }
        public string sin_lugar { get; set; }
        public string sin_tiempolaborprev { get; set; }
        public string sin_causal { get; set; }
        public string sin_muerte { get; set; }
        public string sin_sitio { get; set; }
        public string sin_tipolesion { get; set; }
        public string sin_zona { get; set; }
        public string sin_parteafectada { get; set; }
        public string sin_agente { get; set; }
        public string sin_mecanismo { get; set; }
        public string sin_descripcion { get; set; }

        public string sin_responsable { get; set; }
        public string sin_nombreresponsable { get; set; }
        public string sin_cargoresponsable { get; set; }
        public string sin_documentoresponsable { get; set; }
        public Nullable<DateTime> sin_fechadiagnostico { get; set; }
        public string sin_diagnosticadapor { get; set; }
        public string sin_diagnostico { get; set; }
        public string sin_regmedico { get; set; }
        public string sin_nombremedico { get; set; }
        public string sin_evalmedicapre { get; set; }
        public string sin_evalmedicaper { get; set; }
        public string sin_evalmedicaegre { get; set; }
        public string sin_severidad { get; set; }
        public Nullable<int> sin_diasinc { get; set; }
        public string sin_ipp { get; set; }
        public Nullable<int> sin_valorrta { get; set; }
        public Nullable<int> sin_valorrti { get; set; }
        public string sin_estado { get; set; }
        public string sin_objecionobs { get; set; }
    }
}
