import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Inicio from './components/Inicio';
import Login from './components/Login';
import Error from './components/Error';

import Roles from './components/Roles';
import Usuarios from './components/Usuarios';
import Departamentos from './components/Departamentos';
import Municipios from './components/Municipios';
import Factores from './components/Factores';
import Diagnosticos from './components/Diagnosticos';
import Ips from './components/Ips';
import Eps from './components/Eps';
import ARL from './components/ARL';
import Tipos_Monitoreo from './components/Tipos_Monitoreo';
import Servicios from './components/Servicios';
import Empresas from './components/Empresas';
import Personas from './components/Personas';
import Afiliaciones from './components/Afiliaciones';
import Servicios_contratados from './components/Servicios_contratados';

import Radicacion from './components/Radicacion';
import Accidente from './components/Accidente';
import Enfermedad from './components/Enfermedad';
import Evaluacion from './components/Evaluacion';
import Consulta from './components/Consulta';
import furat from './components/furat';
import furel from './components/furel';
import Objetados from './components/Objetados';
import Autorizaciones from './components/Autorizaciones';
import detalle_objetadosal from './components/detalle_objetadosal';
import detalle_reservasal from './components/detalle_reservasal';
import revisar_autorizaciones from './components/revisar_autorizaciones';
import evaluar_autorizacion from './components/evaluar_autorizacion';

import Crearrol from './components/Creaarrol';
import Crearusuario from './components/Crearusuario';
import Creardpto from './components/Creardpto';
import Crearmuni from './components/Crearmuni';
import Crearfactor from './components/Crearfactor';
import Creardiag from './components/Creardiag';
import Crearips from './components/Crearips';
import Creareps from './components/Creareps';
import Creararl from './components/Creararl';
import Creartipm from './components/Creartipm';
import Crearserv from './components/Crearserv';
import Crearempr from './components/Crearempr';
import Crearpers from './components/Crearpers';
import Crearafil from './components/Crearafil';
import Crearserc from './components/Crearserc';

import Editarol from './components/Editarol';
import Editausuario from './components/Editausuario';
import Editadpto from './components/Editadpto';
import Editamuni from './components/Editamuni';
import Editafactor from './components/Editafactor';
import Editadiag from './components/Editadiag';
import Editaips from './components/Editaips';
import Editaeps from './components/Editaeps';
import Editaarl from './components/Editaarl';
import Editatipm from './components/Editatipm';
import Editaserv from './components/Editaserv';
import Editaempr from './components/Editaempr';
import Editapers from './components/Editapers';
import Editaafil from './components/Editaafil';
import Editaserc from './components/Editaserc';

class Router extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Login}></Route>
                    <Route path="/Inicio" component={Inicio}></Route>
                    <Route path="/Login" component={Login}></Route>
                    <Route path="/Roles" component={Roles}></Route>
                    <Route path="/Usuarios" component={Usuarios}></Route>
                    <Route path="/Departamentos" component={Departamentos}></Route>
                    <Route path="/Municipios" component={Municipios}></Route>
                    <Route path="/Factores" component={Factores}></Route>
                    <Route path="/Diagnosticos" component={Diagnosticos}></Route>
                    <Route path="/Ips" component={Ips}></Route>
                    <Route path="/Eps" component={Eps}></Route>
                    <Route path="/ARL" component={ARL}></Route>
                    <Route path="/Tipos_Monitoreo" component={Tipos_Monitoreo}></Route>
                    <Route path="/Servicios" component={Servicios}></Route>
                    <Route path="/Empresas" component={Empresas}></Route>
                    <Route path="/Personas" component={Personas}></Route>
                    <Route path="/Afiliaciones" component={Afiliaciones}></Route>
                    <Route path="/Servicios_contratados" component={Servicios_contratados}></Route>

                    <Route path="/Radicacion" component={Radicacion}></Route>
                    <Route path="/Accidente/:documento" component={Accidente}></Route>
                    <Route path="/Enfermedad/:documento" component={Enfermedad}></Route>
                    <Route path="/Evaluacion" component={Evaluacion}></Route>
                    <Route path="/Consulta" component={Consulta}></Route>
                    <Route path="/furat/:id" component={furat}></Route>
                    <Route path="/furel/:id" component={furel}></Route>
                    <Route path="/Objetados" component={Objetados}></Route>
                    <Route path="/Autorizaciones" component={Autorizaciones}></Route>
                    <Route path="/detalle_objetadosal/:id" component={detalle_objetadosal}></Route>
                    <Route path="/detalle_reservasal/:id" component={detalle_reservasal}></Route>
                    <Route path="/revisar_autorizaciones" component={revisar_autorizaciones}></Route>
                    <Route path="/evaluar_autorizacion/:id" component={evaluar_autorizacion}></Route>

                    <Route path="/Crearrol" component={Crearrol}></Route>
                    <Route path="/Crearusuario" component={Crearusuario}></Route>
                    <Route path="/Creardpto" component={Creardpto}></Route>
                    <Route path="/Crearmuni" component={Crearmuni}></Route>
                    <Route path="/Crearfactor" component={Crearfactor}></Route>
                    <Route path="/Creardiag" component={Creardiag}></Route>
                    <Route path="/Crearips" component={Crearips}></Route>
                    <Route path="/Creareps" component={Creareps}></Route>
                    <Route path="/Creararl" component={Creararl}></Route>
                    <Route path="/Creartipm" component={Creartipm}></Route>
                    <Route path="/Crearserv" component={Crearserv}></Route>
                    <Route path="/Crearempr" component={Crearempr}></Route>
                    <Route path="/Crearpers" component={Crearpers}></Route>
                    <Route path="/Crearafil" component={Crearafil}></Route>
                    <Route path="/Crearserc" component={Crearserc}></Route>
                    
                    <Route path="/Editarol/:id" component={Editarol}></Route>
                    <Route path="/Editausuario/:id" component={Editausuario}></Route>
                    <Route path="/Editadpto/:id" component={Editadpto}></Route>
                    <Route path="/Editamuni/:id" component={Editamuni}></Route>
                    <Route path="/Editafactor/:id" component={Editafactor}></Route>
                    <Route path="/Editadiag/:id" component={Editadiag}></Route>
                    <Route path="/Editaips/:id" component={Editaips}></Route>
                    <Route path="/Editaeps/:id" component={Editaeps}></Route>
                    <Route path="/Editaarl/:id" component={Editaarl}></Route>
                    <Route path="/Editatipm/:id" component={Editatipm}></Route>
                    <Route path="/Editaserv/:id" component={Editaserv}></Route>
                    <Route path="/Editaempr/:id" component={Editaempr}></Route>
                    <Route path="/Editapers/:id" component={Editapers}></Route>
                    <Route path="/Editaafil/:id" component={Editaafil}></Route>
                    <Route path="/Editaserc/:id" component={Editaserc}></Route>
                    
                    <Route component={Error}></Route>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Router;