$(function(){
  $.ajax
  (
    { 
         url : 'jsoneps.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="eps" name="eps" required>';
                htmlvar +='<option vaule="">EPS del Afiliado Código</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                eps_campo.innerHTML = htmlvar;
            }
      }
  );
});

$(function(){
  $.ajax
  (
    { 
         url : 'jsonafp.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="afp" name="afp" required>';
                htmlvar +='<option vaule="">ARL del Afiliado Código</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                afp_campo.innerHTML = htmlvar;
            }
      }
  );
});

$(function(){
    $.ajax
    (
      { 
           url : 'jsonconsulta.php' ,
               dataType: "text",
               success : function (data) 
               {
            var htmlvar ='<table id="datatable1" class="table display responsive nowrap">';
            htmlvar +='<thead><tr><th>Empleado</th><th>Tipo de siniestro</th><th>Fecha</th><th>Número de Siniestro</th>',
            
            htmlvar +='<th>Estado</th><th>Reporte</th>';
            htmlvar +='</tr></thead><tbody>';
            var strLines = data.split("},");
            for (var i in strLines) {
                var vals = strLines[i];
                vals = vals.replace("[","");
                vals = vals.replace("}","");
                vals = vals.replace("]","");
                vals = vals.replace("{","");
                vals = vals.replace(/['"]+/g, '');
                var vars = vals.split(",");
                var nome =vars[1].split(":");
                var tipo =vars[2].split(":");
                var fech =vars[3].split(":");
                var nume =vars[4].split(":");
                var esta =vars[5].split(":");
                var est1=esta[1];
                if(est1=="Aceptado"){ var luz ="style='color:#0F0;' "; }
                else if(est1=="Rechazado"){ var luz ="style='color:#F00;' "; }
                else { var luz ="style='color:#000;' "; }
                var id=vars[0].split(":");
  
                htmlvar += '<tr><td>'+nome[1]+'</td><td>'+tipo[1]+'</td><td>'+fech[1]+'</td><td>'+nume[1]+'</td>';
                htmlvar += '<td><a href="seguimiento?id='+id[1]+'"><span '+luz+'>'+esta[1]+'</span></a></td>';
                htmlvar += '<td align="center"><a href="informe?id='+id[1]+'">Ver informe</a></td></tr>';
            }
            htmlvar += "</table>";
            tablas.innerHTML = htmlvar;
               }
         }
    );
});
 