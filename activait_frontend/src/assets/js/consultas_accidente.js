$(function(){
  $.ajax
  (
    { 
         url : 'jsoneps.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="eps" name="eps" required>';
                htmlvar +='<option vaule="">EPS del Afiliado Código</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                eps_campo.innerHTML = htmlvar;
            }
      }
  );
});

$(function(){
  $.ajax
  (
    { 
         url : 'jsonarl.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="arl" name="arl" required>';
                htmlvar +='<option vaule="">ARL del Afiliado Código</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                arl_campo.innerHTML = htmlvar;
            }
      }
  );
});


$(function(){
  $.ajax
  (
    { 
         url : 'jsonafp.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="afp" name="afp" required>';
                htmlvar +='<option vaule="">ARL del Afiliado Código</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                afp_campo.innerHTML = htmlvar;
            }
      }
  );
});


$(function(){
  $.ajax
  (
    { 
         url : 'jsondptos.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="depa" name="depa" required>';
                htmlvar +='<option vaule="">Departamento</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[2].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                dep_campo.innerHTML = htmlvar;
            }
      }
  );
});

$(function(){
  $.ajax
  (
    { 
         url : 'jsonciudades.php' ,
             dataType: "text",
             success : function (data) 
             {
                var htmlvar ='<select class="form-control" id="ciud" name="ciud" required>';
                htmlvar +='<option vaule="">Municipio</option>';
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var codig =vars[1].split(":");
                    var idips=vars[0].split(":");
                    var epsno=vars[3].split(":");
                    htmlvar += '<option vaule="'+idips[1]+'">'+epsno[1]+'</option>';
                }
                htmlvar +='</select><br>';
                ciu_campo.innerHTML = htmlvar;
            }
      }
  );
});

$(function(){
  $.ajax
  (
    { 
         url : 'jsonpersona.php' ,
             dataType: "text",
             success : function (data) 
             {
                var strLines = data.split("},");
                for (var i in strLines) {
                    var vals = strLines[i];
                    vals = vals.replace("[","");
                    vals = vals.replace("}","");
                    vals = vals.replace("]","");
                    vals = vals.replace("{","");
                    vals = vals.replace(/['"]+/g, '');
                    var vars = vals.split(",");
                    var trab=vars[0].split(":");
                    var docu=vars[1].split(":");
                    var empl=vars[2].split(":");
                    var codi=vars[3].split(":");

                    document.forma1.trab.value=trab[1];
                    document.forma1.doct.value=docu[1];
                    document.forma1.empt.value=empl[1];
                    document.forma1.codt.value=codi[1];
                }
            }
      }
  );
});
