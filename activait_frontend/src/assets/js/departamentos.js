function guardar()
{
     alert("Guardar");
}
function editar()
{
     alert("Editar");
}
$(function(){
  $.ajax
  (
    { 
         url : 'jsondptos.php' ,
             dataType: "text",
             success : function (data) 
             {
          var htmlvar ='<table id="datatable1" class="table display responsive nowrap">';
          htmlvar +='<thead><tr><th>Código</th><th>Departamento</th>';
          htmlvar +='<th>Editar</th><th>Eliminar</th>';
          htmlvar +='</tr></thead><tbody>';
          var strLines = data.split("},");
          for (var i in strLines) {
              var vals = strLines[i];
              vals = vals.replace("[","");
              vals = vals.replace("}","");
              vals = vals.replace("]","");
              vals = vals.replace("{","");
              vals = vals.replace(/['"]+/g, '');
              var vars = vals.split(",");
              var id=vars[0].split(":");
              var codec =vars[1].split(":");
              var nombre =vars[2].split(":");

              htmlvar += '<tr><td>'+codec[1]+'</td><td>'+nombre[1]+'</td>';
              htmlvar += '<td align="center"><a href="#" onclick="pop_dis3('+id[1]+', \'Departamento\', 0)" title="Editar">';
              htmlvar += '<i class="icon ion-edit"></i></a></td>';
              htmlvar += '<td align="center"><a href="#" title="Eliminar" onclick="return confirm(\'Está seguro de eliminar este registro?\');">';
              htmlvar += '<i class="icon ion-trash-b"></i></a></td></tr>';
          }
          htmlvar += "</table>";
          tablas.innerHTML = htmlvar;
             }
       }
  );

  'use strict';
  $('#datatable1').DataTable({
    responsive: true,
    language: {
      searchPlaceholder: 'Buscar...',
      sSearch: '',
      lengthMenu: '_MENU_ items/pagina',
    }
  });
  $('#datatable2').DataTable({
    bLengthChange: false,
    searching: false,
    responsive: true
  });
  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

});
