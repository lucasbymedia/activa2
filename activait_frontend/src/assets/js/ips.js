function guardar()
{
     alert("Guardar");
}
function editar()
{
     alert("Editar");
}
$(function(){
  $.ajax
  (
    { 
         url : 'jsonips.php' ,
             dataType: "text",
             success : function (data) 
             {
          var htmlvar ='<table id="datatable1" class="table display responsive nowrap">';
          htmlvar +='<thead><tr><th>NIT</th><th>Nombre IPS</th><th>Contacto</th><th>Dirección</th><th>Teléfono</th><th>Editar</th><th>Eliminar</th>';
          htmlvar +='</tr></thead><tbody>';
          var strLines = data.split("},");
          for (var i in strLines) {
              var vals = strLines[i];
              vals = vals.replace("[","");
              vals = vals.replace("}","");
              vals = vals.replace("]","");
              vals = vals.replace("{","");
              vals = vals.replace(/['"]+/g, '');
              var vars = vals.split(",");
              var codi =vars[2].split(":");
              var nomb =vars[3].split(":");
              var cont =vars[4].split(":");
              var dire =vars[5].split(":");
              var tele =vars[6].split(":");
              var id=vars[0].split(":");

              htmlvar += '<tr><td>'+codi[1]+'</td><td>'+nomb[1]+'</td><td>'+cont[1]+'</td><td>'+dire[1]+'</td><td>'+tele[1]+'</td>';
              htmlvar += '<td align="center"><a href="#" onclick="pop_dis3('+id[1]+', \'IPS\', 0)" title="Editar">';
              htmlvar += '<i class="icon ion-edit"></i></a></td>';
              htmlvar += '<td align="center"><a href="#" title="Eliminar" onclick="return confirm(\'Está seguro de eliminar este registro?\');">';
              htmlvar += '<i class="icon ion-trash-b"></i></a></td></tr>';
          }
          htmlvar += "</table>";
          tablas.innerHTML = htmlvar;
             }
       }
  );

  'use strict';
  $('#datatable1').DataTable({
    responsive: true,
    language: {
      searchPlaceholder: 'Buscar...',
      sSearch: '',
      lengthMenu: '_MENU_ items/pagina',
    }
  });
  $('#datatable2').DataTable({
    bLengthChange: false,
    searching: false,
    responsive: true
  });
  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

});
