function guardar()
{
     alert("Guardar");
}
function editar()
{
     alert("Editar");
}
$(function(){
  $.ajax
  (
    { 
         url : 'jsonroles.php' ,
             dataType: "text",
             success : function (data) 
             {
          var htmlvar ='<table id="datatable1" class="table display responsive nowrap">';
          htmlvar +='<thead><tr><th class="wd-50p">Rol nombre</th><th class="wd-25p">Editar</th><th class="wd-25p">Eliminar</th>';
          htmlvar +='</tr></thead><tbody>';
          var strLines = data.split("},");
          for (var i in strLines) {
              var vals = strLines[i];
              vals = vals.replace("[","");
              vals = vals.replace("}","");
              vals = vals.replace("]","");
              vals = vals.replace("{","");
              vals = vals.replace(/['"]+/g, '');
              var vars = vals.split(",");
              var nombre =vars[1].split(":");
              var id=vars[0].split(":");

              htmlvar += '<tr><td>'+nombre[1]+'</td><td align="center"><a href="#" onclick="pop_dis3('+id[1]+', \'Rol\', 0)" title="Editar">';
              htmlvar += '<i class="icon ion-edit"></i></a></td>';
              htmlvar += '<td align="center"><a href="#" title="Eliminar" onclick="return confirm(\'Está seguro de eliminar este registro?\');">';
              htmlvar += '<i class="icon ion-trash-b"></i></a></td></tr>';
          }
          htmlvar += "</table>";
          tablas.innerHTML = htmlvar;
             }
       }
  );

  'use strict';
  $('#datatable1').DataTable({
    responsive: true,
    language: {
      searchPlaceholder: 'Buscar...',
      sSearch: '',
      lengthMenu: '_MENU_ items/pagina',
    }
  });
  $('#datatable2').DataTable({
    bLengthChange: false,
    searching: false,
    responsive: true
  });
  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

});
