function guardar()
{
     alert("Guardar");
}
function editar()
{
     alert("Editar");
}
$(function(){
  $.ajax
  (
    { 
         url : 'jsonserv.php' ,
             dataType: "text",
             success : function (data) 
             {
          var htmlvar ='<table id="datatable1" class="table display responsive nowrap">';
          htmlvar +='<thead><tr><th>Código</th><th>Descripción</th><th>CUBS</th><th>CUM</th><th>Estado</th><th>Prioridad</th><th>PBS</th>';
          htmlvar +='<th>Editar</th><th>Eliminar</th></tr></thead><tbody>';
          var strLines = data.split("},");
          for (var i in strLines) {
              var vals = strLines[i];
              vals = vals.replace("[","");
              vals = vals.replace("}","");
              vals = vals.replace("]","");
              vals = vals.replace("{","");
              vals = vals.replace(/['"]+/g, '');
              var vars = vals.split(",");
              var codi =vars[1].split(":");
              var desc =vars[2].split(":");
              var cubs =vars[3].split(":");
              var cumm =vars[4].split(":");
              var esta =vars[5].split(":");
              var prio =vars[6].split(":");
              var pbs =vars[7].split(":");
              var id=vars[0].split(":");

              htmlvar += '<tr><td>'+codi[1]+'</td><td>'+desc[1]+'</td><td>'+cubs[1]+'</td><td>'+cumm[1]+'</td><td>'+esta[1]+'</td><td>'+prio[1]+'</td>';
              htmlvar += '<td>'+pbs[1]+'</td><td align="center"><a href="#" onclick="pop_dis3('+id[1]+', \'Servicio\', 0)" title="Editar">';
              htmlvar += '<i class="icon ion-edit"></i></a></td>';
              htmlvar += '<td align="center"><a href="#" title="Eliminar" onclick="return confirm(\'Está seguro de eliminar este registro?\');">';
              htmlvar += '<i class="icon ion-trash-b"></i></a></td></tr>';
          }
          htmlvar += "</table>";
          tablas.innerHTML = htmlvar;
             }
       }
  );

  'use strict';
  $('#datatable1').DataTable({
    responsive: true,
    language: {
      searchPlaceholder: 'Buscar...',
      sSearch: '',
      lengthMenu: '_MENU_ items/pagina',
    }
  });
  $('#datatable2').DataTable({
    bLengthChange: false,
    searching: false,
    responsive: true
  });
  $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

});
