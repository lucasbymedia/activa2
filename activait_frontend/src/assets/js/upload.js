// JavaScript Document
(function(){
    var input = document.getElementById('archivoar'),
        formdata = false;
    function mostrarImagenSubida(source){
//        var list = document.getElementById('lista-imagenes'),
//            li   = document.createElement('li'),
//            img  = document.createElement('img');
        
//        img.src = source;
//        li.appendChild(img);
//        list.appendChild(li);
    }
	var sqla = document.getElementById('sqla').value;    
    if(window.FormData){
        formdata = new FormData();
        document.getElementById('btnSubmit').style.display = 'none';
    }
    
    if(input.addEventListener){
        input.addEventListener('change', function(evt){
            var i = 0, len = this.files.length, img, reader, file;
            for( ; i < len; i++){
                file = this.files[i];
				var subir=0;
				if(file.type=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type=="application/msword" ||
				file.type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document" || file.type=="application/vnd.ms-excel" ||
				file.type=="application/vnd.openxmlformats-officedocument.presentationml.presentation" || file.type=="application/zip" || 
				file.type=="application/pdf" || file.type=="application/rar" || file.type=="image/png" || file.type=="image/jpeg" || 
				file.type=="application/x-zip-compressed" ){ subir=1; }
				else { alert("El formato del archivo no es correcto, solo se admiten: .zip, .pdf, .doc, .xls, .jpg)"); return; }
				if(subir==1 && file.size<200000000){ subir=2; }
				else { alert("Solo se puede subir archivos de máximo 200 MB"); return; } 
                if(subir==2){
				    document.getElementById('response').innerHTML = 'Subiendo...';
					document.getElementById("archis").style.display = "none";
                    if(window.FileReader){
                        reader = new FileReader();
                        reader.onloadend = function(e){
                           // mostrarImagenSubida(e.target.result);
                        };
                        reader.readAsDataURL(file);
                    }
                    if(formdata){ 
						formdata.append('archivoar[]', file); 
						formdata.append('sql', sqla);
					}
                }
				else { document.getElementById('response').innerHTML = 'Por favor intente de nuevo'; return } 
            }
            if(formdata){
                $.ajax({
                   url : 'upload.php',
                   type : 'POST',
                   data : formdata,
                   processData : false, 
                   contentType : false, 
                   success : function(res){
                       alert("El archivo se subió correctamente");
					   window.location.reload()
					   document.getElementById('response').innerHTML = res;
                   }                
                });
            }
        }, false);
    }
}());
