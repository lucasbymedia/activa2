import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
import { now } from 'moment';
const cookies = new Cookies(); 

class Accidente extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    param11 = React.createRef(); 
    param12 = React.createRef(); 
    param13 = React.createRef(); 
    param14 = React.createRef(); 
    param15 = React.createRef(); 
    param16 = React.createRef(); 
    param17 = React.createRef(); 
    param18 = React.createRef(); 
    param19 = React.createRef(); 
    param20 = React.createRef(); 
    param21 = React.createRef(); 
    param22 = React.createRef(); 
    param23 = React.createRef(); 
    param24 = React.createRef(); 
    param25 = React.createRef(); 
    param26 = React.createRef(); 
    param27 = React.createRef(); 
    param28 = React.createRef(); 
    param29 = React.createRef(); 
    param30 = React.createRef(); 
    param31 = React.createRef(); 
    param32 = React.createRef(); 
    param33 = React.createRef(); 
    param34 = React.createRef(); 
    param35 = React.createRef(); 
    param36 = React.createRef(); 
    param37 = React.createRef(); 
    param38 = React.createRef(); 
    param39 = React.createRef(); 
    param40 = React.createRef(); 
    param41 = React.createRef(); 
    param42 = React.createRef(); 
    param43 = React.createRef(); 
    param44 = React.createRef(); 
    param45 = React.createRef(); 
    param46 = React.createRef(); 
    param47 = React.createRef(); 
    param48 = React.createRef(); 
    param49 = React.createRef(); 
    param50 = React.createRef(); 
    param51 = React.createRef(); 
    param52 = React.createRef(); 
    param53 = React.createRef(); 
    param54 = React.createRef(); 
    param55 = React.createRef(); 
    param56 = React.createRef(); 
    param57 = React.createRef(); 
    param58 = React.createRef(); 
    param59 = React.createRef(); 
    param60 = React.createRef(); 
    state = {
        tabe:[],
        geps:[],
        garl:[],
        gafp:[],
        tvin:['Empleador', 'Contratante', 'Cooperativa de trabajo asociado'],
        tvil:['Planta', 'Misión', 'Cooperado', 'Estudiante/Aprendíz', 'Independiente'],
        sexo:['Hombre','Mujer'],
        dept:[],
        ciud:[],
        llen:[],
        rins:[],
        status: null
    };
    componentDidMount() {

        this.documento=this.props.match.params.documento;
        this.getdatos(this.documento);

        axios.get(global.url + "eps/listar").then((res) => {
            const geps = res.data;
            this.setState({ geps });
        });
        axios.get(global.url + "arls/listar").then((res) => {
            const garl = res.data;
            this.setState({ garl });
        });
        axios.get(global.url + "afps/listar").then((res) => {
            const gafp = res.data;
            this.setState({ gafp });
        });

        axios.get(global.url + "departamentos/listar").then((res) => {
            if (res.data) {
                const dept = res.data;
                this.setState({ dept });
                }
          });

          axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const ciud = res.data;
                this.setState({ ciud });
                }
          });

          axios.get(global.url + "eps/listar").then((res) => {
            if (res.data) {
                const epss = res.data;
                this.setState({ epss });
                }
          });

    }
    getdatos = (documento) => {
        axios.get(global.url + "vista_personas/Consulta/" + documento ).then((res) => {
            const tabe = res.data;
            this.setState(tabe);
        });
      }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            'personas_idpersonas': this.state.idpersonas,
            'ciudades_idciudades': this.param41.current.value,
            'sin_fecha': this.param31.current.value +" " + this.param32.current.value + ":" + this.param33.current.value +":00",
            'sin_tipoatep': "FURAT",
            'sin_jornada': this.param34.current.value,
            'sin_laboral': this.param35.current.value, 
            'sin_otra' : this.param36.current.value,
            'sin_codigo' : this.param37.current.value,
            'sin_tiempolaborprev': this.param38.current.value +":"+ this.param381.current.value, 
            'sin_causal': this.param39.current.value,
            'sin_muerte': this.param40.current.value,
            'sin_sitio': this.param42.current.value,
            'sin_tipolesion': this.param43.current.value,
            'sin_parteafectada': this.param44.current.value,
            'sin_agente': this.param45.current.value,
            'sin_lugar': this.param46.current.value,
            'sin_mecanismo': this.param47.current.value,
            'sin_descripcion': this.param48.current.value, 
            'sin_responsable': this.param57.current.value,
            'sin_nombreresponsable': this.param57.current.value,
            'sin_cargoresponsable': this.param58.current.value,
            'sin_documentoresponsable': this.param59.current.value,
            'sin_fechadiagnostico': this.param60.current.value,
            'sin_diagnosticadapor': "",
            'sin_diagnostico': "", 
            'sin_regmedico': "",
            'sin_nombremedico': "",
            'sin_evalmedicapre': "",
            'sin_evalmedicaper': "",
            'sin_evalmedicaegre': "",
            'sin_zona': this.param411.current.value,
            'sin_fechacreacion': new Date(),
            'sin_usucreacion': cookies.get("idusuarios")
        }
        console.log(tabe);

        axios.post(global.url+"siniestros/Crear/", tabe).then(res =>{
            console.log(res);

            axios.get(global.url + "siniestros/consulta/" + this.state.idpersonas).then((res) => {
                const rins = res.data;
                this.setState(rins);
                if(this.param49.current.value!==""){   
                    axios
                         .post(global.url + "testigos/Crear/" , {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tes_tipodoc": this.param51.current.value, 
                            "tes_documento": this.param52.current.value, 
                            "tes_nombre": this.param49.current.value, 
                            "tes_cargo": this.param50.current.value,
                            'tes_fechacreacion': new Date(),
                            'tes_usucreacion': cookies.get("idusuarios")
                
                         })
                            .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }
                if(this.param53.current.value!==""){        
                    axios
                        .post(global.url + "testigos/Crear/", {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tes_tipodoc": this.param55.current.value, 
                            "tes_documento": this.param56.current.value, 
                            "tes_nombre": this.param53.current.value, 
                            "tes_cargo": this.param54.current.value,
                            'tes_fechacreacion': new Date(),
                            'tes_usucreacion': cookies.get("idusuarios")
                         })
                        .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }
            });
            swal('Sinietrso creado', 'Se ha creado correctamente el siniestro', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Sinietrso no creado", "Hubo un error al crear el siniestro", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Radicacion"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Accidente Laboral</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                        <h6 className="card-body-title">Accidente Laboral</h6><br/>
<div className="row">
  <div className="col-lg">
    <input className="form-control" defaultValue={this.state.per_primernombre} ref={this.param1} placeholder="Trabajador" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" defaultValue={this.state.per_numeroid} ref={this.param2} placeholder="Documento" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" defaultValue={this.state.emp_nombre} ref={this.param3} placeholder="Empleador" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" placeholder="Código reporte" type="text" />
  </div>
</div>
<div className="card pd-10 pd-sm-20 mg-t-20">
  <h2 className="card-body-title">INFORME DE ACCIDENTE DE TRABAJO DEL EMPLEADOR O CONTRATANTE</h2>
  <br />
  <form onSubmit={this.guardar} name="forma">
      <h4>Afiliaciones</h4>
      <div className="row">
          <div className="col-lg-12">
            <select className="form-control" defaultValue={this.state.ideps} ref={this.param4} name="eps" required>
                <option value="">EPS del Afiliado Código</option>
                    {
                        this.state.geps.map((con, i) => {
                        return (
                            this.state.ideps===con.ideps ? (
                                <option key={con.ideps} value={con.ideps} selected>{con.eps_nombre}</option> ) 
                                :(<option key={con.ideps} value={con.ideps}>{con.eps_nombre}</option> )
                            )
                        })
                    }
            </select><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <select className="form-control" defaultValue={this.state.idarls} ref={this.param5} name="arl" required>
                  <option vaule="">ARL del Afiliado Código</option>
                  {
                        this.state.garl.map((con, i) => {
                        return (
                            this.state.idarls===con.idarls ? (
                                <option key={con.idarls} value={con.idarls} selected>{con.arl_nombre}</option> ) 
                                :(<option key={con.idarls} value={con.idarls}>{con.arl_nombre}</option> )
                            )
                        })
                    }
              </select><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-4">
              Seguro social<br/>
            <div className="row">
                <div className="col-lg-3">Si</div>
                <div className="col-lg-3"><input className="form-control" value="Si" type="radio" /></div>
                <div className="col-lg-3">No</div>
                <div className="col-lg-3"><input checked className="form-control" value="No" type="radio" /></div>
            </div>
          </div>
          <div className="col-lg mg-t-10 mg-lg-t-0">
             <input className="form-control" placeholder="Cuál" id="cual" name="cual" type="text"/>
             <br/><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <select className="form-control" defaultValue={this.state.idafps} ref={this.param6} name="afp" required>
                  <option vaule="">AFP del Afiliado</option>
                  {
                        this.state.gafp.map((con, i) => {
                        return (
                            this.state.idafps===con.idafps ? (
                                <option key={con.idafps} value={con.idafps} selected>{con.afp_nombre}</option> ) 
                                :(<option key={con.idafps} value={con.idafps}>{con.afp_nombre}</option> )
                            )
                        })
                    }
              </select><br/>
          </div>
      </div>
      <h4> I. Identificación General</h4>
      <h6>I. IDENTIFICACIÓN GENERAL DEL EMPLEADOR, CONTRATANTE O COOPERATIVA</h6>  
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">Tipo de vinculador laboral</h6>
          </div>
       </div>
       <div className="row">   
       {
            this.state.tvin.map((con, i) => {
                return (
                    
                    this.state.afi_tipovinculacion===con ? (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" checked className="form-control" name="param7" required defaultValue={this.state.afi_tipovinculacion} ref={this.param7} value={con}/></div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" className="form-control" name="param7" required defaultValue={this.state.afi_tipovinculacion} ref={this.param7} value={con}/></div>
                    </React.Fragment>
                    )
                )
            })
        }
        <br/><br/>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">SEDE PRINCIPAL</h6><br/>
          </div>
      </div>   
      <div className="row">
          <div className="col-lg-8">
             <input type="text" className="form-control" defaultValue={this.state.emp_actividad} ref={this.param8} required name="actec"
              placeholder="Nombre de la actividad económica"/>
          </div>
          <div className="col-lg-4">
             <input type="number" className="form-control" defaultValue={this.state.emp_codactividad} ref={this.param9}  
             required name="codac" placeholder="Código"/><br/>
           </div>
       </div>    
       <div className="row">
          <div className="col-lg-6">
             <input type="text" className="form-control" defaultValue={this.state.emp_nombre} ref={this.param10}
             required name="nombr" placeholder="Nombre Razón social" />
          </div>
          <div className="col-lg-2">
              <select className="form-control" required>
                  <option vaule="NI" selected>NI</option>
                  <option vaule="CC">CC</option>
                  <option vaule="CE">CE</option>
                  <option vaule="NU">NU</option>
                  <option vaule="PA">PA</option>
              </select><br/>
           </div>
           <div className="col-lg-4">
              <input type="text" className="form-control" defaultValue={this.state.emp_nit} ref={this.param10}
              required name="ndoc" placeholder="Número"/>
           </div>
        </div>    

        <div className="row">
          <div className="col-lg-6">
             <input type="text" className="form-control" defaultValue={this.state.emp_direccion} ref={this.param11} 
             name="dire" placeholder="Dirección" required/>
          </div>
          <div className="col-lg-2">
              <select className="form-control" id="zona" name="zona" required>
                  <option vaule="U" selected>Urbana</option>
                  <option vaule="R">Rural</option>
              </select><br/>
           </div>
        </div>    
      <br/>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">CENTRO DE TRABAJO DONDE LABORA EL TRABAJADOR</h6><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-9">
              ¿SON LOS DATOS DEL CENTRO DE TRABAJO LOS MISMOS DE LA SEDE PRINCIPAL? <br/>
              <span className="pequeno">Solo en caso negativo diligenciar las siguientes casillas del centro de trabajo</span>
          </div>
          <div className="col-lg-3">
              <div className="row">
                  <div className="col-lg-3">Si</div>
                  <div className="col-lg-3"><input checked className="form-control" value="Si" type="radio" required/></div>
                  <div className="col-lg-3">No</div>
                  <div className="col-lg-3"><input className="form-control" value="No" type="radio" required /></div>
              </div>
              <br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-8">
             <input type="text" defaultValue={this.state.emp_actividad} ref={this.param12} className="form-control" name="actc" 
             placeholder="Nombre de la actividad económica del Centro de Trabajo"/>
          </div>
          <div className="col-lg-4">
              <input type="text" defaultValue={this.state.emp_codactividad} ref={this.param13} className="form-control" name="cact" 
              placeholder="Código de actividad"/><br/>
          </div>
        </div>    
        <div className="row">
          <div className="col-lg-6">
             <input type="text" defaultValue={this.state.emp_direccion} ref={this.param14} className="form-control" name="dir2" 
             placeholder="Dirección"/>
          </div>
           <div className="col-lg-2">
              <select className="form-control" id="zon2" name="zon2" required>
                  <option vaule="U" selected>Urbana</option>
                  <option vaule="R">Rural</option>
              </select><br/>
           </div>
        </div>   
        <h4> II. Información de la persona accidentada</h4> 
        <h6>II. INFORMACIÓN DE LA PERSONA QUE SE ACCIDENTÓ</h6>  
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">TIPO DE VINCULADOR LABORAL</h6>
          </div>
       </div>
       <div className="row">
       {
            this.state.tvil.map((con, i) => {
                return (
                    
                    this.state.per_tipovinculacion===con ? (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" checked className="form-control" name="param15" required defaultValue={this.state.per_tipovinculacion} ref={this.param15} value={con}/></div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" className="form-control" name="param15" required defaultValue={this.state.per_tipovinculacion} ref={this.param15} value={con}/></div>
                    </React.Fragment>
                    )
                )
            })
        }
            <div className="col-lg-4">
              <input type="text" className="form-control" required
              defaultValue={this.state.per_tipoid} ref={this.param16} name="coda" placeholder="Código"/><br></br>
            </div>
      </div>
      <div className="row">
          <div className="col-lg-3">
             <input type="text" className="form-control" defaultValue={this.state.per_primerapellido} ref={this.param17} 
             required id="ape1" name="ape1" placeholder="Primer Apellido" />
          </div>
          <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_segundoapellido} ref={this.param18} 
              id="ape2" name="ape2" placeholder="Segundo Apellido" />
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_primernombre} ref={this.param19}  
              required id="nom1" name="nom1" placeholder="Primer Nombre" />
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_segundonombre} ref={this.param20} 
              id="nom2" name="nom2" placeholder="Segundo Nombre" /><br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-2">
              <select className="form-control"  required>
                  <option vaule="">Tipo identificación</option>
                  <option vaule="NI">NI</option>
                  <option vaule="CC" selected>CC</option>
                  <option vaule="CE">CE</option>
                  <option vaule="NU">NU</option>
                  <option vaule="PA">PA</option>
              </select><br/>
          </div>
          <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_numeroid} ref={this.param21} 
              required name="docp" placeholder="Documento"/>
           </div>
           <div className="col-lg-4">Fecha nacimiento<br/>
              <input type="date" className="form-control" defaultValue={this.state.per_nacimiento} ref={this.param22}
               required name="fnac" placeholder="Fecha de nacimiento"/><br/>
           </div>
           <div className="col-lg-3">
           {
            this.state.sexo.map((con, i) => {
                return (
                    
                    this.state.per_genero===con ? (
                    <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-8">{con}</div>
                        <div className="col-lg-4">
                        <input type="radio" checked className="form-control" name="param23" required defaultValue={this.state.per_genero} 
                        ref={this.param23} value={con}/></div>
                    </div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-8">{con}</div>
                        <div className="col-lg-4">
                        <input type="radio" className="form-control" name="param23" required defaultValue={this.state.per_genero} ref={this.param23} value={con}/></div>
                    </div>
                    </React.Fragment>
                    )
                )
            })
        }
            </div>
      </div>    
      <div className="row">
          <div className="col-lg-6">
             <input type="text" defaultValue={this.state.per_direccion} ref={this.param24} className="form-control" name="dirp" placeholder="Dirección" required/>
          </div>
          <div className="col-lg-3">
              <input type="text" defaultValue={this.state.per_telefono} ref={this.param25} required className="form-control" name="telp" placeholder="Teléfono"/>
              <br/>
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" id="faxp" name="faxp" placeholder="Fax"/>
              <br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
              <select className="form-control" defaultValue={this.state.iddepartamentos}  ref={this.param26} name="depa" required>
                  <option vaule="">Departamento</option>
                  {
                        this.state.dept.map((con, i) => {
                        return (
                            this.state.iddepartamentos===con.iddepartamentos ? (
                                <option key={con.iddepartamentos} value={con.iddepartamentos} selected>{con.dep_nombre} </option> ) 
                                :(<option key={con.iddepartamentos} value={con.iddepartamentos}>{con.dep_nombre} </option> )
                            )
                        })
                    }
              </select>
          </div>
          <div className="col-lg-5">
              <select className="form-control" defaultValue={this.state.idciudades} ref={this.param27} name="ciud" required>
                  <option vaule="">Municipio</option>
                  {
                        this.state.ciud.map((con, i) => {
                        return (
                            this.state.idciudades===con.idciudades ? (
                                <option key={con.idciudades} value={con.idciudades} selected>{con.mun_nombre} </option> ) 
                                :(<option key={con.idciudades} value={con.idciudades}>{con.mun_nombre} </option> )
                            )
                        })
                    }
              </select>
          </div>
          <div className="col-lg-2">
              <select className="form-control" required>
                  <option vaule="U" selected>Urbana</option>
                  <option vaule="R">Rural</option>
              </select><br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
             <input type="text" defaultValue={this.state.per_cargo} ref={this.param28} className="form-control" name="carg" placeholder="Cargo que desempeña"/>
          </div>
          <div className="col-lg-5">
              <input type="text" className="form-control" name="ocup" placeholder="Ocupación Habitual"/><br/>
          </div>
          <div className="col-lg-2">
              <input type="text" className="form-control" name="codc" placeholder="Código"/><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
             Tiempo de ocupación habitual al momento del accidente
          </div>
          <div className="col-lg-2">
              <input type="number" className="form-control" id="dias" name="dias" placeholder="DD"/>
              <br/>
           </div>
           <div className="col-lg-2">
              <input type="number" className="form-control" id="mese" name="mese" placeholder="MM"/>
              <br/>
           </div>
           <div className="col-lg-3">Fecha de ingreso a la empresa
              <input type="date" defaultValue={this.state.afi_fechaafiliacion} ref={this.param29}  className="form-control" name="fvin" 
              placeholder="Fecha de ingreso a la empresa" required/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
              <input type="number" defaultValue={this.state.afi_ibc} ref={this.param30} className="form-control" required name="sala" 
              placeholder="Salario u horonarios"/>
              <br/>
           </div>
           <div className="col-lg-3">
              <select className="form-control" id="zon3" name="zon3" required>
                  <option vaule="Diurna" selected>Diurna</option>
                  <option vaule="Nocturna">Nocturna</option>
                  <option vaule="Mixto">Mixto</option>
                  <option vaule="Turnos">Turnos</option>
              </select><br/>
           </div>
      </div>   
      <h2>III. Información del accidente</h2> 
      <h4>III. INFORMACIÓN SOBRE EL ACCIDENTE</h4>  
      <div className="row">
          <div className="col-lg-3">Fecha del accidente
              <input type="date" ref={this.param31} className="form-control" id="feca" name="feca" 
              placeholder="Fecha del accidente" required/><br/>
          </div>
          <div className="col-lg-3">Hora del accidente
              <div className="row">
                  <div className="col-lg-6"><input ref={this.param32} className="form-control" type="number" id="hora" 
                  placeholder="HH" name="hora" required/></div>
                  <div className="col-lg-6"><input ref={this.param33} className="form-control" type="number" id="mins" 
                  name="mins" placeholder="MM" required/></div>
              </div>    
            </div>
            <div className="col-lg-6">Jornada de trabajo habitual
              <div className="row">
                  <div className="col-lg-3">Normal</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param34} 
                  value="Normal" id="jorn" name="jorn" type="radio" required/></div>
                  <div className="col-lg-3">Extra</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param34} 
                  value="Extra" id="jorn" name="jorn" type="radio"  required/></div>
              </div><br/>
            </div>
      </div>
      <div className="row">
          <div className="col-lg-5">¿Estaba realizando su labor habitual?
              <div className="row">
                  <div className="col-lg-3">Si</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param35}
                  id="labo" name="labo" value="Si" type="radio"  required/></div>
                  <div className="col-lg-3">No</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param35} 
                  id="labo" name="labo" value="No" type="radio"  required/></div>
              </div>
          </div>    
          <div className="col-lg-5">
              <input type="text" className="form-control" ref={this.param36} id="cua2" name="cua2" 
              placeholder="Cúal (diligenciar en caso negativo)"/>
           </div>
           <div className="col-lg-2">
              <input type="text" className="form-control" ref={this.param37} id="codj" name="codj" placeholder="Código"/><br/>
           </div>
       </div>    
       <div className="row">
          <div className="col-lg-3">Tiempo laborado previo al accidente
              <div className="row">
                  <div className="col-lg-6"><input className="form-control" ref={this.param38} 
                  type="number" id="hola" name="hola" placeholder="HH" required/></div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param381}
                  type="number" id="mila" name="mila" placeholder="MM" required/><br/></div>
                  
              </div>
          </div>
       </div>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">Causa del accidente</h6><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
              <div className="row">
                  <div className="col-lg-6">Violencia</div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param39} type="radio" id="caua" name="caua"  value="Violencia" required/></div>
                  <div className="col-lg-6">Tránsito</div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param39} type="radio" id="caua" name="caua"  value="Tránsito" required/></div>
                  <div className="col-lg-6">Deportivo</div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param39} type="radio" id="caua" name="caua"  value="Deportivo" required/></div>
                  <div className="col-lg-6">Recreativo/Cultural</div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param39} type="radio" id="caua" name="caua"  value="Recreativo/Cultural" required/></div>
                  <div className="col-lg-6">Propio del trabajo</div>
                  <div className="col-lg-6"><input className="form-control" ref={this.param39} type="radio" id="caua" name="caua"  value="Propio del trabajo" required/></div>
                  <br></br>
              </div>
          </div>
          <div className="col-lg-4"><b>¿Causó la muerte al trabajador?</b>
              <div className="row">
                  <div className="col-lg-3">Si</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param40} value="Si" id="muer" name="muer" type="radio"  required/></div>
                  <div className="col-lg-3">No</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param40} value="No" id="muer" name="muer" type="radio"  required/><br/></div>
              </div>
              <br/><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
                  <select className="form-control" name="dep2" required>
                      <option value="" selected>Departamento</option>
                      {
                        this.state.dept.map((con, i) => {
                        return (
                                <option key={con.iddepartamentos} value={con.iddepartamentos}>{con.dep_nombre} </option> 
                            )
                        })
                    }
                  </select>
          </div>
          <div className="col-lg-5">
                  <select className="form-control" ref={this.param41} name="ciu2" required>
                      <option value="" selected>Municipio</option>
                      {
                        this.state.ciud.map((con, i) => {
                        return (
                                <option key={con.idciudades} value={con.idciudades}>{con.mun_nombre} </option> 
                            )
                        })
                    }

                  </select>
              </div>
              <div className="col-lg-2">
                  <select className="form-control" ref={this.param411} id="zon4" name="zon4" required>
                      <option value="U">Urbana</option>
                      <option value="R">Rural</option>
                  </select><br/>
               </div>
      </div>    
      <div className="row">
          <div className="col-lg-12">Lugar de ocurrencia
              <div className="row">
                  <div className="col-lg-3">Dentro de la empresa</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param42}  
                  name="luga" value="Dentro de la empresa" type="radio"  required/></div>
                  <div className="col-lg-3">Fuera de la empresa</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param42}  
                  name="luga" value="Fuera de la empresa" type="radio"  required/><br/></div>
                  <div className="col-lg-3">Trabajo en casa</div>
                  <div className="col-lg-3"><input className="form-control" ref={this.param42} 
                  name="luga" value="Trabajo en casa" type="radio"  required/><br/></div>
              </div>
              <br/>
           </div>
      </div>    
    <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">INDIQUE CUÁL SITIO</h6><br/>
          </div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Almacenes o depósitos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} value="Almacenes o depósitos" type="radio" id="siti" name="siti"  required/></div>
      <div className="col-lg-3">Áreas de producción</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} value="Áreas de producción" type="radio" id="siti" name="siti"   required/><br/></div>
      <div className="col-lg-3">Áreas recreativas o productivas</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} value="Áreas recreativas o productivas" id="siti" name="siti"  type="radio"  required/><br/></div>
     </div>    
     <div className="row">
      <div className="col-lg-3">Corredores o pasillos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"  value="Corredores o pasillos" type="radio"  required/></div>
      <div className="col-lg-3">Escaleras</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"   value="Escaleras" type="radio"  required/><br/></div>
      <div className="col-lg-3">Parqueadero o área de circulación vehicular</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"   value="Parqueadero o área de circulación vehicular" type="radio" required/><br/></div>
     </div>    
     <div className="row">
      <div className="col-lg-3">Oficinas</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"  value="Oficinas" type="radio" required/></div>
      <div className="col-lg-3">Otras áreas comunes</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"   value="Otras áreas comunes" type="radio"  required/><br/></div>
      <div className="col-lg-3">Otro (especifique)</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param43} id="siti" name="siti"  value="Otro (especifique)" type="radio"  required/><br/></div>
     </div>
    <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">TIPO DE LESIÓN</h6><br/>
          </div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Fractura</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles"  value="TL1" type="radio" required/></div>
      <div className="col-lg-3">Luxación</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL2" type="radio" required/><br/></div>
      <div className="col-lg-3">Torcedura, esguince, desgarro muscular, hernia o laceración del músculo o tendón sin herida </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL3" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Conmoción o trauma interno</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL4" type="radio" required/></div>
      <div className="col-lg-3">Amputación o enucleación (Exclusión o pérdida del ojo)  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL5" type="radio" required/><br/></div>
      <div className="col-lg-3">Herida</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL6" type="radio" required/><br/></div>
    </div>                   
    <div className="row">
      <div className="col-lg-3">Trauma superficial (Incluye rasguño, punción o pinchazo y lesión en ojo por cuerpo extraño)</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL7" type="radio" required/></div>
      <div className="col-lg-3">Golpe, contusión o aplastamiento  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL8" type="radio" required/><br/></div>
      <div className="col-lg-3">Quemadura</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL9" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Envenenamiento o intoxicación aguda o alergia</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL10" type="radio" required/></div>
      <div className="col-lg-3">Efecto del tiempo, del clima u otro relacionado con el ambiente </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL11" type="radio" required/><br/></div>
      <div className="col-lg-3">Asfixia</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL12" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Efecto de la electricidad</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL13" type="radio" required/></div>
      <div className="col-lg-3">Efecto nocivo de la radicación </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL14" type="radio" required/><br/></div>
      <div className="col-lg-3">Lesiones múltiples</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL15" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Otro. (Especifique)  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param44} id="tles" name="tles" value="TL16" type="radio" required/></div>
    </div>    
    <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">PARTE DEL CUERPO AFECTADO</h6><br/>
          </div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Cabeza</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC1" type="radio" required/></div>
      <div className="col-lg-3">Ojo</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC2" type="radio"  required/><br/></div>
      <div className="col-lg-3">Cuello </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC3" type="radio"  required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Tronco (Incluye espalda, columna vertebral, médula espinal, pélvis)  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC4" type="radio" required/></div>
      <div className="col-lg-3">Tórax</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC5" type="radio" required/><br/></div>
      <div className="col-lg-3">Abdomen </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC6" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Miembros superiores  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC7" type="radio" required/></div>
      <div className="col-lg-3">Manos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC8" type="radio" required/><br/></div>
      <div className="col-lg-3">Miembros inferiores </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC9" type="radio" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Pies  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC10" type="radio"  required/></div>
      <div className="col-lg-3">Ubicaciones múltiples</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC11" type="radio"  required/><br/></div>
      <div className="col-lg-3">Lesiones generales u otras </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param45} id="parc" name="parc" value="PC12" type="radio"  required/><br/></div>
    </div>    
    <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">AGENTES DEL ACCIDENTE</h6><br/>
          </div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Máquinas y/o equipos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG1" type="radio" id="agea" name="agea" required/></div>
      <div className="col-lg-3">Medios de transporte</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG2" type="radio" id="agea" name="agea" required/><br/></div>
      <div className="col-lg-3">Aparatos </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG3" type="radio" id="agea" name="agea"  required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Herramientas, implementos o utensilios</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG4" type="radio" id="agea" name="agea"  required/></div>
      <div className="col-lg-3">Materiales o sustancias</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG5" type="radio" id="agea" name="agea" required/><br/></div>
      <div className="col-lg-3">Radiaciones </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG6" type="radio" id="agea" name="agea"  required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Ambiente de trabajo (Incluye superficies de tránsito y de trabajo, muebles, tejados, en el exterior, interior o subterráneos)  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG7" type="radio" id="agea" name="agea"  required/></div>
      <div className="col-lg-3">Otros agentes no clasificados</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG8" type="radio" id="agea" name="agea"  required/><br/></div>
      <div className="col-lg-3">Animales (Vivos o productos animales)   </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG9" type="radio" id="agea" name="agea"  required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Agentes no clasificados por falta de datos  </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param46} value="AG10" type="radio" id="agea" name="agea"  required/></div>
    </div>    

    <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">MECANISMO O FORMA</h6><br/>
          </div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Caída de personas</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME1" type="radio" id="meca" name="meca"  required/></div>
      <div className="col-lg-3">Caída de objetos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME2" type="radio" id="meca" name="meca"  required/><br/></div>
      <div className="col-lg-3">Sobreesfuerzo, esfuerzo excesivo o falso movimiento </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME3" type="radio" id="meca" name="meca"  required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Exposición o contacto con temperatura extrema</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME4" type="radio" id="meca" name="meca" required/></div>
      <div className="col-lg-3">Pisadas, choques o golpes</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME5" type="radio" id="meca" name="meca" required/><br/></div>
      <div className="col-lg-3">Exposición o contacto con la electricidad </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME6" type="radio" id="meca" name="meca" required/><br/></div>
    </div>    
    <div className="row">
      <div className="col-lg-3">Atrapamientos</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME7" type="radio" id="meca" name="meca" required/></div>
      <div className="col-lg-3">Exposición o contacto con sustancias novicas, radiaciones o salpicaduras</div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME8" type="radio" id="meca" name="meca" required/><br/></div>
      <div className="col-lg-3">Otro (especifique) </div>
      <div className="col-lg-1"><input className="form-control" ref={this.param47} value="ME9" type="radio" id="meca" name="meca" required/><br/></div>
    </div>    
      <h4>IV. Descripción del accidente</h4>
      <h6>IV. DESCRIPCIÓN DEL ACCIDENTE</h6>  
      <div className="row">
          <div className="col-lg-12">DESCRIBA DETALLADAMENTE EL ACCIDENTE. QUÉ LO ORIGINO O CAUSÓ
              <span className="pequeno"> (Responda a las preguntas qué paso, cuándo, dónde, cómo y por qué)</span><br/>
              <textarea className="form-control" ref={this.param48} id="desc" name="desc" required></textarea><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">PERSONAS QUE PRESENCIARON EL ACCIDENTE</h6><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-6">¿Hubo personas que presenciaron el accidente?
              <div className="row">
                  <div className="col-lg-3">Si</div>
                  <div className="col-lg-3"><input className="form-control" value="Si" type="radio" id="htes" name="htes" required/></div>
                  <div className="col-lg-3">No</div>
                  <div className="col-lg-3"><input className="form-control" value="No" type="radio" id="htes" name="htes" required/><br/></div>
              </div>
              <br/>
           </div>
           <div className="col-lg-6">Solo en caso afirmativo diligenciar la siguiente información</div>
      </div>
      <div className="row">
          <div className="col-lg-4">
              <input className="form-control" ref={this.param49} placeholder="Apellidos y Nombres completos" type="text" id="tes1" name="tes1"/>
          </div>    
          <div className="col-lg-3">
              <input className="form-control" ref={this.param50} placeholder="Cargo" type="tec1" id="tec1" name="tes1"/>
          </div>    
          <div className="col-lg-2">
              <select className="form-control" ref={this.param51} id="ttd1" name="ttd1">
                  <option vaule="">Tipo identificación</option>
                  <option vaule="NI" selected>NI</option>
                  <option vaule="CC">CC</option>
                  <option vaule="CE">CE</option>
                  <option vaule="NU">NU</option>
                  <option vaule="PA">PA</option>
              </select><br/>
          </div>
          <div className="col-lg-3">
              <input type="text" ref={this.param52} className="form-control" id="ted1" name="ted1" placeholder="Documento"/><br/>
           </div>
      </div>
      <div className="row">
          <div className="col-lg-4">
              <input className="form-control" ref={this.param53} placeholder="Apellidos y Nombres completos" type="text" id="tes2" name="tes2"/>
          </div>    
          <div className="col-lg-3">
              <input className="form-control" ref={this.param54} placeholder="Cargo" type="tec1" id="tec2" name="tes2"/>
          </div>    
          <div className="col-lg-2">
              <select className="form-control" ref={this.param55} id="ttd2" name="ttd2">
                  <option vaule="">Tipo identificación</option>
                  <option vaule="NI">NI</option>
                  <option vaule="CC">CC</option>
                  <option vaule="CE">CE</option>
                  <option vaule="NU">NU</option>
                  <option vaule="PA">PA</option>
              </select><br/>
          </div>
          <div className="col-lg-3">
              <input type="text" ref={this.param56} className="form-control" id="ted2" name="ted2" placeholder="Documento"/><br/>
           </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">PERSONA RESPONSABLE DEL INFORME</h6><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-4">
              <input className="form-control" ref={this.param57} 
              placeholder="Apellidos y Nombres completos" type="text" id="resp" name="resp"/>
          </div>    
          <div className="col-lg-3">
              <input className="form-control" ref={this.param58} 
              placeholder="Cargo" type="tec1" id="cres" name="cres"/>
          </div>    
          <div className="col-lg-2">
              <select className="form-control" id="tres" name="tres" required>
                  <option vaule="">Tipo identificación</option>
                  <option vaule="NI">NI</option>
                  <option vaule="CC">CC</option>
                  <option vaule="CE">CE</option>
                  <option vaule="NU">NU</option>
                  <option vaule="PA">PA</option>
              </select><br/>
          </div>
          <div className="col-lg-3">
              <input type="text" ref={this.param59} className="form-control" 
              id="dres" name="dres" placeholder="Documento"/><br/>
           </div>
      </div>
      <div className="row">
          <div className="col-lg-4">Fecha de diligenciamiento del informe<br/>
              <input className="form-control" ref={this.param60} 
              type="date" id="fdil" name="fdil" required/><br/>
          </div>    
      </div>
      <div className="row">
          <div className="col-lg-6">Firma<br/>
          </div>
          <div className="col-lg-6">Antes de continuar, por favor verifique que la información es correcta<br/>
          </div>    
      </div>
      <div className="row">
          <div className="col-lg-6"></div>
          <div className="col-lg-6" align="right"><br/>
              <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
          </div>
      </div>
  </form>
</div>


                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Accidente;