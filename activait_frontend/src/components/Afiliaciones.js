import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Afiliaciones extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        tabe: [],
        epss:[],
        depa:[],
        status: null,
    };
    llena(){
        axios.get(global.url+"vista_personas/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }
    componentDidMount() {
        axios.get(global.url + "eps/listar").then((res) => {
            if (res.data) {
                const epss = res.data;
                this.setState({ epss });
                }
          });

          axios.get(global.url + "departamentos/listar").then((res) => {
            if (res.data) {
                const depa = res.data;
                this.setState({ depa });
                }
          });

        this.llena();
    }
    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"Afiliaciones/Eliminar/" + id).then((res) => {
                swal("Persona eliminada", "Se ha eliminado la persona", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }
    
    consulta2 = () =>
    {
        const iddep = this.param1.current.value;
        const ideps = this.param2.current.value;
        axios.get(global.url+"vista_personas/Consulta2/"+ iddep + "/"+ ideps)
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });

    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Afiliaciones</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Afiliaciones</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearafil">
                                <i className="icon ion-plus-circled"></i> Agregar Afiliación</NavLink><br />
                            <form name="forma">
                            <div className="row">
                                <div className="col-md-2">Departamento</div>
                                <div className="col-md-4">
                                <select name="param1" ref={this.param1} className="form-control" onChange={this.consulta2}>
                                    <option value="0">Seleccione...</option>
                                    {
                                        this.state.depa.map((con, i) => {
                                        return (
                                                <option key={con.iddepartamentos} value={con.iddepartamentos} >
                                                    {con.dep_nombre}</option>
                                            )
                                        })
                                    }
                                </select>
                                </div>

                                <div className="col-md-2">EPS</div>
                                <div className="col-md-4">
                                <select name="param2" ref={this.param2} className="form-control" onChange={this.consulta2}>
                                    <option value="0">Seleccione...</option>
                                    {
                                        this.state.epss.map((con, i) => {
                                        return (
                                                <option key={con.ideps} value={con.ideps} >
                                                    {con.eps_nombre}</option>
                                            )
                                        })
                                    }
                                </select>
                                </div>
                            </div>
                            </form>
                            <div className="clearfix"><br></br></div>


                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>Municipio</th>
                                            <th>Empresa</th>
                                            <th>EPS</th>
                                            <th>Nombre</th>
                                            <th>Documento</th>
                                            <th>Fecha Afiliación</th>
                                            <th>Editar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idafiliados}>
                            <td align="left">{ tab.mun_nombre }</td><td align="left">{ tab.emp_nombre }</td>
                            <td>{ tab.eps_nombre}</td><td> {tab.per_primernombre} { tab.per_primerapellido }</td>
                            <td>{ tab.per_numeroid}</td>
                            <td align="left"><Moment format="DD MMM YYYY">{ tab.afi_fechaafiliacion }</Moment></td>
                            <td align="center"><NavLink to={"/Editaafil/"+tab.idafiliados}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idafiliados)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Afiliaciones;