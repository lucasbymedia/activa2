import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect, NavLink } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Consulta extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    state = {
        tabe:[],
        estados: ['RESERVADO', 'ACEPTADO', 'OBJETADO'],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "vista_siniestros/listar").then((res) => {
            const tabe = res.data;
            this.setState({ tabe });
          });
    }

    consultas = () => {
        var fec1=this.param1.current.value;
        if(fec1===""){
            fec1 = new Date().toJSON().slice(0,4).replace(/-/g,'-')+"-01-01";
        }
        var fec2=this.param2.current.value;
        if(fec2===""){
            fec2 = new Date().toJSON().slice(0,4).replace(/-/g,'-')+"-12-31";
        }
        var esta = this.param3.current.value; 
        if(esta===""){
            esta="RESERVADO";
        }
        axios.get(global.url + "vista_siniestros/Consulta_estado/"
        + esta + "/" + fec1 + "/" + fec2 + "/1"  ).then((res) => {
            const tabe = res.data;
            this.setState({ tabe });
        });
    }



    render() {
        if(!cookies.get("idroles"))
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Radicacion"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
      <div className="am-pagetitle">
      <h5 className="am-title">Consulta de siniestros</h5>
      <form id="searchBar" className="search-bar" action="inicio">
        <div className="form-control-wrapper">
          <input
            type="search" className="form-control bd-0"
            placeholder="Buscar..."/>
        </div>
        <button id="searchBtn" className="btn btn-orange">
          <i className="fa fa-search" ></i>
        </button>
      </form>
    </div>

    <div className="am-mainpanel">
      <div className="am-pagebody">
        <div className="card pd-20 pd-sm-40">
          <h6 className="card-body-title">Evaluación</h6>
          <form name="forma">
            <div className="row">
                <div className="col-lg-3">Fecha incial
                    <input ref={this.param1} className="form-control" type="date"/>
                </div>
                <div className="col-lg-3">Fecha final
                    <input ref={this.param2} className="form-control" type="date"/>
                </div>
                <div className="col-lg-3">Estado
                    <select ref={this.param3} className="form-control">
                    <option value="">Seleccione...</option>
                    {
                        this.state.estados.map((con, i) => {
                        return (
                                <option key={i} value={con}>{con}</option> ) 
                        })
                    }
                  </select>
                </div>
                <div className="col-lg-3">
                    <input onClick={this.consultas} type="button" className="btn btn-info pd-x-20" value="Consultar" />
                </div>
            </div>
            <br></br>
          </form>

          <div className="table-wrapper" id="tablas">
            <table id="datatable1" className="table display responsive nowrap">
              <thead>
                <tr>
                  <th>Nombre</th><th>Identificación</th><th>Tipo de siniestro</th><th>Fecha</th>
                  <th>Estado</th><th>Reporte</th>
                </tr>
              </thead>
              <tbody>
              {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={i}>
                            <td>{ tab.per_primernombre } { tab.per_primerapellido }</td>
                            <td>{ tab.per_numeroid }</td><td>{ tab.sin_tipoatep }</td>
                            <td><Moment format="DD MMM YYYY">{ tab.sin_fecha }</Moment></td>
                            { 
                                tab.sin_estado==='RESERVADO' ? ( <td><div className="amarillo">{tab.sin_estado}</div></td> )  
                                : tab.sin_estado==='OBJETADO' ? ( <td><div className="rojo">{tab.sin_estado}</div></td> )  
                                : tab.sin_estado==='ACEPTADO' ? ( <td><div className="verde">{tab.sin_estado}</div></td> )
                                : (<td>{tab.sin_estado}</td>)  
                            }
                            <td align="center">
                                {
                                    tab.sin_tipoatep==='FURAT'? 
                                    (<NavLink to={"/furat/"+tab.idsieniestros}>Ver informe</NavLink>)
                                    : (<NavLink to={"/furel/"+tab.idsieniestros}>Ver informe</NavLink>)
                                }
                            </td>
                        </tr>
                    )
                })
            }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Consulta;