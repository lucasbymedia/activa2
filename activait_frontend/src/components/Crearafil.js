import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Creaafil extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    param11 = React.createRef(); 
    param12 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        pers:[],
        deps:[],
        ciud:[],
        epss:[],
        esta:['Activo', 'Inactivo'],
        tvin:['Empleado', 'Otro'],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "empresas/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });

          axios.get(global.url + "departamentos/listar").then((res) => {
            if (res.data) {
                const deps = res.data;
                this.setState({ deps });
                }
          });

          axios.get(global.url + "eps/listar").then((res) => {
            if (res.data) {
                const epss = res.data;
                this.setState({ epss });
                }
          });

    }
    cambioper = () =>
    {
        const idemp = this.param1.current.value;
        axios.get(global.url+"personas/Consulta_emp/"+ idemp)
        .then(res => {
            const pers = res.data;
            console.log(pers);
            this.setState({ pers });
        });
    }

    cambiodep = () =>
    {
        const iddep = this.param3.current.value;
        axios.get(global.url+"ciudades/Consulta_dep/"+ iddep)
        .then(res => {
            const ciud = res.data;
            console.log(ciud);
            this.setState({ ciud });
        });
    }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "personas_idpersonas": this.param2.current.value,
            "ciudades_idciudades": this.param4.current.value,
            "eps_ideps": this.param12.current.value,
            "afi_tipo": this.param5.current.value,
            "afi_cronico": this.param6.current.value,
            "afi_estado": this.param7.current.value,
            "afi_fechaafiliacion": this.param8.current.value,
            "afi_fechaingreso": this.param9.current.value,
            "afi_ibc": this.param10.current.value,
            "afi_tipovinculacion": this.param11.current.value,
        }
        console.log(tabe);

        axios.post(global.url+"afiliados/Crear", tabe).then(res =>{
            console.log(res);
            swal('Afiliación creada', 'Se ha creado la afiliación correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Afiliación no creada", "Hubo un error al crear la afiliación", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Afiliaciones"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Afiliación</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Afiliación</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Empresa</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} onChange={this.cambioper} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                                <option key={con.idempresas} value={con.idempresas} >
                                    {con.emp_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Persona</div>
                <div className="col-md-5">
                  <select name="param2" ref={this.param2} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.pers.map((con, i) => {
                        return (
                                <option key={con.idpersonas} value={con.idpersonas} >
                                    {con.per_primernombre} {con.per_primerapellido}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Departamento</div>
                <div className="col-md-5 derechas">
                <select name="param3" ref={this.param3} onChange={this.cambiodep} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.deps.map((con, i) => {
                            return (
                                <option key={con.iddepartamentos} value={con.iddepartamentos}>{con.dep_nombre}</option>
                                )
                            })
                        }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Municipio</div>
                <div className="col-md-5">
                  <select name="param4" ref={this.param4} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.ciud.map((con, i) => {
                        return (
                                <option key={con.idciudades} value={con.idciudades}>{con.mun_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">EPS</div>
                <div className="col-md-5 derechas">
                <select name="param3" ref={this.param12} onChange={this.cambiodep} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.epss.map((con, i) => {
                            return (
                                <option key={con.ideps} value={con.ideps}>{con.eps_nombre}</option>
                                )
                            })
                        }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo Afiliación</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Crónico</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Estado</div>
                <div className="col-md-5 derechas">
                <select name="param7" ref={this.param7} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.esta.map((con, i) => {
                        return (
                            <option key={i} value={con} >{con}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha de afiliación</div>
                <div className="col-md-5 derechas">
                    <input name="param8" ref={this.param8} className="form-control"  type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha de ingreso</div>
                <div className="col-md-5 derechas">
                    <input name="param9" ref={this.param9} className="form-control"  type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">IBC</div>
                <div className="col-md-5 derechas">
                    <input name="param10" ref={this.param10} className="form-control"  type="number"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo de vinculación</div>
                <div className="col-md-5 derechas">
                <select name="param11" ref={this.param11} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.tvin.map((con, i) => {
                        return (
                            <option key={i} value={con} >{con}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Afiliaciones">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Creaafil;