import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Creardiag extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        tabe: {},
        status: null
    };
    componentDidMount() {
    }
    guardar = (e) =>{
        e.preventDefault();
        const tabe = {
            "dia_codigo": this.param1.current.value,
            "dia_descripcion": this.param2.current.value
        }
        axios.post(global.url+"diagnosticos/crear", tabe
        ).then(res =>{
            console.log(res);
            swal('Diagnóstico creado', 'Se ha creado el diagnóstico correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Diagnóstico no creado", "Hubo un error al crear el diagnóstico", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(!cookies.get("idroles"))
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Diagnosticos"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Diagnóstico</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Diagnóstico</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
                                    <div className="row">
                                            <div className="col-md-5">Código</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param1" ref={this.param1} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">Diagnóstico</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param2" ref={this.param2} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <NavLink className="btn btn-secondary pd-x-20" to="/Diagnosticos">Cancelar</NavLink>
                                        <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Creardiag;