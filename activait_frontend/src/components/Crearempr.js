import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearempr extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        arls:[],
        status: null
    };
    componentDidMount() {
        
        axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });

          axios.get(global.url + "arls/listar").then((res) => {
            if (res.data) {
                const arls = res.data;
                this.setState({ arls });
                }
          });


    }
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "ciudades_idciudades": this.param1.current.value,
            "arl_idarl": this.param2.current.value,
            "emp_nombre": this.param3.current.value,
            "emp_nit": this.param4.current.value,
            "emp_email": this.param5.current.value,
            "emp_telefono": this.param6.current.value,
            "emp_representante": this.param7.current.value,
            "emp_direccion": this.param8.current.value,
            "emp_actividad": this.param9.current.value,
            "emp_codactividad": this.param10.current.value,
        }
        console.log(tabe);

        axios.post(global.url+"empresas/Crear", tabe).then(res =>{
            console.log(res);
            swal('Empresas creada', 'Se ha creado la empresa correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Empresa no creada", "Hubo un error al crear la empresa", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Empresas"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Empresa</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Empresa</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Municipio</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                                <option key={con.idciudades} value={con.idciudades} >
                                    {con.mun_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">ARL</div>
                <div className="col-md-5">
                  <select name="param2" ref={this.param2} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.arls.map((con, i) => {
                        return (
                                <option key={con.idarls} value={con.idarls} >
                                    {con.arl_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">NIT</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Email</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="email"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Teléfono</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Representante</div>
                <div className="col-md-5 derechas">
                    <input name="param7" ref={this.param7} className="form-control" type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Dirección</div>
                <div className="col-md-5 derechas">
                    <input name="param8" ref={this.param8} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Actividad</div>
                <div className="col-md-5 derechas">
                    <input name="param9" ref={this.param9} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Código Actividad</div>
                <div className="col-md-5 derechas">
                    <input name="param10" ref={this.param10} className="form-control"  type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Empresas">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearempr;