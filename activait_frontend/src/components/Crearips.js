import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearips extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });
    }
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "ciudades_idciudades": this.param1.current.value,
            "ips_nit": this.param2.current.value,
            "ips_nombre": this.param3.current.value,
            "ips_contacto": this.param4.current.value,
            "ips_direccion": this.param5.current.value,
            "ips_telefono": this.param6.current.value,
        }
        console.log(tabe);

        axios.post(global.url+"ips/Crear", tabe).then(res =>{
            console.log(res);
            swal('IPS creada', 'Se ha creado la IPS correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("IPS no creada", "Hubo un error al crear la IPS", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Ips"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar IPS</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar IPS</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Municipio</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                                <option key={con.idciudades} value={con.idciudades} >
                                    {con.mun_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">NIT</div>
                <div className="col-md-5 derechas">
                    <input name="param2" ref={this.param2} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Contácto</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Dirección</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Teléfono</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/IPS">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearips;