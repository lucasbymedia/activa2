import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearmuni extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "Departamentos/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });
    }
    guardar = (e) =>{
        e.preventDefault();
        const tabe = {
            "iddepartamentos": this.param1.current.value,
            "mun_codigo": this.param2.current.value,
            "mun_nombre": this.param3.current.value
        }
        console.log(tabe);
        axios.post(global.url+"ciudades/Crear", tabe).then(res =>{
            console.log(res);
            swal('Municipio creado', 'Se ha creado el municipio correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Municipio no creado", "Hubo un error al crear el municipio", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Municipios"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Municipio</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Municipio</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Departamento</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                                <option key={con.iddepartamentos} value={con.iddepartamentos} >
                                    {con.dep_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Código</div>
                <div className="col-md-5 derechas">
                    <input name="param2" ref={this.param2} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Municipios">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearmuni;