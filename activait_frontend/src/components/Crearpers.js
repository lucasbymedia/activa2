import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearpers extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    param11 = React.createRef(); 
    param12 = React.createRef(); 
    param13 = React.createRef(); 
    param14 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        arls:[],
        tipd:['CC', 'CE'],
        gene:['Hombre', 'Mujer'],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "empresas/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });

          axios.get(global.url + "afps/listar").then((res) => {
            if (res.data) {
                const arls = res.data;
                this.setState({ arls });
                }
          });


    }
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "empresas_idempresas": this.param1.current.value,
            "afps_idafps": this.param2.current.value,
            "per_tipoid": this.param3.current.value,
            "per_numeroid": this.param4.current.value,
            "per_primernombre": this.param5.current.value,
            "per_segundonombre": this.param6.current.value,
            "per_primerapellido": this.param7.current.value,
            "per_segundoapellido": this.param8.current.value,
            "per_nacimiento": this.param9.current.value,
            "per_genero": this.param10.current.value,
            "per_tipovinculacion": this.param11.current.value,
            "per_direccion": this.param12.current.value,
            "per_telefono": this.param13.current.value,
            "per_cargo": this.param14.current.value,
        }
        console.log(tabe);

        axios.post(global.url+"personas/Crear", tabe).then(res =>{
            console.log(res);
            swal('Persona creada', 'Se ha creado la personas correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Persona no creada", "Hubo un error al personas la empresa", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Personas"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Persona</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Persona</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Empresa</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                                <option key={con.idempresas} value={con.idempresas} >
                                    {con.emp_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">AFP</div>
                <div className="col-md-5">
                  <select name="param2" ref={this.param2} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.arls.map((con, i) => {
                        return (
                                <option key={con.idafps} value={con.idafps} >
                                    {con.afp_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo de documento</div>
                <div className="col-md-5 derechas">
                <select name="param3" ref={this.param3} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.tipd.map((con, i) => {
                        return (
                                <option key={i} value={con} >
                                    {con}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Documento</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Primer nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Segundo nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} className="form-control" type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Primer apellido</div>
                <div className="col-md-5 derechas">
                    <input name="param7" ref={this.param7} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Segundo apellido</div>
                <div className="col-md-5 derechas">
                    <input name="param8" ref={this.param8} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha de nacimiento</div>
                <div className="col-md-5 derechas">
                    <input name="param9" ref={this.param9} className="form-control"  type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Género</div>
                <div className="col-md-5 derechas">
                <select name="param10" ref={this.param10} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.gene.map((con, i) => {
                        return (
                                <option key={i} value={con} >
                                    {con}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo de vinculación</div>
                <div className="col-md-5 derechas">
                    <input name="param11" ref={this.param11} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Dirección</div>
                <div className="col-md-5 derechas">
                    <input name="param12" ref={this.param12} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Teléfono</div>
                <div className="col-md-5 derechas">
                    <input name="param13" ref={this.param13} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Cargo</div>
                <div className="col-md-5 derechas">
                    <input name="param14" ref={this.param14} className="form-control"  type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Empresas">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearpers;