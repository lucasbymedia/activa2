import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearserc extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    state = {
        serv: [],
        ipss: [],
        cons:[],
        esta: ['Activo', 'Inactivo'],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "servicios/listar").then((res) => {
            if (res.data) {
                const serv = res.data;
                this.setState({ serv });
                }
          });
          axios.get(global.url + "ips/listar").then((res) => {
            if (res.data) {
                const ipss = res.data;
                this.setState({ ipss });
                }
          });

    }
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "servicios_idservicios": this.param1.current.value,
            "ips_idps": this.param2.current.value,
            "sec_fechainicio": this.param3.current.value,
            "sec_fechafin": this.param4.current.value,
            "sec_valor": this.param5.current.value,
            "sec_estado": this.param6.current.value,
            "sec_prioridadasign": this.param7.current.value,
            "sec_prioridadcosto": this.param8.current.value
        }
        console.log(tabe);

        axios.post(global.url+"sercontratados/Crear", tabe).then(res =>{
            console.log(res);
            swal('Servicio contratado', 'Se ha contratado el servicio correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Servicio no contratado", "Hubo un error al contratar el servicio", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Servicios_contratados"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Contratar servicios</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Contratar servicios</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Servicios</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.serv.map((con, i) => {
                        return (
                                <option key={con.idservicios} value={con.idservicios} >
                                    {con.ser_codigo} {con.ser_descripcion}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">IPS</div>
                <div className="col-md-5">
                  <select name="param2" ref={this.param2} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.ipss.map((con, i) => {
                        return (
                                <option key={con.idips} value={con.idips} >
                                    {con.ips_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha inicial</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} className="form-control" required type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha final</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} className="form-control" required type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Valor</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="number"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Estado</div>
                <div className="col-md-5 derechas">
                <select name="param6" ref={this.param6} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.esta.map((con, i) => {
                        return (
                            <option key={i} value={con}>{con}</option> 
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Prioridad asignada</div>
                <div className="col-md-5 derechas">
                    <input name="param7" ref={this.param7} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Prioridad costo</div>
                <div className="col-md-5 derechas">
                    <input name="param8" ref={this.param8} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Servicios_contratados">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearserc;