import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Crearserv extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    state = {
        tabe: {},
        status: null, 
        cons: ["Activo", "Inactivo"],
        pior: ["Alta", "Media", "Baja"],
        pbss: ["Ok", ""]
    };
    componentDidMount() {
    }
    guardar = (e) =>{
        e.preventDefault();
        const tabe = {
            "ser_codigo": this.param1.current.value,
            "ser_descripcion": this.param2.current.value,
            "ser_cups": this.param3.current.value,
            "ser_cum": this.param4.current.value,
            "ser_estado": this.param5.current.value,
            "ser_prioridad": this.param6.current.value,
            "ser_pbs": this.param7.current.value
        }
        axios.post(global.url+"servicios/crear", tabe
        ).then(res =>{
            console.log(res);
            swal('Servicio creado', 'Se ha creado el servicio correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Servicio no creado", "Hubo un error al crear el servicio", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Servicios"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Servicio</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Servicio</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
                                    <div className="row">
                                            <div className="col-md-5">Código</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param1" ref={this.param1} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">Descripción</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param2" ref={this.param2} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">CUPS</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param3" ref={this.param3} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">CUM</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param4" ref={this.param4} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">Estado</div>
                                            <div className="col-md-5">
                                            <select name="param5" ref={this.param5} className="form-control" required>
                                                <option>Seleccione...</option>
                                                {
                                                    this.state.cons.map((con, i) => {
                                                    return (
                                                            <option key={i} value={con} >
                                                                {con}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">Prioridad</div>
                                            <div className="col-md-5">
                                            <select name="param6" ref={this.param6} className="form-control" required>
                                                <option>Seleccione...</option>
                                                {
                                                    this.state.pior.map((pio, i) => {
                                                    return (
                                                            <option key={i} value={pio} >
                                                                {pio}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-5">PBS</div>
                                            <div className="col-md-5">
                                            <select name="param7" ref={this.param7} className="form-control">
                                                {
                                                    this.state.pbss.map((pbs, i) => {
                                                    return (
                                                            <option key={i} value={pbs} >
                                                                {pbs}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <NavLink className="btn btn-secondary pd-x-20" to="/Servicios">Cancelar</NavLink>
                                        <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearserv;