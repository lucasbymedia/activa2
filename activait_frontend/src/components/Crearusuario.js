import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class Creaarrol extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    state = {
        usuas: {},
        role:[],
        status: null
    };
    componentDidMount() {
        axios.get(global.url + "roles/listar").then((res) => {
            if (res.data) {
                const role = res.data;
                this.setState({ role });
                }
          });
    }
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "roles_idroles": this.param1.current.value,
            "usu_nombre": this.param2.current.value,
            "usu_email": this.param3.current.value,
            "password": this.param4.current.value,
            "usu_documento": this.param5.current.value,
            "usu_telefono": this.param6.current.value,
        }
        console.log(tabe);

        axios.post(global.url+"users/Crear", tabe).then(res =>{
            console.log(res);
            swal('Usuario creado', 'Se ha creado el usuario correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Usuario no creado", "Hubo un error al crear el usuario", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Usuarios"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar Usuario</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar Usuario</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Roles</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.role.map((rol, i) => {
                        return (
                                <option key={rol.idroles} value={rol.idroles} >{rol.rol_nombre}</option>
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param2" ref={this.param2} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Email</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} className="form-control" required type="email"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Contraseña</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} className="form-control" required type="password"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Documento</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Teléfono</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Usuarios">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Creaarrol;