import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Diagnosticos extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"diagnosticos/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"diagnosticos/Eliminar/" + id).then((res) => {
                swal("Diagnóstico Eliminado", "Se ha eliminado el diagnóstico", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(!cookies.get("idroles"))
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Diagnósticos</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Diagnósticos</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Creardiag">
                                <i className="icon ion-plus-circled"></i> Agregar Diagnóstico</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th className="wd-20p" align="center">Código</th>
                                            <th className="wd-40p" align="center">Diagnóstico</th>
                                            <th className="wd-20p" align="center">Editar</th>
                                            <th className="wd-20p" align="center">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.iddiagnosticos}>
                            <td align="left">{tab.dia_codigo}</td>
                            <td align="left">{tab.dia_descripcion}</td>
                            <td align="center"><NavLink to={"/Editadiag/"+tab.iddiagnosticos}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.iddiagnosticos)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Diagnosticos;