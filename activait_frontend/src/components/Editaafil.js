import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Editaafil extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    param11 = React.createRef(); 
    param12 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        pers:[],
        ciud:[],
        epss:[],
        esta:['Activo', 'Inactivo'],
        tvin:['Empleado', 'Otro'],
        status: null
    };
    componentDidMount() {

        this.idc=this.props.match.params.id;
        this.getCampos(this.idc);

        axios.get(global.url + "personas/listar").then((res) => {
            if (res.data) {
                const pers = res.data;
                this.setState({ pers });
                }
          });

          axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const ciud = res.data;
                this.setState({ ciud });
                }
          });

          axios.get(global.url + "eps/listar").then((res) => {
            if (res.data) {
                const epss = res.data;
                this.setState({ epss });
                }
          });

    }

    getCampos = (id) => {
        axios.get(global.url + "afiliados/Mostrar/" + id)
        .then(res => {
            const tabe = res.data;
            this.setState(tabe);
        });
    }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "idafiliados": this.idc,
            "personas_idpersonas": this.param2.current.value,
            "ciudades_idciudades": this.param4.current.value,
            "eps_ideps": this.param12.current.value,
            "afi_tipo": this.param5.current.value,
            "afi_cronico": this.param6.current.value,
            "afi_estado": this.param7.current.value,
            "afi_fechaafiliacion": this.param8.current.value,
            "afi_fechaingreso": this.param9.current.value,
            "afi_ibc": this.param10.current.value,
            "afi_tipovinculacion": this.param11.current.value,
        }
        axios.put(global.url+"afiliados/Actualizar/" + this.idc, tabe).then(res =>{
            console.log(res);
            swal('Afiliación editada', 'Se ha editado la afiliación correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Afiliación no editada", "Hubo un error al editar la afiliación", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Afiliaciones"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Editar Afiliación</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Editar Afiliación</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Persona</div>
                <div className="col-md-5">
                  <select name="param2" defaultValue={this.state.personas_idpersonas}  ref={this.param2} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.pers.map((con, i) => {
                        return (
                            this.state.personas_idpersonas===con.idpersonas ? (
                                <option key={con.idpersonas} value={con.idpersonas} selected>{con.per_primernombre} {con.per_primerapellido} </option> ) 
                                :(<option key={con.idpersonas} value={con.idpersonas}>{con.per_primernombre} {con.per_primerapellido} </option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Municipio</div>
                <div className="col-md-5">
                  <select name="param4" defaultValue={this.state.ciudades_idciudades}  ref={this.param4} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.ciud.map((con, i) => {
                        return (
                            this.state.ciudades_idciudades===con.idciudades ? (
                                <option key={con.idciudades} value={con.idciudades} selected>{con.mun_nombre} </option> ) 
                                :(<option key={con.idciudades} value={con.idciudades}>{con.mun_nombre} </option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">EPS</div>
                <div className="col-md-5 derechas">
                <select name="param12" defaultValue={this.state.eps_ideps} ref={this.param12} onChange={this.cambiodep} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.epss.map((con, i) => {
                            return (
                                this.state.eps_ideps===con.ideps ? (
                                    <option key={con.ideps} value={con.ideps} selected>{con.eps_nombre} </option> ) 
                                    :(<option key={con.ideps} value={con.ideps}>{con.eps_nombre} </option> )
                                )
                            })
                        }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo Afiliación</div>
                <div className="col-md-5 derechas">
                    <input name="param5" defaultValue={this.state.afi_tipo} ref={this.param5} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Crónico</div>
                <div className="col-md-5 derechas">
                    <input name="param6" defaultValue={this.state.afi_cronico} ref={this.param6} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Estado</div>
                <div className="col-md-5 derechas">
                <select name="param7" defaultValue={this.state.afi_estado} ref={this.param7} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.esta.map((con, i) => {
                        return (
                            this.state.afi_estado===con ? (
                                <option key={i} value={con} selected>{con}</option> ) 
                                :(<option key={i} value={con}>{con}</option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha de afiliación</div>
                <div className="col-md-5 derechas">
                    <input name="param8" defaultValue={this.state.afi_fechaafiliacion} ref={this.param8} className="form-control"  type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Fecha de ingreso</div>
                <div className="col-md-5 derechas">
                    <input name="param9" defaultValue={this.state.afi_fechaingreso} ref={this.param9} className="form-control"  type="date"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">IBC</div>
                <div className="col-md-5 derechas">
                    <input name="param10" defaultValue={this.state.afi_ibc} ref={this.param10} className="form-control"  type="number"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo de vinculación</div>
                <div className="col-md-5 derechas">
                <select name="param11" defaultValue={this.state.afi_tipovinculacion} ref={this.param11} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.tvin.map((con, i) => {
                        return (
                            this.state.afi_tipovinculacion===con ? (
                                <option key={i} value={con} selected>{con}</option> ) 
                                :(<option key={i} value={con}>{con}</option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Afiliaciones">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Editaafil;