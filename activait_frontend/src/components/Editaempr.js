import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Editaempr extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        arls:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });

          axios.get(global.url + "arls/listar").then((res) => {
            if (res.data) {
                const arls = res.data;
                this.setState({ arls });
                }
          });

          this.getCampos(this.idc);
    }

    getCampos = (id) => {
        axios.get(global.url + "empresas/Mostrar/" + id)
        .then(res => {
            const tabe = res.data;
            this.setState(tabe);
        });
    }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "idempresas": this.idc,
            "ciudades_idciudades": this.param1.current.value,
            "arl_idarl": this.param2.current.value,
            "emp_nombre": this.param3.current.value,
            "emp_nit": this.param4.current.value,
            "emp_email": this.param5.current.value,
            "emp_telefono": this.param6.current.value,
            "emp_representante": this.param7.current.value,
            "emp_direccion": this.param8.current.value,
            "emp_actividad": this.param9.current.value,
            "emp_codactividad": this.param10.current.value,
        }
        console.log(tabe);

        axios.put(global.url+"empresas/Actualizar/" + this.idc, tabe).then(res =>{
            console.log(res);
            swal('Empresa editada', 'Se ha editado la empresa correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Empresa no editada", "Hubo un error al editar la empresa", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Empresas"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Editar Empresa</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Editar Empresa</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Municipio</div>
                <div className="col-md-5">
                  <select name="param1" ref={this.param1} defaultValue={this.state.ciudades_idciudades} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                            this.state.ciudades_idciudades===con.idciudades ? (
                                <option key={con.idciudades} value={con.idciudades} selected>{con.mun_nombre}</option> ) 
                                :(<option key={con.idciudades} value={con.idciudades}>{con.mun_nombre}</option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">ARL</div>
                <div className="col-md-5">
                  <select name="param2" ref={this.param2} defaultValue={this.state.arl_idarl} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.arls.map((arl, i) => {
                        return (
                            this.state.arl_idarl===arl.idarls ? (
                                <option key={arl.idarls} value={arl.idarls} selected>{arl.arl_nombre}</option> ) 
                                :(<option key={arl.idarls} value={arl.idarls}>{arl.arl_nombre}</option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param3" ref={this.param3} defaultValue={this.state.emp_nombre} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">NIT</div>
                <div className="col-md-5 derechas">
                    <input name="param4" ref={this.param4} defaultValue={this.state.emp_nit} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Email</div>
                <div className="col-md-5 derechas">
                    <input name="param5" ref={this.param5} defaultValue={this.state.emp_email} className="form-control" required type="email"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Teléfono</div>
                <div className="col-md-5 derechas">
                    <input name="param6" ref={this.param6} defaultValue={this.state.emp_telefono} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Representante</div>
                <div className="col-md-5 derechas">
                    <input name="param7" ref={this.param7} defaultValue={this.state.emp_representante} className="form-control" type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Dirección</div>
                <div className="col-md-5 derechas">
                    <input name="param8" ref={this.param8} defaultValue={this.state.emp_direccion} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Actividad</div>
                <div className="col-md-5 derechas">
                    <input name="param9" ref={this.param9} defaultValue={this.state.emp_actividad} className="form-control"  type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Código Actividad</div>
                <div className="col-md-5 derechas">
                    <input name="param10" ref={this.param10} defaultValue={this.state.emp_codactividad} className="form-control"  type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Empresas">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Editaempr;