import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Creareps extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        this.getCampos(this.idc);
    }

    getCampos = (id) => {
        axios.get(global.url + "eps/Mostrar/" + id)
        .then(res => {
            const tabe = res.data;
            this.setState(tabe);
        });
    }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "ideps": this.idc,
            "eps_codigo": this.param1.current.value,
            "eps_nombre": this.param2.current.value,
        }
        console.log(tabe);

        axios.put(global.url+"eps/Actualizar/" + this.idc, tabe).then(res =>{
            console.log(res);
            swal('EPS editada', 'Se ha editado la EPS correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("EPS no editada", "Hubo un error al editar la EPS", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Eps"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Agregar EPS</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Agregar EPS</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Código</div>
                <div className="col-md-5 derechas">
                    <input name="param1" ref={this.param1} defaultValue={this.state.eps_codigo} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param2" ref={this.param2} defaultValue={this.state.eps_nombre} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Eps">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Creareps;