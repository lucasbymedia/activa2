import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

class Crearmuni extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    idc = null;
    
    state = {
        tabe: {},
        cons:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        this.getCampos(this.idc);
        axios.get(global.url + "Departamentos/listar").then((res) => {
            if (res.data) {
                const cons = res.data;
                this.setState({ cons });
                }
          });
    }

    getCampos = (id) => {
        axios.get(global.url + "ciudades/Mostrar/" + id)
        .then(res => {
            const tabe = res.data;
            this.setState(tabe);
        });
    }

    guardar = (e) =>{
        e.preventDefault();

        const tabe1 = {
            "idciudades": this.idc,
            "iddepartamentos": this.param1.current.value,
            "mun_codigo": this.param2.current.value,
            "mun_nombre": this.param3.current.value
        }
        console.log(tabe1);

        axios.put(global.url+"ciudades/Actualizar/" + this.idc, tabe1).then(res =>{
            console.log(res);
            swal('Municipio actualizado', 'Se ha actualizar el municipio correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Municipio no actualizado", "Hubo un error al actualizar el municipio", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Municipios"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Editar Municipio</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Editar Municipio</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Departamento</div>
                <div className="col-md-5">
                  <select name="param1" defaultValue={this.state.iddepartamentos} ref={this.param1} className="form-control" required>
                    <option>Seleccione...</option>
                    {
                        this.state.cons.map((con, i) => {
                        return (
                            this.state.iddepartamentos===con.iddepartamentos ? (
                                <option key={i} value={con.iddepartamentos} selected>{con.dep_nombre}</option> ) 
                                :(<option key={i} value={con.iddepartamentos}>{con.dep_nombre}</option> )
                            )
                        })
                    }
                  </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Código</div>
                <div className="col-md-5 derechas">
                    <input name="param2" defaultValue={this.state.mun_codigo} ref={this.param2} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Nombre</div>
                <div className="col-md-5 derechas">
                    <input name="param3" defaultValue={this.state.mun_nombre} ref={this.param3} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Municipios">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Crearmuni;