import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Editarol extends Component {
    param1 = React.createRef(); 
    idc = null;
    state = {
        rols:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        this.getCampos(this.idc);
    }

    getCampos = (id) => {
        axios.get(global.url + "roles/Mostrar/" + id)
        .then(res => {
            const rols = res.data;
            this.setState(rols);
        });
    }

    guardar = (e) =>{
        e.preventDefault();
        axios.put(global.url+"roles/Actualizar/" + this.idc, {
            idroles: this.idc,
            rol_nombre: this.param1.current.value
        }).then(res =>{
            swal('Rol editado', 'Se ha editado el rol correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Rol no editado", "Hubo un error al editar el rol", "error");
            this.setState({ status: 'Mal'})
        });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }

        if(this.state.status==="Ok"){
            return <Redirect to="/Roles"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Editar Rol</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Editar Rol</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
                                        <div className="row">
                                            <div className="col-md-5">Nombre rol</div>
                                            <div className="col-md-5 derechas">
                                                <input name="param1" defaultValue={this.state.rol_nombre} ref={this.param1} className="form-control" required type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <NavLink className="btn btn-secondary pd-x-20" to="/Roles">Cancelar</NavLink>
                                        <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Editarol;