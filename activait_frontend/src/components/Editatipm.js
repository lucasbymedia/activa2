import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Editatipm extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        muni: {},
        cons:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        this.getCampos(this.idc);
    }

    getCampos = (id) => {
        axios.get(global.url + "tipos_mmonitoreo/Mostrar/" + id)
        .then(res => {
            const tabe = res.data;
            this.setState(tabe);
        });
    }
    
    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            "idtipos_monitoreo": this.idc,
            "tip_codigo": this.param1.current.value,
            "tip_descripcion": this.param2.current.value,
        }
        console.log(tabe);

        axios.put(global.url+"tipos_mmonitoreo/Actualizar/" + this.idc, tabe).then(res =>{
            console.log(res);
            swal('Tipo de monitoreo editado', 'Se ha editado el Tipo de monitoreo correctamente', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Tipo de monitoreo no editado", "Hubo un error al editar el Tipo de monitoreo", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Tipos_Monitoreo"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Editar Tipo de Monitoreo</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Editar Tipo de Monitoreo</h6>
                            <form  name="forma" onSubmit={this.guardar}>
                                <div className="modal-content tx-size-sm">
                                    <div className="modal-body pd-20">
            <div className="row">
                <div className="col-md-5">Código</div>
                <div className="col-md-5 derechas">
                    <input name="param1" defaultValue={this.state.tip_codigo} ref={this.param1} className="form-control" required type="text"/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">Tipo de monitoreo</div>
                <div className="col-md-5 derechas">
                    <input name="param2" defaultValue={this.state.tip_descripcion} ref={this.param2} className="form-control" required type="text"/>
                </div>
            </div>
        </div>
        <div className="modal-footer">
            <NavLink className="btn btn-secondary pd-x-20" to="/Tipos_Monitoreo">Cancelar</NavLink>
            <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Editatipm;