import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Empresas extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"empresas/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"empresas/Eliminar/" + id).then((res) => {
                swal("Empresa eliminada", "Se ha eliminado la empresa", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Empresas</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Empresas</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearempr">
                                <i className="icon ion-plus-circled"></i> Agregar Empresa</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>Empresa</th><th>NIT</th>
                                            <th>Teléfono</th><th>Email</th>
                                            <th>Representante</th><th>Editar</th><th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idempresas}>
                            <td align="left">{ tab.emp_nombre }</td><td align="left">{ tab.emp_nit }</td>
                            <td align="left">{ tab.emp_telefono }</td><td align="left">{ tab.emp_email }</td>
                            <td align="left">{ tab.emp_representante }</td>

                            <td align="center"><NavLink to={"/Editaempr/"+tab.idempresas}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idempresas)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Empresas;