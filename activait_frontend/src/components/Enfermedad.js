import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Enfermedad extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    param10 = React.createRef(); 
    param11 = React.createRef(); 
    param12 = React.createRef(); 
    param13 = React.createRef(); 
    param14 = React.createRef(); 
    param15 = React.createRef(); 
    param16 = React.createRef(); 
    param17 = React.createRef(); 
    param18 = React.createRef(); 
    param19 = React.createRef(); 
    param20 = React.createRef(); 
    param21 = React.createRef(); 
    param22 = React.createRef(); 
    param23 = React.createRef(); 
    param24 = React.createRef(); 
    param25 = React.createRef(); 
    param26 = React.createRef(); 
    param27 = React.createRef(); 
    param28 = React.createRef(); 
    param29 = React.createRef(); 
    param30 = React.createRef(); 
    param31 = React.createRef(); 
    param32 = React.createRef(); 
    param33 = React.createRef(); 
    param34 = React.createRef(); 
    param35 = React.createRef(); 
    param36 = React.createRef(); 
    param37 = React.createRef(); 
    param38 = React.createRef(); 
    param39 = React.createRef(); 
    param40 = React.createRef(); 
    param41 = React.createRef(); 
    param42 = React.createRef(); 
    param43 = React.createRef(); 
    param44 = React.createRef(); 
    param45 = React.createRef(); 
    param46 = React.createRef(); 
    param47 = React.createRef(); 
    param48 = React.createRef(); 
    param49 = React.createRef(); 
    param50 = React.createRef(); 
    param51 = React.createRef(); 
    param52 = React.createRef(); 
    param53 = React.createRef(); 
    param54 = React.createRef(); 
    param55 = React.createRef(); 
    param56 = React.createRef(); 
    param57 = React.createRef(); 
    param58 = React.createRef(); 
    param59 = React.createRef(); 
    param60 = React.createRef(); 
    param61 = React.createRef(); 
    param62 = React.createRef(); 
    param63 = React.createRef(); 
    param64 = React.createRef(); 
    param65 = React.createRef(); 
    param66 = React.createRef(); 
    param67 = React.createRef(); 
    param68 = React.createRef(); 
    param70 = React.createRef(); 
    param71 = React.createRef(); 
    param72 = React.createRef(); 
    param73 = React.createRef(); 
    param74 = React.createRef(); 
    param75 = React.createRef(); 
    param76 = React.createRef(); 
    param77 = React.createRef(); 
    param78 = React.createRef(); 
    param79 = React.createRef(); 
    param80 = React.createRef(); 
    param81 = React.createRef(); 
    param82 = React.createRef(); 
    param83 = React.createRef(); 

    param66 = React.createRef(); 
    state = {
        tabe:[],
        geps:[],
        garl:[],
        gafp:[],
        tvin:['Empleador', 'Contratante', 'Cooperativa de trabajo asociado'],
        tvil:['Planta', 'Misión', 'Cooperado', 'Estudiante/Aprendíz', 'Independiente'],
        sexo:['Hombre','Mujer'],
        dept:[],
        ciud:[],
        llen:[],
        rins:[],
        status: null
    };
    componentDidMount() {

        this.documento=this.props.match.params.documento;
        this.getdatos(this.documento);

        axios.get(global.url + "eps/listar").then((res) => {
            const geps = res.data;
            this.setState({ geps });
        });
        axios.get(global.url + "arls/listar").then((res) => {
            const garl = res.data;
            this.setState({ garl });
        });
        axios.get(global.url + "afps/listar").then((res) => {
            const gafp = res.data;
            this.setState({ gafp });
        });

        axios.get(global.url + "departamentos/listar").then((res) => {
            if (res.data) {
                const dept = res.data;
                this.setState({ dept });
                }
          });

          axios.get(global.url + "ciudades/listar").then((res) => {
            if (res.data) {
                const ciud = res.data;
                this.setState({ ciud });
                }
          });

          axios.get(global.url + "eps/listar").then((res) => {
            if (res.data) {
                const epss = res.data;
                this.setState({ epss });
                }
          });

    }
    getdatos = (documento) => {
        axios.get(global.url + "vista_personas/Consulta/" + documento ).then((res) => {
            const tabe = res.data;
            this.setState(tabe);
        });
      }

    guardar = (e) =>{
        e.preventDefault();

        const tabe = {
            'personas_idpersonas': this.state.idpersonas,
            'ciudades_idciudades': this.param27.current.value,
            'sin_fecha': this.param36.current.value,
            'sin_tipoatep': "FUREL",
            'sin_jornada': "",
            'sin_laboral': "", 
            'sin_otra' : "",
            'sin_codigo' : this.param32.current.value,
            'sin_tiempolaborprev': "", 
            'sin_causal': "",
            'sin_muerte': this.param37.current.value,
            'sin_sitio': "",
            'sin_tipolesion': "",
            'sin_parteafectada': "",
            'sin_agente': "",
            'sin_lugar': "",
            'sin_mecanismo': "",
            'sin_descripcion': this.param64.current.value, 
            'sin_responsable': this.param80.current.value,
            'sin_nombreresponsable': this.param80.current.value,
            'sin_cargoresponsable': this.param81.current.value,
            'sin_documentoresponsable': this.param82.current.value,
            'sin_fechadiagnostico': this.param36.current.value,
            'sin_diagnosticadapor': this.param33.current.value,
            'sin_diagnostico': this.param31.current.value, 
            'sin_regmedico': this.param35.current.value,
            'sin_nombremedico': this.param34.current.value,
            'sin_evalmedicapre': this.param65.current.value,
            'sin_evalmedicaper': this.param66.current.value,
            'sin_evalmedicaegre': this.param67.current.value,
            'sin_zona': this.param411.current.value,
            'sin_fechacreacion': new Date(),
            'sin_usucreacion': cookies.get("idusuarios")
        }
        console.log(tabe);

        axios.post(global.url+"siniestros/Crear/", tabe).then(res =>{
            console.log(res);

            axios.get(global.url + "siniestros/consulta/" + this.state.idpersonas).then((res) => {
                const rins = res.data;
                this.setState(rins);
                if(this.param40.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 1, 
                        "fac_clase": this.param40.current.value, 
                        "fac_descripcion": this.param41.current.value, 
                        "fac_tiempoexpactual": this.param42.current.value, 
                        "fac_tiempoexpanter": this.param43.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")

                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param44.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 2, 
                        "fac_clase": this.param44.current.value, 
                        "fac_descripcion": this.param45.current.value, 
                        "fac_tiempoexpactual": this.param46.current.value, 
                        "fac_tiempoexpanter": this.param47.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")
                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param48.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 3, 
                        "fac_clase": this.param48.current.value, 
                        "fac_descripcion": this.param49.current.value, 
                        "fac_tiempoexpactual": this.param50.current.value, 
                        "fac_tiempoexpanter": this.param51.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")
                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param52.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 4, 
                        "fac_clase": this.param52.current.value, 
                        "fac_descripcion": this.param53.current.value, 
                        "fac_tiempoexpactual": this.param54.current.value, 
                        "fac_tiempoexpanter": this.param55.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")
                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param56.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 5, 
                        "fac_clase": this.param56.current.value, 
                        "fac_descripcion": this.param57.current.value, 
                        "fac_tiempoexpactual": this.param58.current.value, 
                        "fac_tiempoexpanter": this.param59.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")
                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param60.current.value!==""){   
                    axios.post(global.url + "factoresrs/Crear/" , {
                        "siniestros_idsiniestros": this.state.idsieniestros,
                        "factores_idfactores_riesgo": 6, 
                        "fac_clase": this.param60.current.value, 
                        "fac_descripcion": this.param61.current.value, 
                        "fac_tiempoexpactual": this.param62.current.value, 
                        "fac_tiempoexpanter": this.param63.current.value,
                        'fac_fechacreacion': new Date(),
                        'fac_usucreacion': cookies.get("idusuarios")

                    })
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                }

                if(this.param68.current.value!==""){   
                    axios
                        .post(global.url + "monitoreos/Crear/" , {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tipos_idtipos_monitoreo": 1, 
                            "mon_respuesta": this.param68.current.value, 
                            "mon_resultado": this.param69.current.value, 
                            "mon_fecha": this.param70.current.value,
                            'mon_fechacreacion': new Date(),
                            'mon_usucreacion': cookies.get("idusuarios")
                            })
                        .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }

                if(this.param71.current.value!==""){   
                    axios
                        .post(global.url + "monitoreos/Crear/" , {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tipos_idtipos_monitoreo": 2, 
                            "mon_respuesta": this.param71.current.value, 
                            "mon_resultado": this.param72.current.value, 
                            "mon_fecha": this.param73.current.value,
                            'mon_fechacreacion': new Date(),
                            'mon_usucreacion': cookies.get("idusuarios")
                        })
                        .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }

                if(this.param74.current.value!==""){   
                    axios
                        .post(global.url + "monitoreos/Crear/" , {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tipos_idtipos_monitoreo": 3, 
                            "mon_respuesta": this.param74.current.value, 
                            "mon_resultado": this.param75.current.value, 
                            "mon_fecha": this.param76.current.value,
                            'mon_fechacreacion': new Date(),
                            'mon_usucreacion': cookies.get("idusuarios")
                        })
                        .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }

                if(this.param77.current.value!==""){   
                    axios
                        .post(global.url + "monitoreos/Crear/" , {
                            "siniestros_idsiniestros": this.state.idsieniestros, 
                            "tipos_idtipos_monitoreo": 4, 
                            "mon_respuesta": this.param77.current.value, 
                            "mon_resultado": this.param78.current.value, 
                            "mon_fecha": this.param79.current.value,
                            'mon_fechacreacion': new Date(),
                            'mon_usucreacion': cookies.get("idusuarios")
                        })
                        .then((res) => {
                            console.log(res);
                        })
                        .catch((error) => {
                            console.log(error);
                    });
                }


            });
            swal('Sinietrso creado', 'Se ha creado correctamente el siniestro', 'success' );
            this.setState({ status: 'Ok'})
        }).catch((error) => {
            swal("Sinietrso no creado", "Hubo un error al crear el siniestro", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
        });
    }
    render() {
        if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Radicacion"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Enfermedad Laboral</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                        <h6 className="card-body-title">Enfermedad Laboral</h6><br/>
<div className="row">
  <div className="col-lg">
    <input className="form-control" defaultValue={this.state.per_primernombre} ref={this.param1} placeholder="Trabajador" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" defaultValue={this.state.per_numeroid} ref={this.param2} placeholder="Documento" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" defaultValue={this.state.emp_nombre} ref={this.param3} placeholder="Empleador" type="text" required />
  </div>
  <div className="col-lg mg-t-10 mg-lg-t-0">
    <input className="form-control" placeholder="Código reporte" type="text" />
  </div>
</div>
<div className="card pd-10 pd-sm-20 mg-t-20">
  <h2 className="card-body-title">INFORME DE ENFERMDAD LABORAL DEL EMPLEADOR O CONTRATANTE</h2>
  <br />
  <form onSubmit={this.guardar} name="forma">
      <h4>Afiliaciones</h4>
      <div className="row">
          <div className="col-lg-12">
            <select className="form-control" defaultValue={this.state.ideps} ref={this.param4} name="eps" required>
                <option value="">EPS del Afiliado Código</option>
                    {
                        this.state.geps.map((con, i) => {
                        return (
                            this.state.ideps===con.ideps ? (
                                <option key={con.ideps} value={con.ideps} selected>{con.eps_nombre}</option> ) 
                                :(<option key={con.ideps} value={con.ideps}>{con.eps_nombre}</option> )
                            )
                        })
                    }
            </select><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <select className="form-control" defaultValue={this.state.idarls} ref={this.param5} name="arl" required>
                  <option value="">ARL del Afiliado Código</option>
                  {
                        this.state.garl.map((con, i) => {
                        return (
                            this.state.idarls===con.idarls ? (
                                <option key={con.idarls} value={con.idarls} selected>{con.arl_nombre}</option> ) 
                                :(<option key={con.idarls} value={con.idarls}>{con.arl_nombre}</option> )
                            )
                        })
                    }
              </select><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-4">
              Seguro social<br/>
            <div className="row">
                <div className="col-lg-3">Si</div>
                <div className="col-lg-3"><input className="form-control" value="Si" type="radio" /></div>
                <div className="col-lg-3">No</div>
                <div className="col-lg-3"><input defaultChecked className="form-control" value="No" type="radio" /></div>
            </div>
          </div>
          <div className="col-lg mg-t-10 mg-lg-t-0">
             <input className="form-control" placeholder="Cuál" id="cual" name="cual" type="text"/>
             <br/><br/>
          </div>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <select className="form-control" defaultValue={this.state.idafps} ref={this.param6} name="afp" required>
                  <option value="">AFP del Afiliado</option>
                  {
                        this.state.gafp.map((con, i) => {
                        return (
                            this.state.idafps===con.idafps ? (
                                <option key={con.idafps} value={con.idafps} selected>{con.afp_nombre}</option> ) 
                                :(<option key={con.idafps} value={con.idafps}>{con.afp_nombre}</option> )
                            )
                        })
                    }
              </select><br/>
          </div>
      </div>
      <h4> I. Identificación General</h4>
      <h6>I. IDENTIFICACIÓN GENERAL DEL EMPLEADOR, CONTRATANTE O COOPERATIVA</h6>  
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">Tipo de vinculador laboral</h6>
          </div>
       </div>
      <div className="row">   
       {
            this.state.tvin.map((con, i) => {
                return (
                    
                    this.state.afi_tipovinculacion===con ? (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" checked className="form-control" name="param7" required defaultValue={this.state.afi_tipovinculacion} ref={this.param7} value={con}/></div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" className="form-control" name="param7" required defaultValue={this.state.afi_tipovinculacion} ref={this.param7} value={con}/></div>
                    </React.Fragment>
                    )
                )
            })
        }
        <br/><br/>
      </div>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">SEDE PRINCIPAL</h6><br/>
          </div>
      </div>   
      <div className="row">
          <div className="col-lg-8">
             <input type="text" className="form-control" defaultValue={this.state.emp_actividad} ref={this.param8} required name="actec"
              placeholder="Nombre de la actividad económica"/>
          </div>
          <div className="col-lg-4">
             <input type="number" className="form-control" defaultValue={this.state.emp_codactividad} ref={this.param9}  
             required name="codac" placeholder="Código"/><br/>
           </div>
       </div>    
      <div className="row">
          <div className="col-lg-6">
             <input type="text" className="form-control" defaultValue={this.state.emp_nombre} ref={this.param10}
             required name="nombr" placeholder="Nombre Razón social" />
          </div>
          <div className="col-lg-2">
              <select className="form-control" required>
                  <option value="NI" selected>NI</option>
                  <option value="CC">CC</option>
                  <option value="CE">CE</option>
                  <option value="NU">NU</option>
                  <option value="PA">PA</option>
              </select><br/>
           </div>
           <div className="col-lg-4">
              <input type="text" className="form-control" defaultValue={this.state.emp_nit} ref={this.param10}
              required name="ndoc" placeholder="Número"/>
           </div>
        </div>    
      <div className="row">
          <div className="col-lg-6">
             <input type="text" className="form-control" defaultValue={this.state.emp_direccion} ref={this.param11} 
             name="dire" placeholder="Dirección" required/>
          </div>
          <div className="col-lg-2">
              <select className="form-control" id="zona" name="zona" required>
                  <option value="U" selected>Urbana</option>
                  <option value="R">Rural</option>
              </select><br/>
           </div>
        </div>    
      <br/>
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">CENTRO DE TRABAJO DONDE LABORA EL TRABAJADOR</h6><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-9">
              ¿SON LOS DATOS DEL CENTRO DE TRABAJO LOS MISMOS DE LA SEDE PRINCIPAL? <br/>
              <span className="pequeno">Solo en caso negativo diligenciar las siguientes casillas del centro de trabajo</span>
          </div>
          <div className="col-lg-3">
              <div className="row">
                  <div className="col-lg-3">Si</div>
                  <div className="col-lg-3"><input checked className="form-control" value="Si" type="radio" required/></div>
                  <div className="col-lg-3">No</div>
                  <div className="col-lg-3"><input className="form-control" value="No" type="radio" required /></div>
              </div>
              <br/>
          </div>
      </div>
      
      <div className="row">
          <div className="col-lg-8">
             <input type="text" defaultValue={this.state.emp_actividad} ref={this.param12} className="form-control" name="actc" 
             placeholder="Nombre de la actividad económica del Centro de Trabajo"/>
          </div>
          <div className="col-lg-4">
              <input type="text" defaultValue={this.state.emp_codactividad} ref={this.param13} className="form-control" name="cact" 
              placeholder="Código de actividad"/><br/>
          </div>
        </div>    
        <div className="row">
          <div className="col-lg-6">
             <input type="text" defaultValue={this.state.emp_direccion} ref={this.param14} className="form-control" name="dir2" 
             placeholder="Dirección"/>
          </div>
           <div className="col-lg-2">
              <select className="form-control" id="zon2" name="zon2" required>
                  <option value="U" selected>Urbana</option>
                  <option value="R">Rural</option>
              </select><br/>
           </div>
        </div>   
        <h4> II. Información de la persona enferma</h4> 
        <h6>II. INFORMACIÓN DE LA PERSONA QUE SE ENFERMÓ</h6>  
      <div className="row">
          <div className="col-lg-12">
              <h6 className="titul2">TIPO DE VINCULADOR LABORAL</h6>
          </div>
       </div>
       <div className="row">
       {
            this.state.tvil.map((con, i) => {
                return (
                    
                    this.state.per_tipovinculacion===con ? (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" checked className="form-control" name="param15" required defaultValue={this.state.per_tipovinculacion} ref={this.param15} value={con}/></div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}><div className="col-lg-3">{con}</div>
                    <div className="col-lg-1">
                    <input type="radio" className="form-control" name="param15" required defaultValue={this.state.per_tipovinculacion} ref={this.param15} value={con}/></div>
                    </React.Fragment>
                    )
                )
            })
        }
            <div className="col-lg-4">
              <input type="text" className="form-control" required
              defaultValue={this.state.per_tipoid} ref={this.param16} name="coda" placeholder="Código"/><br></br>
            </div>
      </div>
      <div className="row">
          <div className="col-lg-3">
             <input type="text" className="form-control" defaultValue={this.state.per_primerapellido} ref={this.param17} 
             required id="ape1" name="ape1" placeholder="Primer Apellido" />
          </div>
          <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_segundoapellido} ref={this.param18} 
              id="ape2" name="ape2" placeholder="Segundo Apellido" />
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_primernombre} ref={this.param19}  
              required id="nom1" name="nom1" placeholder="Primer Nombre" />
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_segundonombre} ref={this.param20} 
              id="nom2" name="nom2" placeholder="Segundo Nombre" /><br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-2">
              <select className="form-control"  required>
                  <option value="">Tipo identificación</option>
                  <option value="NI">NI</option>
                  <option value="CC" selected>CC</option>
                  <option value="CE">CE</option>
                  <option value="NU">NU</option>
                  <option value="PA">PA</option>
              </select><br/>
          </div>
          <div className="col-lg-3">
              <input type="text" className="form-control" defaultValue={this.state.per_numeroid} ref={this.param21} 
              required name="docp" placeholder="Documento"/>
           </div>
           <div className="col-lg-4">Fecha nacimiento<br/>
              <input type="date" className="form-control" defaultValue={this.state.per_nacimiento} ref={this.param22}
               required name="fnac" placeholder="Fecha de nacimiento"/><br/>
           </div>
           <div className="col-lg-3">
           {
            this.state.sexo.map((con, i) => {
                return (
                    
                    this.state.per_genero===con ? (
                    <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-8">{con}</div>
                        <div className="col-lg-4">
                        <input type="radio" checked className="form-control" name="param23" required defaultValue={this.state.per_genero} 
                        ref={this.param23} value={con}/></div>
                    </div>
                    </React.Fragment> ) : (
                    <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-8">{con}</div>
                        <div className="col-lg-4">
                        <input type="radio" className="form-control" name="param23" required defaultValue={this.state.per_genero} ref={this.param23} value={con}/></div>
                    </div>
                    </React.Fragment>
                    )
                )
            })
        }
            </div>
      </div>    
      <div className="row">
          <div className="col-lg-6">
             <input type="text" defaultValue={this.state.per_direccion} ref={this.param24} className="form-control" name="dirp" placeholder="Dirección" required/>
          </div>
          <div className="col-lg-3">
              <input type="text" defaultValue={this.state.per_telefono} ref={this.param25} required className="form-control" name="telp" placeholder="Teléfono"/>
              <br/>
           </div>
           <div className="col-lg-3">
              <input type="text" className="form-control" id="faxp" name="faxp" placeholder="Fax"/>
              <br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
              <select className="form-control" defaultValue={this.state.iddepartamentos}  ref={this.param26} name="depa" required>
                  <option value="">Departamento</option>
                  {
                        this.state.dept.map((con, i) => {
                        return (
                            this.state.iddepartamentos===con.iddepartamentos ? (
                                <option key={con.iddepartamentos} value={con.iddepartamentos} selected>{con.dep_nombre} </option> ) 
                                :(<option key={con.iddepartamentos} value={con.iddepartamentos}>{con.dep_nombre} </option> )
                            )
                        })
                    }
              </select>
          </div>
          <div className="col-lg-5">
              <select className="form-control" defaultValue={this.state.idciudades} ref={this.param27} name="ciud" required>
                  <option value="">Municipio</option>
                  {
                        this.state.ciud.map((con, i) => {
                        return (
                            this.state.idciudades===con.idciudades ? (
                                <option key={con.idciudades} value={con.idciudades} selected>{con.mun_nombre} </option> ) 
                                :(<option key={con.idciudades} value={con.idciudades}>{con.mun_nombre} </option> )
                            )
                        })
                    }
              </select>
          </div>
          <div className="col-lg-2">
              <select className="form-control" required>
                  <option value="U" selected>Urbana</option>
                  <option value="R">Rural</option>
              </select><br/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
             <input type="text" defaultValue={this.state.per_cargo} ref={this.param28} className="form-control" name="carg" placeholder="Cargo que desempeña"/>
          </div>
          <div className="col-lg-5">
              <input type="text" className="form-control" name="ocup" placeholder="Ocupación Habitual"/><br/>
          </div>
          <div className="col-lg-2">
              <input type="text" className="form-control" name="codc" placeholder="Código"/><br/>
          </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
             Tiempo de ocupación habitual al momento del accidente
          </div>
          <div className="col-lg-2">
              <input type="number" className="form-control" id="dias" name="dias" placeholder="DD"/>
              <br/>
           </div>
           <div className="col-lg-2">
              <input type="number" className="form-control" id="mese" name="mese" placeholder="MM"/>
              <br/>
           </div>
           <div className="col-lg-3">Fecha de ingreso a la empresa
              <input type="date" defaultValue={this.state.afi_fechaafiliacion} ref={this.param29}  className="form-control" name="fvin" 
              placeholder="Fecha de ingreso a la empresa" required/>
           </div>
      </div>    
      <div className="row">
          <div className="col-lg-5">
              <input type="number" defaultValue={this.state.afi_ibc} ref={this.param30} className="form-control" required name="sala" 
              placeholder="Salario u horonarios"/>
              <br/>
           </div>
           <div className="col-lg-3">
              <select className="form-control" id="zon3" name="zon3" required>
                  <option value="Diurna" selected>Diurna</option>
                  <option value="Nocturna">Nocturna</option>
                  <option value="Mixto">Mixto</option>
                  <option value="Turnos">Turnos</option>
              </select><br/>
           </div>
      </div>   

      <h4>III. Información de la enfermedad</h4> 
      <h6>III. INFORMACIÓN DE LA ENFERMEDAD</h6><br></br>  
                        <div className="row">
                            <div className="col-lg-7">
                                <input type="text" v-model="sin.sin_diagnostico" className="form-control" 
                                name="diag" ref={this.param31} placeholder="Diagnóstico" required/><br></br></div>
                            <div className="col-lg-5">
                            <input type="text" ref={this.param32} className="form-control" name="cdia" placeholder="Código diagnóstico" required/><br/>
                            </div>  
                        </div>
                        <div className="row">
                            <div className="col-lg-9"><b>Diagnosticada por:</b>
                                <div className="row">
                                    <div className="col-lg-3">MD EPS*</div>
                                    <div className="col-lg-3"><input name="diap" ref={this.param33} className="form-control" value="MD EPS" type="radio" required/></div>
                                    <div className="col-lg-3">MD IPS*</div>
                                    <div className="col-lg-3"><input name="diap" ref={this.param33} className="form-control" value="MD IPS" type="radio" required/></div>
                                    <div className="col-lg-3">MD ARF*</div>
                                    <div className="col-lg-3"><input name="diap" ref={this.param33} className="form-control" value="MD ARF" type="radio" required/></div>
                                    <div className="col-lg-3">MD EMPRESA*</div>
                                    <div className="col-lg-3"><input name="diap" ref={this.param33} className="form-control" value="MD EMPRESA" type="radio" required/></div>
                                    <div className="col-lg-3">MD PARTICULAR*</div>
                                    <div className="col-lg-3"><input name="diap" ref={this.param33} className="form-control" value="MD PARTICULAR" type="radio" required/>
                                    <br></br></div>
                                </div>
                            </div>    
                            <div className="col-lg-3">
                            <i>* Médicos con capacidad de diagnóstico sin competencia legal para determinar origen</i><br/><br/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4">
                                <input className="form-control" ref={this.param34} type="text" name="medi" placeholder="Apellidos y Nombres" required />
                            </div>
                            <div className="col-lg-4">
                                <input className="form-control" ref={this.param35} type="text" name="regm" placeholder="Registro médico" required/><br/>
                            </div>
                            <div className="col-lg-4">Fecha del diagnóstico
                                <input className="form-control" ref={this.param36} type="date" name="fecd" required/><br/>
                            </div>
                        </div>  
                        <div className="row">
                        <div className="col-lg-4">¿Causó la muerte al trabajador?
                            <div className="row">
                                <div className="col-lg-3">Si</div>
                                <div className="col-lg-3"><input name="muer" ref={this.param37} className="form-control" value="Si" type="radio"  required /></div>
                                <div className="col-lg-3">No</div>
                                <div className="col-lg-3"><input name="muer"ref={this.param37} className="form-control" value="No" type="radio"  required /><br/></div>
                            </div>
                            <br/><br/>
                        </div>
                        <div className="col-lg-4">Fecha de la muerte
                            <input className="form-control" type="date" name="fecm" ref={this.param38} /><br/>
                        </div>
                        </div>    
                    <h4>IV. Información factores de riesgo</h4> 
                    <h6>IV. INFORMACIÓN SOBRE FACTORES DE RIESGO ASOCIADOS CON LA ENFERMEDAD</h6>
                    <div className="row">
                    <div className="col-lg-4"><b>Existe información?</b><br></br>
                        <div className="row">
                            <div className="col-lg-3">Si</div>
                            <div className="col-lg-3"><input name="ezai" ref={this.param39} className="form-control" value="Si" type="radio" required/></div>
                            <div className="col-lg-3">No</div>
                            <div className="col-lg-3"><input name="ezai" ref={this.param39} className="form-control" value="No" type="radio" required/><br/></div>
                        </div>
                        <br/><br/>
                    </div>
                    <div className="col-lg-8"><i>En caso afirmativo, indique los factores de riesgo a los cuales ha estado expuesto el trabajador y que se encuentren 
                        relacionados con la enfermedad, para cada uno de llos indique tiempo de exposición según se trate d elo actual o anterior, de acuerdo con 
                        los antecedentes y fundamentos tmoados en cuenta por el médico que diagnosticó la enfermedad y la historia laboral del trabajador</i><br/><br/>
                    </div>
                    </div>    
                    <div className="row">
                        <div className="col-lg-12">
                        <table>
                            <tr className='titul2'><td></td>
                            <td>FACTOR DE RIESGO</td>
                            <td>¿CUÁLES FACTORES?</td>
                            <td>TIEMPO EXPOSICIÓN EMPRESA O CONTRATO</td>
                            <td>TIEMPO EXPOSICIÓN EMPRESA O CONTRATO</td>
                            </tr>
                            <tr>
                            <td>FÍSICO</td>
                            <td><input ref={this.param40} className="form-control" type="text" name="rif1" /> </td>
                            <td><input ref={this.param41} className="form-control" type="text" name="rif2" /> </td>
                            <td><input ref={this.param42} className="form-control" type="text" name="rif3" /> </td>
                            <td><input ref={this.param43} className="form-control" type="text" name="rif4" /> </td>
                            </tr>
                            <tr>
                            <td>QUÍMICO</td>
                            <td><input ref={this.param44} className="form-control" type="text" name="riq1"/> </td>
                            <td><input ref={this.param45} className="form-control" type="text" name="riq2"/> </td>
                            <td><input ref={this.param46} className="form-control" type="text" name="riq3"/> </td>
                            <td><input ref={this.param47} className="form-control" type="text" name="riq4"/> </td>
                            </tr>
                            <tr>
                            <td>BIOlÓGICO</td>
                            <td><input ref={this.param48} className="form-control" type="text" name="rib1"/> </td>
                            <td><input ref={this.param49} className="form-control" type="text" name="rib2"/> </td>
                            <td><input ref={this.param50} className="form-control" type="text" name="rib3"/> </td>
                            <td><input ref={this.param51} className="form-control" type="text" name="rib4"/> </td>
                            </tr>
                            <tr>
                            <td>ERGONÓMICO</td>
                            <td><input ref={this.param52} className="form-control" type="text" name="rie1"/> </td>
                            <td><input ref={this.param53} className="form-control" type="text" name="rie2"/> </td>
                            <td><input ref={this.param54} className="form-control" type="text" name="rie3"/> </td>
                            <td><input ref={this.param55} className="form-control" type="text" name="rie4"/> </td>
                            </tr>
                            <tr>
                            <td>PSICOSOCIAL</td>
                            <td><input ref={this.param56} className="form-control" type="text" name="rip1"/> </td>
                            <td><input ref={this.param57} className="form-control" type="text" name="rip2"/> </td>
                            <td><input ref={this.param58} className="form-control" type="text" name="rip3"/> </td>
                            <td><input ref={this.param59} className="form-control" type="text" name="rip4"/> </td>
                            </tr>
                            <tr>
                            <td>AMBIENTAL</td>
                            <td><input ref={this.param60} className="form-control" type="text" name="ria1"/> </td>
                            <td><input ref={this.param61} className="form-control" type="text" name="ria2"/> </td>
                            <td><input ref={this.param62} className="form-control" type="text" name="ria3"/> </td>
                            <td><input ref={this.param63} className="form-control" type="text" name="ria4"/> </td>
                            </tr>
                        </table>
                        <br/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">DESCRIPCION DE LA ACTIVIDAD LABORAL DESEMPEÑADA (NEXO LABORAL):
                        <textarea className="form-control" ref={this.param64} name="desc" required></textarea><br/>
                    </div>
                    </div>
                  <h4>V. Información de evaluaciones médicas</h4>
                  <h6>V. INFORMACIÓN SOBRE EVALUACIONES MÉDICAS OCUPACIONALES REALIZADAS AL TRABAJADOR</h6><br/>
                    DETECCIÓN DE LA ENFERMEDAD POR ALGUNA DE LAS SIGUIENTES EVALUACIONES MÉDICAS OCUPACIONALES<br/>
                    <i>(Sólo marque sí o no, si diagnosticó la enfermedad por alguna de las evaluaciones citadas, realizadas al trabajador</i><br/><br/>   
                    <div className="row">
                        <div className="col-lg-4">¿Evaluación MD Pre-Ocupacional?
                            <div className="row">
                                <div className="col-lg-3">Si</div>
                                <div className="col-lg-3"><input ref={this.param65} className="form-control" value="Si" type="radio"  required/></div>
                                <div className="col-lg-3">No</div>
                                <div className="col-lg-3"><input ref={this.param65} className="form-control" value="No" type="radio"  required/><br/></div>
                            </div>
                            <br/>
                        </div>
                        <div className="col-lg-4">¿Evaluación MD Periódica Ocupacional?
                        <div className="row">
                            <div className="col-lg-3">Si</div>
                            <div className="col-lg-3"><input ref={this.param66} className="form-control" value="Si" type="radio"  required/></div>
                            <div className="col-lg-3">No</div>
                            <div className="col-lg-3"><input ref={this.param66} className="form-control" value="No" type="radio"  required/><br/></div>
                        </div>
                        <br/>
                    </div>
                    <div className="col-lg-4">¿Evaluación de egreso?
                        <div className="row">
                            <div className="col-lg-3">Si</div>
                            <div className="col-lg-3"><input ref={this.param67} className="form-control" value="Si" type="radio"  required/></div>
                            <div className="col-lg-3">No</div>
                            <div className="col-lg-3"><input ref={this.param67} className="form-control" value="No" type="radio"  required/><br/></div>
                        </div>
                        <br/>
                    </div>
                    </div>
                    <h4>VI. Monitoreos relacionados</h4>
                    <h6>VI. MONITOREOS RELACIONADOS CON LA ENFERMEDAD</h6>  
                    <div className="row">
                        <div className="col-lg-12"><i>En el caso de que cualquiera de los siguientes hubiera sido fundamento para el diagnóstico de la enfermedad, 
                        deberán citarse y anotar la fecha de su realización en la casilla correspondiente</i><br/><br/>

                        <table>
                            <tr className='titul2'>
                            <td>MONITOREO DEL RIESGO</td>
                            <td width="10%">SI</td><td width="10%">NO</td>
                            <td>TIPO - CUÁLES - RESULTADO</td>
                            <td>FECHA</td>
                            </tr>
                            <tr>
                            <td>Mediciones ambientales</td>
                            <td><input className="form-control" name="mon1" ref={this.param68} type="radio" value="Si" /></td><td>
                            <input className="form-control"  name="mon1" ref={this.param68} type="radio" value="No" /> </td>
                            <td><input className="form-control" ref={this.param69} type="text" name="mor2" /> </td>
                            <td><input className="form-control" ref={this.param70} type="date" name="mor3" /> </td>
                            </tr>
                            <tr>
                            <td>Indicadores biológicos (Si está reportando COVID-19, indique si tiene prueba RT-PCR y su resultado)</td>
                            <td><input className="form-control" ref={this.param71} type="radio" name="mon2" value="Si" /></td><td>
                            <input className="form-control"  ref={this.param71} type="radio" name="mon2" value="No" /> </td>
                            <td><input className="form-control" ref={this.param72} type="text" name="mob2"/> </td>
                            <td><input className="form-control" ref={this.param73} type="date" name="mob3"/> </td>
                            </tr>
                            <tr>
                            <td>Estudio de puesto de trabajo</td>
                            <td><input className="form-control" ref={this.param74} name="mon3" type="radio" value="Si"/></td><td>
                            <input className="form-control"  ref={this.param74} name="mon3" type="radio" value="No"/> </td>
                            <td><input className="form-control" ref={this.param75} type="text" name="moe2"/> </td>
                            <td><input className="form-control" ref={this.param76} type="date" name="moe3"/> </td>
                            </tr>
                            <tr>
                            <td>Otros</td>
                            <td><input className="form-control" ref={this.param77} name="mon4" type="radio" value="Si" /></td><td>
                            <input className="form-control" ref={this.param77} name="mon4" type="radio" value="No" /> </td>
                            <td><input className="form-control" ref={this.param78} name="moo2" type="text"/> </td>
                            <td><input className="form-control" ref={this.param79} name="moo3" type="date"/> </td>
                            </tr>
                        </table>
                        <br/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <h6 className='titul2'>PERSONA RESPONSABLE DEL INFORME</h6><br/>
                        </div>
                    </div>    
                    <div className="row">
                        <div className="col-lg-4">
                            <input className="form-control" ref={this.param80} placeholder="Apellidos y Nombres completos" type="text" name="resp"/>
                        </div>    
                        <div className="col-lg-3">
                            <input className="form-control" ref={this.param81} placeholder="Cargo" type="tec1" name="cres"/>
                        </div>    
                        <div className="col-lg-2">
                            <select className="form-control" name="tres" required>
                                <option value="">Tipo identificación</option>
                                <option value="NI">NI</option>
                                <option value="CC" selected>CC</option>
                                <option value="CE">CE</option>
                                <option value="NU">NU</option>
                                <option value="PA">PA</option>
                            </select><br/>
                        </div>
                        <div className="col-lg-3">
                            <input type="text" ref={this.param82} className="form-control" name="dres" placeholder="Documento"/><br/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4">Fecha de diligenciamiento del informe<br/>
                            <input className="form-control" ref={this.param83} type="date" name="fdil" required/><br/>
                        </div>    
                    </div>
                    <div className="row">
                        <div className="col-lg-6">Firma<br/>
                        </div>
                        <div className="col-lg-6">Antes de continuar, por favor verifique que la información es correcta<br/>
                        </div>    
                    </div>
                    <div className="row">
                    <div className="col-lg-6"></div>
                    <div className="col-lg-6" align="right"><br/>
                        <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" />
                    </div>
                </div>
  </form>
</div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Enfermedad;