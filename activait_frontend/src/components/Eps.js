import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Eps extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"eps/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"eps/Eliminar/" + id).then((res) => {
                swal("EPS Eliminada", "Se ha eliminado la EPS", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Entidades Prestadoras de Salud</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">EPS</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Creareps">
                                <i className="icon ion-plus-circled"></i> Agregar EPS</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th className="wd-20p" align="center">Código</th>
                                            <th className="wd-40p" align="center">Nombre EPS</th>
                                            <th className="wd-20p" align="center">Editar</th>
                                            <th className="wd-20p" align="center">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.ideps}>
                            <td align="left">{tab.eps_codigo}</td>
                            <td align="left">{tab.eps_nombre}</td>
                            <td align="center"><NavLink to={"/Editaeps/"+tab.ideps}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.ideps)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Eps;