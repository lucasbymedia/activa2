import React, { Component } from 'react'
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';

class Error extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
               <div className="am-pagetitle">
                    <h5 className="am-title">Error de navegación</h5>
                </div>
                <div class="am-mainpanel">
                    <div class="am-pagebody">
                        <div class="card pd-20 pd-sm-40">
                            <h4>Página no encontrada</h4>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );

    }
}
export default Error;