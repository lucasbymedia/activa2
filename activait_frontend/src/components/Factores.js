import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Factores extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"factoress/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"factoress/Eliminar/" + id).then((res) => {
                swal("Factor de riesgo Eliminado", "Se ha eliminado el factor de riesgo", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }
        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Factores de riesgo</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Factores de riesgo</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearfactor">
                                <i className="icon ion-plus-circled"></i> Agregar Factor de riesgo</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th className="wd-50p" align="center">Factor de riesgo</th>
                                            <th className="wd-25p" align="center">Editar</th>
                                            <th className="wd-25p" align="center">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idfactoresriesgo}>
                            <td align="left">{tab.fac_codigo}</td>
                            <td align="center"><NavLink to={"/Editafactor/"+tab.idfactoresriesgo}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idfactoresriesgo)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Factores;