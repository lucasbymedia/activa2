import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import logo from '../assets/images/image007.jpg';
import pers from '../assets/images/img5.jpg';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Header extends Component {
    render() {
        return (

            <div className="am-header">
                <div className="am-header-left">
                    <NavLink to="/Inicio" className="am-logo"><img src={logo} alt="Logo" height="40px" /></NavLink>
                </div>

                <div className="am-header-right">
                    <div className="dropdown dropdown-notification">
                        <NavLink to="/" className="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                            <i className="icon ion-ios-bell-outline tx-24"></i>
                            <span className="square-8 bg-danger pos-absolute t-15 r-0 rounded-circle"></span>
                        </NavLink>
                        <div className="dropdown-menu wd-300 pd-0-force">
                            <div className="dropdown-menu-header">
                                <label>Notificaciones</label>
                            </div>
                            <div className="media-list">
                                <div className="media-list-footer">
                                    <NavLink to="/Notificaciones" className="tx-12">
                                        <i className="fa fa-angle-down mg-r-5"></i> Mostrar todo
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dropdown dropdown-profile">
                        <NavLink to="/" className="nav-link nav-link-profile" data-toggle="dropdown">
                            <img src={pers} className="wd-32 rounded-circle" alt="" />
                            <span className="logged-name"><span className="hidden-xs-down tituls">
                            {
                                cookies.get("nombre")
                            }</span>
                            <i className="fa fa-angle-down mg-l-3"></i></span>
                        </NavLink>
                        <div className="dropdown-menu wd-200">
                            <ul className="list-unstyled user-profile-nav">
                                <li><NavLink to="/EditarPerfil"><i className="icon ion-ios-person-outline"></i> Editar perfil</NavLink></li>
                                <li><NavLink to="/Login"><i className="icon ion-power"></i> Salir</NavLink></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Header;