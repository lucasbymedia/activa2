import React, { Component } from 'react'
import logo from '../assets/images/image007.jpg';
import { NavLink } from 'react-router-dom';

class Header1 extends Component {

    render() {
        return (

            <div className="am-header">
                <div className="am-header-left">
                    <NavLink to="/Inicio" className="am-logo"><img src={logo} alt="Logo" height="40px" /></NavLink>
                </div>
            </div>
        );
    }
}
export default Header1;