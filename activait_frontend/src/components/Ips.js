import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Ips extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"ips/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"ips/Eliminar/" + id).then((res) => {
                swal("IPS Eliminada", "Se ha eliminado la IPS", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }
        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Instituciones Prestadoras de Salud</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">IPS</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearips">
                                <i className="icon ion-plus-circled"></i> Agregar IPS</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th className="wd-20p" align="center">NIT</th>
                                            <th className="wd-20p" align="center">Nombre</th>
                                            <th className="wd-20p" align="center">Contácto</th>
                                            <th className="wd-20p" align="center">Dirección</th>
                                            <th className="wd-20p" align="center">Teléfono</th>
                                            <th className="wd-15p" align="center">Editar</th>
                                            <th className="wd-15p" align="center">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idips}>
                            <td align="left">{tab.ips_nit}</td>
                            <td align="left">{tab.ips_nombre}</td>
                            <td align="left">{tab.ips_contacto}</td>
                            <td align="left">{tab.ips_direccion}</td>
                            <td align="left">{tab.ips_telefono}</td>
                            <td align="center"><NavLink to={"/Editaips/"+tab.idips}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idips)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Ips;