import React, { Component } from 'react';
import Header1 from './Header1';
import Menulat1 from './Menulat1';
import Footer from './Footer';
import axios from 'axios';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import global from '../Global'; 
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

class Inicio extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        status: null,
        correo: "",
        clave: "",
    };

    Login = (e) => {
        e.preventDefault();
        var correo=this.param1.current.value;
        var clave=this.param2.current.value;

        axios
        .post(global.url + "users/login/", {email: correo, password: clave})
        .then((res) => {
            var respuesta=res.data;
            console.log(respuesta);
            cookies.set("idusuarios", respuesta.idusuarios, {path:"/"});
            cookies.set("idroles", respuesta.idroles, {path:"/"});
            cookies.set("nombre", respuesta.nombre, {path:"/"});
            cookies.set("token", respuesta.token, {path:"/"});
            this.setState({ status: 'Ok'})
        })
        .catch((error) => {
          swal("Login incorrecto", "Usuarrio o password incorrecto", "error");
          console.log(error);
        });
    }

    componentDidMount(){
      cookies.remove("idusuarios");
      cookies.remove("idroles");
      cookies.remove("nombre");
    }
    render() { 

        if(this.state.status==="Ok"){
            return <Redirect to="/Inicio"/>;
        }
        else {}

        return (
            <div>
                <Header1></Header1>
                <Menulat1></Menulat1>
                <div className="am-pagetitle">
                    <h5 className="am-title">Ingreso al sistema de Autorizaciones</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                        <div className="row">
        <div className="col-lg-4"></div>
        <div className="col-lg-4">
          <h2 className="tx-gray-800 mg-b-25">Bienvenido</h2>
          <form onSubmit={this.Login} name="forma">
            <div className="form-group">
              <label className="form-control-label">Email:</label>
              <input ref={this.param1} className="form-control" required placeholder="Email" />
            </div>
            <div className="form-group">
              <label className="form-control-label">Contraseña:</label>
              <input ref={this.param2} type="password" required className="form-control" placeholder="Contraseña"/>
            </div>
            <button type="submit" className="btn btn btn-info pd-x-120">Ingresar</button>
            <br></br><br></br>
          </form>
        </div>
      </div>

                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Inicio;