import React, { Component } from 'react'
import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';
import { NavLink } from 'react-router-dom';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Menulat extends Component {

    
    render() {
        return (
            <div className="am-sideleft colorback">
                <ul className="nav am-sideleft-tab colorback">
                    <li className="nav-item">
                        <NavLink to="/Inicio" className="nav-link active">
                            <i className="icon ion-ios-home-outline tx-24"></i>
                        </NavLink>
                    </li>
                </ul>
                <div className="tab-content">
                    <div id="mainMenu" className="tab-pane active">
                        <ProSidebar>
                            <Menu iconShape="square">
                                {
                                    cookies.get("idroles")==="1"?
                                    (
                                        <React.Fragment>
                                        <SubMenu title="Configuración" icon={<i className="icon ion-ios-gear-outline"></i>} >
                                            <MenuItem><NavLink to='/Roles'>Roles</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Usuarios'>Usuarios</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Departamentos'>Departamentos</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Municipios'>Municipios</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Factores'>Factores de riesgo</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Tipos_Monitoreo'>Tipos de Monitoreo</NavLink></MenuItem>
                                        </SubMenu>
                                        <SubMenu title="Parametrización" icon={<i className="icon ion-ios-filing-outline"></i>} >
                                            <MenuItem><NavLink to='/Diagnosticos'>Diagnósticos</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/IPS'>IPS</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/EPS'>EPS</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/ARL'>ARL</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Servicios'>Servicios</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Empresas'>Empresas</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Personas'>Personas</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Afiliaciones'>Afiliaciones</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Servicios_contratados'>Servicios Contratados</NavLink></MenuItem>
                                        </SubMenu>
                                        </React.Fragment>
                                ):(
                                        <React.Fragment></React.Fragment>
                                    )
                                }
                                {
                                    cookies.get("idroles")==="1" || cookies.get("idroles")==="3"?
                                    (
                                        <SubMenu title="Siniestros" icon={<i className="icon fa fa-ambulance"></i>} >
                                            <MenuItem><NavLink to='/Radicacion'>Radicación</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Consulta'>Consulta</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Evaluacion'>Evaluación</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Objetados'>Objetados</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Autorizaciones'>Autorizaciones</NavLink></MenuItem>
                                            <MenuItem><NavLink to='/Revisar_autorizaciones'>Revisar autorizaciones</NavLink></MenuItem>
                                        </SubMenu>
                                    ):
                                    cookies.get("idroles")==="2"?
                                    (
                                        <SubMenu title="Siniestros" icon={<i className="icon fa fa-ambulance"></i>} >
                                            <MenuItem><NavLink to='/Consulta'>Consulta</NavLink></MenuItem>
                                        </SubMenu>
                                    )
                                    :(
                                        <React.Fragment></React.Fragment>
                                    )
                                }
                                <SubMenu title="Reportes" icon={<i className="icon ion-pie-graph"></i>} >
                                    <MenuItem><NavLink to='/Reportes_1'>Reportes 1</NavLink></MenuItem>
                                    <MenuItem><NavLink to='/Reportes_1'>Reportes 2</NavLink></MenuItem>
                                </SubMenu>
                                <MenuItem icon={<i className="icon ion-close"></i>}>
                                    <NavLink to='/'>Salir</NavLink>
                                </MenuItem>
                            </Menu>
                        </ProSidebar>


                    </div>
                </div>
            </div>
        );
    }
}

export default Menulat;