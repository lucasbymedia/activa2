import React, { Component } from 'react'
import { ProSidebar, Menu } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';
import { NavLink } from 'react-router-dom';

class Menulat1 extends Component {

    render() {
        return (
            <div className="am-sideleft colorback">
                <ul className="nav am-sideleft-tab colorback">
                    <li className="nav-item">
                        <NavLink to="/Login" className="nav-link active">
                            <i className="fa fa-plug tx-24"></i>
                        </NavLink>
                    </li>
                </ul>
                <div className="tab-content">
                    <div id="mainMenu" className="tab-pane active">
                        <ProSidebar>
                            <Menu iconShape="square">
                            </Menu>
                        </ProSidebar>


                    </div>
                </div>
            </div>
        );
    }
}

export default Menulat1;