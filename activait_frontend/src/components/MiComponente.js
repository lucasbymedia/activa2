import React,{Component} from 'react';

class MiComponente extends Component {
    render(){
        var receta = {
            nombre: 'Pizza',
            ingredientes: ['Tomate', 'Queso', 'Jamon'],
            calorias: 400
        }
        return (
            <React.Fragment>
            <h1>{'Nombre ' + receta.nombre}</h1>
            <h2>{'Calorias' + receta.calorias}</h2>
            <ol>
            {
                receta.ingredientes.map((ingredientes,i)=>{
                    return (
                        <li key={i}>{ingredientes}</li>
                    );
                })
            }
            </ol>
            <hr/>
            </React.Fragment>
        );
    }
}
export default MiComponente;