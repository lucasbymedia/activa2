import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Personas extends Component {
 
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"personas/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"personas/Eliminar/" + id).then((res) => {
                swal("Persona eliminada", "Se ha eliminado la persona", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Personas</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Personas</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearpers">
                                <i className="icon ion-plus-circled"></i> Agregar Persona</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>Documento</th>
                                            <th>Nombre</th>
                                            <th>Nacimiento</th>
                                            <th>Género</th>
                                            <th>Editar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idpersonas}>
                            <td align="left">{ tab.per_numeroid }</td>
                            <td align="left">{ tab.per_primernombre } { tab.per_segundonombre} {tab.per_primerapellido} { tab.per_segundoapellido }</td>
                            <td align="left"><Moment format="DD MMM YYYY">{ tab.per_nacimiento }</Moment></td>
                            <td align="left">{ tab.per_genero }</td>
                            <td align="center"><NavLink to={"/Editapers/"+tab.idpersonas}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idpersonas)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Personas;