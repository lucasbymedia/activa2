import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Radicacion extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    state = {
        ruta: "./accidente_laboral", 
        cedula:""
    };
    componentDidMount() {
        <Redirect push to="./Roles"/>;
    }
    consulta2 = (e) =>{
        e.preventDefault();
        const documento = this.param1.current.value;
        this.ruta = this.param2.current.value;

        if(this.ruta==="Accidente laboral"){  
            this.ruta="./Accidente/" + documento;
        }
        else {             
            this.ruta="./Enfermedad/" + documento;
        }
        document.location.href = this.ruta;
    }
    render() {
        if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
        {
            return <Redirect to="./"/>;
        }
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Radicación de siniestros</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Seleccione el tipo de radicación que desea realizar</h6>
    <br/>
    <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
            <form onSubmit={this.consulta2}>
                <input className="form-control" type="text" ref={this.param1} required name="cedula" placeholder="Documento de identidad" />
                <br />
                <select className="form-control"  ref={this.param2} >
                    <option>Seleccione el tipo de radicación</option>
                    <option>Accidente laboral</option>
                    <option>Enfermedad laboral</option>
                </select><br />
                <input type="submit" className="btn btn-primary btn-block mg-b-2" value="Ingresar" /> 
            </form>
        </div>
        <div className="col-md-4"></div>
    </div>

                            <div className="clearfix"><br></br></div>


                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Radicacion;