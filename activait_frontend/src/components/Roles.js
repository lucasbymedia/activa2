import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

class Roles extends Component {
    state = {
        rols: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"roles/listar")
        .then(res => {
            const rols = res.data;
            this.setState({ rols });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"roles/Eliminar/" + id).then((res) => {
                swal("Rol Eliminado", "Se ha eliminado el rol", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Roles de acceso</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Roles de acceso</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearrol">
                                <i className="icon ion-plus-circled"></i> Agregar Rol</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th className="wd-50p" align="center">Rol nombre</th>
                                            <th className="wd-25p" align="center">Editar</th>
                                            <th className="wd-25p" align="center">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.rols.map((rol, i) => {
                    return (
                        <tr key={rol.idroles}>
                            <td align="left">{rol.rol_nombre}</td>
                            <td align="center"><NavLink to={"/Editarol/"+rol.idroles}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(rol.idroles)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Roles;