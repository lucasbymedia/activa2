import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Servicios extends Component {
    state = {
        tabe: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"servicios/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"servicios/Eliminar/" + id).then((res) => {
                swal("Servicio eliminado", "Se ha eliminado el servicio", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Servicios</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Servicios</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearserv">
                                <i className="icon ion-plus-circled"></i> Agregar servicio</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                        <th>Código</th><th>Descripción</th><th >CUPS</th><th >CUM</th>
                                        <th >Estado</th><th>Prioridad</th><th >PBS</th>
                                        <th>Editar</th><th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idservicios}>
                            <td align="left">{ tab.ser_codigo }</td><td align="left">{ tab.ser_descripcion }</td>
                            <td align="left">{ tab.ser_cups }</td><td align="left">{ tab.ser_cum }</td>
                            <td align="left">{ tab.ser_estado }</td><td align="left">{ tab.ser_prioridad }</td>
                            <td align="left">{ tab.ser_pbs }</td>

                            <td align="center"><NavLink to={"/Editaserv/"+tab.idservicios}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idservicios)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Servicios;