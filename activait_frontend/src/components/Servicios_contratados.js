import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Servicios_contratados extends Component {
    param1 = React.createRef(); 
    state = {
        tabe: [],
        ipss:[],
        status: null,
    };
    llena(){
        axios.get(global.url+"vista_servicios/listar")
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });
    }
    componentDidMount() {
        axios.get(global.url + "ips/listar").then((res) => {
            if (res.data) {
                const ipss = res.data;
                this.setState({ ipss });
                }
          });

        this.llena();
    }
    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"sercontratados/Eliminar/" + id).then((res) => {
                swal("Servicio contratado eliminado", "Se ha eliminado el servicio contratado", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }
    
    currencyFormat(num) {
        return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
     

    consulta2 = () =>
    {
        const idips = this.param1.current.value;
        axios.get(global.url+"vista_servicios/Consulta/"+ idips )
        .then(res => {
            const tabe = res.data;
            this.setState({ tabe });
            this.setState({ status: 'success'})
        });

    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }


        return (

            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Servicios contratados</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Servicios contratados</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearserc">
                                <i className="icon ion-plus-circled"></i> Agregar Servicio-IPS</NavLink><br />
                            <form name="forma">
                            <div className="row">
                                <div className="col-md-2">IPS</div>
                                <div className="col-md-4">
                                <select name="param1" ref={this.param1} className="form-control" onChange={this.consulta2}>
                                    <option value="0">Seleccione...</option>
                                    {
                                        this.state.ipss.map((con, i) => {
                                        return (
                                                <option key={con.idips} value={con.idips} >
                                                    {con.ips_nombre}</option>
                                            )
                                        })
                                    }
                                </select>
                                </div>
                            </div>
                            </form>
                            <div className="clearfix"><br></br></div>


                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>Municipio</th>
                                            <th>IPS</th>
                                            <th>Servicio</th>
                                            <th>Fecha inicial</th>
                                            <th>Fecha final</th>
                                            <th>Valor</th>
                                            <th>Estado</th>
                                            <th>Editar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idsercontratados}>
                            <td align="left">{ tab.mun_nombre }</td><td align="left">{ tab.ips_nombre }</td>
                            <td align="left">{ tab.ser_descripcion}</td>
                            <td align="left"><Moment format="DD MMM YYYY">{tab.sec_fechainicio}</Moment></td>
                            <td align="left"><Moment format="DD MMM YYYY">{tab.sec_fechafin}</Moment></td>
                            <td>{this.currencyFormat(tab.sec_valor)}</td><td>{ tab.sec_estado}</td>
                            <td align="center"><NavLink to={"/Editaserc/"+tab.idsercontratados}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(tab.idsercontratados)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Servicios_contratados;