import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import global from '../Global';
import { MDBDataTable } from 'mdbreact';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class Usuarios extends Component {
    
    state = {
        usua: [],
        status: null,
    };
    llena(){
        axios.get(global.url+"users/listar")
        .then(res => {
            const usua = res.data;
            this.setState({ usua });
            this.setState({ status: 'success'})
        });
    }

    DatatablePage = () => {
        const data = {
          columns: [
            {
              label: 'Name',
              field: 'name',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Position',
              field: 'position',
              sort: 'asc',
              width: 270
            },
            {
              label: 'Office',
              field: 'office',
              sort: 'asc',
              width: 200
            },
            {
              label: 'Age',
              field: 'age',
              sort: 'asc',
              width: 100
            },
            {
              label: 'Start date',
              field: 'date',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Salary',
              field: 'salary',
              sort: 'asc',
              width: 100
            }
          ],
          rows: [
            {
              name: 'Tiger Nixon',
              position: 'System Architect',
              office: 'Edinburgh',
              age: '61',
              date: '2011/04/25',
              salary: '$320'
            },
            {
              name: 'Michael Silva',
              position: 'Marketing Designer',
              office: 'London',
              age: '66',
              date: '2012/11/27',
              salary: '$198'
            },
            {
              name: 'Paul Byrd',
              position: 'Chief Financial Officer (CFO)',
              office: 'New York',
              age: '64',
              date: '2010/06/09',
              salary: '$725'
            },
            {
              name: 'Gloria Little',
              position: 'Systems Administrator',
              office: 'New York',
              age: '59',
              date: '2009/04/10',
              salary: '$237'
            },
            {
              name: 'Bradley Greer',
              position: 'Software Engineer',
              office: 'London',
              age: '41',
              date: '2012/10/13',
              salary: '$132'
            },
            {
              name: 'Dai Rios',
              position: 'Personnel Lead',
              office: 'Edinburgh',
              age: '35',
              date: '2012/09/26',
              salary: '$217'
            },
            {
              name: 'Jenette Caldwell',
              position: 'Development Lead',
              office: 'New York',
              age: '30',
              date: '2011/09/03',
              salary: '$345'
            },
            {
              name: 'Yuri Berry',
              position: 'Chief Marketing Officer (CMO)',
              office: 'New York',
              age: '40',
              date: '2009/06/25',
              salary: '$675'
            },
            {
              name: 'Caesar Vance',
              position: 'Pre-Sales Support',
              office: 'New York',
              age: '21',
              date: '2011/12/12',
              salary: '$106'
            },
            {
              name: 'Doris Wilder',
              position: 'Sales Assistant',
              office: 'Sidney',
              age: '23',
              date: '2010/09/20',
              salary: '$85'
            },
            {
              name: 'Angelica Ramos',
              position: 'Chief Executive Officer (CEO)',
              office: 'London',
              age: '47',
              date: '2009/10/09',
              salary: '$1'
            },
            {
              name: 'Gavin Joyce',
              position: 'Developer',
              office: 'Edinburgh',
              age: '42',
              date: '2010/12/22',
              salary: '$92'
            },
            {
              name: 'Serge Baldwin',
              position: 'Data Coordinator',
              office: 'Singapore',
              age: '64',
              date: '2012/04/09',
              salary: '$138'
            },
            {
              name: 'Zenaida Frank',
              position: 'Software Engineer',
              office: 'New York',
              age: '63',
              date: '2010/01/04',
              salary: '$125'
            },
            {
              name: 'Zorita Serrano',
              position: 'Software Engineer',
              office: 'San Francisco',
              age: '56',
              date: '2012/06/01',
              salary: '$115'
            },
            {
              name: 'Jennifer Acosta',
              position: 'Junior Javascript Developer',
              office: 'Edinburgh',
              age: '43',
              date: '2013/02/01',
              salary: '$75'
            },
            {
              name: 'Cara Stevens',
              position: 'Sales Assistant',
              office: 'New York',
              age: '46',
              date: '2011/12/06',
              salary: '$145'
            },
            {
              name: 'Hermione Butler',
              position: 'Regional Director',
              office: 'London',
              age: '47',
              date: '2011/03/21',
              salary: '$356'
            },
            {
              name: 'Lael Greer',
              position: 'Systems Administrator',
              office: 'London',
              age: '21',
              date: '2009/02/27',
              salary: '$103'
            },
            {
              name: 'Jonas Alexander',
              position: 'Developer',
              office: 'San Francisco',
              age: '30',
              date: '2010/07/14',
              salary: '$86'
            },
            {
              name: 'Shad Decker',
              position: 'Regional Director',
              office: 'Edinburgh',
              age: '51',
              date: '2008/11/13',
              salary: '$183'
            },
            {
              name: 'Michael Bruce',
              position: 'Javascript Developer',
              office: 'Singapore',
              age: '29',
              date: '2011/06/27',
              salary: '$183'
            },
            {
              name: 'Donna Snider',
              position: 'Customer Support',
              office: 'New York',
              age: '27',
              date: '2011/01/25',
              salary: '$112'
            }
          ]
        };
      
        return (
          <MDBDataTable
            striped
            bordered
            hover
            data={data}
          />
        );
      }

    componentDidMount() {
        this.llena();
    }

    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"users/Eliminar/" + id).then((res) => {
                swal("Usuario Eliminado", "Se ha eliminado el usuario", "success");
                this.setState({ status: 'deleted'})
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }
    render() {
        if(cookies.get("idroles")!=="1")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status === 'deleted')
        {
            this.llena();
        }
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Usuarios del sistema</h5>
                </div>
                <div className="am-mainpanel">
                    <div className="am-pagebody">
                        <div className="card pd-20 pd-sm-40">
                            <h6 className="card-body-title">Usuarios del sistema</h6>
                            <NavLink className="btn btn-primary btn-block mg-b-2 botones1" to="/Crearusuario">
                                <i className="icon ion-plus-circled"></i> Agregar Usuario</NavLink><br />

                            <div className="table-wrapper" id="tablas">
                                <table id="datatable1" className="table display responsive nowrap">
                                    <thead>
                                        <tr>
                                        <th class="wd-20p">Documento</th>
                                            <th className="wd-40p">Nombre</th>
                                            <th className="wd-40p">Email</th>
                                            <th className="wd-40p">Teléfono</th>
                                            <th className="wd-20p">Editar</th>
                                            <th className="wd-20p">Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
            {
                this.state.usua.map((usu, i) => {
                    return (
                        <tr key={usu.idusuarios}>
                            <td align="left">{usu.usu_documento}</td>
                            <td align="left">{usu.usu_nombre}</td>
                            <td align="left">{usu.usu_email}</td>
                            <td align="left">{usu.usu_telefono}</td>
                            <td align="center"><NavLink to={"/Editausuario/"+usu.idusuarios}><i className="icon ion-edit"></i></NavLink></td>
                            <td align="center"><button onClick={
                                () => { this.elimina(usu.idusuarios)}
                            } ><i className="icon ion-trash-b"></i></button></td>
                        </tr>
                    )
                })
            }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default Usuarios;