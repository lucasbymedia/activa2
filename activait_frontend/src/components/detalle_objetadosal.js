import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class detalle_objetadosal extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    idc = null;
    state = {
        sin:[],
        noa:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        console.log(this.idc);
        axios.get(global.url + "vista_siniestros/Muestra/" + this.idc ).then((res) => {
            const sin = res.data;
            console.log(res.data);
            this.setState({ sin });
          });
          this.gestiones();

    }

    gestiones (){
        axios.get(global.url + "noaceptados/Mostrarsin/" + this.idc ).then((res) => {
            const noa = res.data;
            this.setState({ noa });
        });
    }
    guardar = (e) =>{
      e.preventDefault();
      axios
        .post(global.url + "noaceptados/Crear/" , {
            "siniestros_idsiniestros": this.idc, 
            "noa_descripcion": this.param3.current.value, 
            "noa_tipoaccion": this.param1.current.value, 
            "noa_fecha": this.param2.current.value,
            'noa_fechacreacion': new Date(),
            'noa_usucreacion': cookies.get("idusuarios")
        })
        .then((res) => {
          console.log(res);
          swal("Seguimiento creado", "Se ha creado correctamente el seguimiento", "success");
          this.setState({ status: 'Ok'})
          this.gestiones();
        })
        .catch((error) => {
          swal("Seguimiento no creado", "Hubo un error al crear el seguimiento", "error");
          console.log(error);
          this.setState({ status: 'Mal'})
        });
    }
    currencyFormat(num) {
      return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    elimina  = (id) =>{ 
        swal({
            title: "Está seguro?",
            text: "Una vez lo elimine no podrá recuperarlo!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((willDelete) => {
            if (willDelete) {
              axios.delete(global.url+"noaceptados/Eliminar/" + id).then((res) => {
                swal("Seguimiento Eliminado", "Se ha eliminado el seguimiento", "success");
                this.setState({ status: 'deleted'})
                this.gestiones();
              });
            } else {
              swal("Eliminación cancelada");
            }
          });
    }

    render() {
      if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
      {
          return <Redirect to="./"/>;
      }
     
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                <h5 className="am-title">Reporte Objetados</h5>
                </div>

                <div className="am-mainpanel">
      <div className="am-pagebody">
        <div className="card pd-20 pd-sm-40">
        {
                this.state.sin.map((sin, i) => {
                    return (
        <React.Fragment key={i}>
          <div className="row">
              <div className="col-lg-3 izqq1 rojo">
                <b>Estado:</b> {sin.sin_estado}
              </div>            
              <div className="col-lg-3 izqq1">
                <b>EPS del Afiliado:</b> {sin.eps_nombre}
              </div>
              <div className="col-lg-3 izqq1">
                <b>Nombre Razón social: </b>{sin.emp_nombre}
              </div>
              <div className="col-lg-3 izqq1">
                <b>Identificación: </b>{sin.emp_nit}<br></br>
              </div>
          </div>    
          <div className="row">
              <div className="col-lg-3 izqq1">
                <b>Primer Apellido: </b>{sin.per_primerapellido}
              </div>
              <div className="col-lg-3 izqq1">
                <b>Segundo Apellido: </b>{sin.per_segundoapellido}
              </div>
              <div className="col-lg-3 izqq1">
                <b>Primer Nombre: </b>{sin.per_primernombre}
              </div>
              <div className="col-lg-3 izqq1">
                <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
              </div>
          </div>
          <div className="row">
              <div className="col-lg-3 izqq1">
                <b>Documento: </b>{sin.per_numeroid}<br></br>
              </div>
              <div className="col-lg-3 izqq1">
                <b>Fecha de nacimiento: </b><Moment format="DD MMM YYYY">{sin.per_nacimiento }</Moment><br></br>
              </div>
              <div className="col-lg-3 izqq1">
                <b>Género: </b>{sin.per_genero }<br></br>
              </div>
              <div className="col-lg-3 izqq1">
                <b>Municipio: </b>{sin.mun_nombre }<br></br>
              </div>
          </div>    
          <div className="row">
            <div className="col-lg-3 izqq1">
              <b>Fecha accidente: </b><Moment format="DD MMM">{sin.sin_fecha }</Moment><br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Causa del accidente: </b>{sin.sin_causal }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>¿Causó la muerte al trabajador? </b>{sin.sin_muerte }<br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-3 izqq1">
              <b>Municipio: </b>{sin.mun_nombre }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Lugar de ocurrencia: </b>{sin.sin_lugar }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Sitio del accidente: </b>{sin.sin_sitio }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Tipo de lesión: </b>{sin.sin_tipolesion }<br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 izqq1">
              <b>Parte del cuerpo afectada: </b>{sin.sin_parteafectada }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Agentes del accidente: </b>{sin.sin_agente }<br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Mecanismo o forma: </b>{sin.sin_mecanismo }<br></br><br></br>
            </div>
          </div>    
          </React.Fragment>    
                    )
                })
            }
          <div className="row">
            <div className="col-lg-12">
                <h6 className='titul2'>Agregar Seguimiento</h6><br></br>
            </div>
          </div>    
          <React.Fragment>
          <form name="forma" onSubmit={this.guardar}>
          <div className="row">
            <div className="col-lg-3 izqq1">
              <b>Tipo de acción: </b><br></br>
            </div>
            <div className="col-lg-3">  
              <input type="text"  className="form-control" ref={this.param1}  name="tacc" placeholder="Tipo acción"/><br></br>
            </div>
            <div className="col-lg-3 izqq1">
              <b>Fecha de revisión: </b><br></br>
            </div>
            <div className="col-lg-3">
              <input type="date" className="form-control" ref={this.param2} name="fnac" placeholder="Fecha de nacimiento"/><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 izqq1">DESCRIPCION DE LA ACTIVIDAD LABORAL DESEMPEÑADA (NEXO LABORAL):
            <textarea className="form-control" ref={this.param3} name="desc" required></textarea><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6"></div>
            <div className="col-lg-6" align="right">
                <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" /><br></br>
              <br></br>
            </div>
          </div>
          </form>
          </React.Fragment>
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className='titul2'>Seguimientos</h6><br></br>
            </div>
          </div> 
          <div className="row">
            <div className="col-lg-12" align="center">
                <div className="table-wrapper" id="tablas">
                    <table id="datatable1" className="table display responsive nowrap">
                    <thead>
                        <tr>
                        <th>Tipo de acción</th><th>Fecha</th><th>Descripción</th><th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    { 
                        this.state.noa.map((na, i) => {
                        return (
                                <tr key={i}>
                                    <td>{ na.noa_tipoaccion }</td>
                                    <td><Moment format="DD MMM YYYY">{ na.noa_fecha }</Moment></td>
                                    <td>{ na.noa_descripcion }</td>
                                    <td align="center"><button onClick={
                                        () => { this.elimina(na.idnoaceptados)}
                                    } ><i className="icon ion-trash-b"></i></button></td>
                                </tr>
                           )
                        })
                    }
                    </tbody>
                    </table>
                </div>
            </div>
          </div>   
        </div>
      </div>
    </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default detalle_objetadosal;