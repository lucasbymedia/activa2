import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class detalle_objetadosal extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    idc = null;
    idafiliados = null;
    state = {
        sin:[],
        rsv:[],
        serv:[],
        dept:[],
        ciud:[],
        ips1:[],
        ips2:[],
        diag:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;

        axios.get(global.url + "vista_siniestros/Muestra/" + this.idc ).then((res) => {
            const sin = res.data;
            this.setState({ sin });
            console.log( this.sin);
            this.setState({idafiliados: res.data.idafiliados })
          });

          axios.get(global.url + "reservas/Mostrarsin/" + this.idc).then((res) => {
            const rsv = res.data;
            this.setState({rsv});
        });

        axios.get(global.url + "servicios/listar").then((res) => {
            const serv = res.data;
            this.setState({serv});
        });

        axios.get(global.url + "departamentos/listar").then((res) => {
            const dept = res.data;
            this.setState({dept});
        });

        axios.get(global.url + "ips/listar").then((res) => {
            const ips1 = res.data;
            const ips2 = res.data;
            this.setState({ips1});
            this.setState({ips2});
        });

        axios.get(global.url + "diagnosticos/listar").then((res) => {
            const diag = res.data;
            this.setState({diag});
        });
    }

    cambioips = () =>
    {
        const idciu = this.param3.current.value;
        const idser = this.param1.current.value;
        console.log(idciu, idser);
        axios.get(global.url+"consultaips/Consulta/"+ idciu + "/" + idser)
        .then(res => {
            const ips1 = res.data;
            const ips2 = res.data;
            this.setState({ips1});
            this.setState({ips2});
        });
    }

    cambiodep = () =>
    {
        const iddep = this.param2.current.value;
        console.log(iddep);
        axios.get(global.url+"ciudades/Consulta_dep/"+ iddep)
        .then(res => {
            const ciud = res.data;
            this.setState({ ciud });
        });
    }

    guardar = (e) =>{
      e.preventDefault();
      axios
        .post(global.url + "autorizaciones/Crear/" , {
            "siniestros_idsiniestos": this.idc, 
            "servicios_idservicios": this.param1.current.value, 
            "aut_ipssolicitante": this.param6.current.value, 
            "aut_ipsdireccionada": this.param8.current.value, 
            "aut_cuotamoderadora": 0, 
            "aut_copago": 0, 
            "aut_valorizacion": 0, 
            "aut_codigoservicio": 0, 
            "aut_valorestimado": 0, 
            "diagnosticos_iddiagnosticos": this.param7.current.value, 
            "aut_estado": "",
            'aut_fechacreacion': new Date(),
            'aut_usucreacion': cookies.get("idusuarios")

        })
        .then((res) => {
          console.log(res);
          swal("Autorización creada", "Se ha creado correctamente la autorización", "success");
          this.setState({ status: 'Ok'})
        })
        .catch((error) => {
          swal("Autorización no creada", "Hubo un error al crear la autorización", "error");
          console.log(error);
          this.setState({ status: 'Mal'})

        });
    }
    currencyFormat(num) {
      return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    render() {
        if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
        {
            return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/revisar_autorizaciones"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                <h5 className="am-title">Reporte Objetados</h5>
                </div>

                <div className="am-mainpanel">
      <div className="am-pagebody">
        <div className="card pd-20 pd-sm-40">
        {
                this.state.sin.map((sin, i) => {
                    return (
        <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-3 verde izqq1">
                            <b>Estado:</b> {sin.sin_estado}
                        </div>            
                        <div className="col-lg-3 izqq1">
                            <b>EPS del Afiliado:</b> {sin.eps_nombre}
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Nombre Razón social: </b>{sin.emp_nombre}
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Identificación: </b>{sin.emp_nit}<br></br>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-3 izqq1">
                            <b>Primer Apellido: </b>{sin.per_primerapellido}
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Segundo Apellido: </b>{sin.per_segundoapellido}
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Primer Nombre: </b>{sin.per_primernombre}
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
                        </div>
                    </div>    
                    <div className="row">
                        <div className="col-lg-3 izqq1">
                            <b>Documento: </b>{sin.per_numeroid}<br></br>
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Fecha de nacimiento: </b><Moment format="DD MMM YYYY">{sin.per_nacimiento }</Moment><br></br>
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Género: </b>{sin.per_genero }}<br></br>
                        </div>
                        <div className="col-lg-3 izqq1">
                            <b>Municipio: </b>{sin.mun_nombre }<br></br><br></br>
                        </div>
                    </div>    
          </React.Fragment>    
                    )
                })
            }
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2">Reservas</h6><br></br>
            </div>
          </div> 
          <div className="row">
            <div className="col-lg-12" align="center">
                <div className="table-wrapper" id="tablas">
                    <table id="datatable1" className="table display responsive nowrap">
                    <thead>
                        <tr>
                        <th>Código</th><th>Valor</th><th>Fecha</th><th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.rsv.map((re, i) => {
                            return (
                                <tr key={i}>
                                    <td>{ re.res_codigo }</td><td>{this.currencyFormat(re.res_valor)}</td>
                                    <td><Moment format="YYYY MMM DD">{ re.res_fecha }</Moment></td>
                                    <td>{ re.res_estado }</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                    </table>
                </div>
            </div>
          </div>   
          <React.Fragment>
          <form name="forma" onSubmit={this.guardar}>
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2">Solicitud de autorización</h6><br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-3 izqq">
              <b>Servicio solicitado: </b><br></br>
            </div>
            <div className="col-lg-9 izqq">  
                <select className="form-control" name="ser" onChange={this.cambioips} ref={this.param1} required>
                    <option value="" selected>Servicio solicitado</option>
                    {
                        this.state.serv.map((ser, i) => {
                        return (
                            <option key={ser.idservicios} value={ser.idservicios}>
                            {ser.ser_descripcion} {ser.ser_codigo}</option> 
                            )
                        })
                    }
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 izqq">
              <b>Departamento: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
                <select className="form-control" onChange={this.cambiodep} ref={this.param2} name="dep" required>
                    <option value="" selected>Departamento</option>
                    {
                        this.state.dept.map((dep, i) => {
                        return (
                            <option key={dep.iddepartamentos} value={dep.iddepartamentos}>
                            {dep.dep_nombre} </option> 
                            )
                        })
                    }
                </select><br></br>
            </div>
            <div className="col-lg-3 izqq">
              <b>Municipio: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
                <select className="form-control" ref={this.param3} onChange={this.cambioips} name="mun" required>
                    <option value="" selected>Municipio</option>
                    {
                        this.state.ciud.map((con, i) => {
                        return (
                                <option key={con.idciudades} value={con.idciudades}>{con.mun_nombre}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 izqq">
              <b>Dirección: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
              <input type="text" className="form-control" ref={this.param4} name="dir" placeholder="Dirección"/><br></br>
            </div>
            <div className="col-lg-3 izqq">
              <b>Teléfono: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
              <input type="text" className="form-control" ref={this.param5} name="tel" placeholder="Teléfono"/><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 izqq">
              <b>IPS solicitante: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
                <select className="form-control" ref={this.param6} name="ips1" required>
                    <option value="" selected>IPS</option>
                    {
                        this.state.ips1.map((ips, i) => {
                        return (
                                <option key={ips.idips} value={ips.idips}>{ips.ips_nombre}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
            <div className="col-lg-3 izqq">
              <b>Diagnóstico: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
                <select className="form-control" ref={this.param7} name="dia" required>
                    <option value="" selected>Diagnósticos</option>
                    {
                        this.state.diag.map((dia, i) => {
                        return (
                                <option key={dia.iddiagnosticos} value={dia.iddiagnosticos}>
                                {dia.dia_descripcion}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 izqq">
              <b>IPS direccionamiento: </b><br></br>
            </div>
            <div className="col-lg-3 izqq">
                <select className="form-control" ref={this.param8} name="ips2" required>
                    <option value="" selected>IPS</option>
                    {
                        this.state.ips2.map((ips, i) => {
                        return (
                                <option key={ips.idips} value={ips.idips}>{ips.ips_nombre}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
            <div className="col-lg-6" align="right">
              <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" /><br></br>
            </div>
          </div>

          </form>
          </React.Fragment>
        </div>
      </div>
    </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default detalle_objetadosal;