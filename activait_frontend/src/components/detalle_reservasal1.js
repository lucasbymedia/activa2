import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import swal from 'sweetalert';
import Moment from 'react-moment';
import { months } from 'moment';

class Enfermedad extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    idc = null;

    state = {
        sini:[],
        rsv:[],
        serv:[],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        this.getdatos(this.idc);
        this.getmostrar(this.idc);
        this.getservicios();
    }
    getdatos = (id) => {
        axios.get(global.url + "vista_siniestros/mostrar/" + id ).then((res) => {
            const sini = res.data;
            this.setState(sini);
        });
    }

    getmostrar = (id) => {
        axios.get(global.url + "reservas/Mostrarsin/" + id).then((res) => {
            const rsv = res.data;
            console.log(rsv.data);
            this.setState(rsv);
        });
    }
    getservicios(){
        axios.get(global.url + "servicios/listar").then((res) => {
            const serv = res.data;
            this.setState(serv);
        });
    }
    getdpt(){
        axios.get(global.url + "departamentos/listar").then((res) => {
            if (res.data) {
                this.gdep = res.data;
            }
        });
    }


    guardar = (e) =>{
        e.preventDefault();
        axios
        .post(global.url + "autorizaciones/Crear/" , this.aut)
        .then((res) => {
          console.log(res);
          swal("Autorización creada", "Se ha creado correctamente la autorización", "success");
//          this.$router.push("../autorizaciones");
        })
        .catch((error) => {
          swal("Autorización no creada", "Hubo un error al crear la autorización", "error");
          console.log(error);
//          this.$router.push("../autorizaciones");
        });


    }
    render() {
        if(this.state.status==="Ok"){
            return <Redirect to="/Radicacion"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                    <h5 className="am-title">Detalle reservas</h5>
                </div>

                <div className="am-mainpanel">
      <div className="am-pagebody">
      <form name="forma" onSubmit={this.guardar}>
        <div className="card pd-20 pd-sm-40">
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2" >Información del Afiliado</h6><br></br>
            </div>
          </div> 

          {
            this.state.sini.map((sin, i) => {
                return (
                    <React.Fragment key={i}>
                    <div className="row">
                        <div className="col-lg-3 verde">
                            <b>Estado:</b> {sin.sin_estado}
                        </div>            
                        <div className="col-lg-3">
                            <b>EPS del Afiliado:</b> {sin.eps_nombre}
                        </div>
                        <div className="col-lg-3">
                            <b>Nombre Razón social: </b>{sin.emp_nombre}
                        </div>
                        <div className="col-lg-3">
                            <b>Identificación: </b>{sin.emp_nit}<br></br>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-3">
                            <b>Primer Apellido: </b>{sin.per_primerapellido}
                        </div>
                        <div className="col-lg-3">
                            <b>Segundo Apellido: </b>{sin.per_segundoapellido}
                        </div>
                        <div className="col-lg-3">
                            <b>Primer Nombre: </b>{sin.per_primernombre}
                        </div>
                        <div className="col-lg-3">
                            <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
                        </div>
                    </div>    
                    <div className="row">
                        <div className="col-lg-3">
                            <b>Documento: </b>{sin.per_numeroid}<br></br>
                        </div>
                        <div className="col-lg-3">
                            <b>Fecha de nacimiento: </b><Moment format="DD MMM YYYY">{sin.per_nacimiento }</Moment><br></br>
                        </div>
                        <div className="col-lg-3">
                            <b>Género: </b>{sin.per_genero }}<br></br>
                        </div>
                        <div className="col-lg-3">
                            <b>Municipio: </b>{sin.mun_nombre }<br></br>
                        </div>
                    </div>    
                    </React.Fragment>      
                )
            })
          }
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2">Reservas</h6><br></br>
            </div>
          </div> 
          <div className="row">
            <div className="col-lg-12" align="center">
                <div className="table-wrapper" id="tablas">
                    <table id="datatable1" className="table display responsive nowrap">
                    <thead>
                        <tr>
                        <th>Código</th><th>Valor</th><th>Fecha</th><th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.rsv.map((re, i) => {
                            return (
                                    <tr key={i}>
                                        <td>{ re.res_codigo }</td><td>{ re.res_valor }</td>
                                        <td><Moment format="YYYY MMM DD">{ re.res_fecha }</Moment></td>
                                        <td>{ re.res_estado }</td>
                                    </tr>
                            )
                        })
                    }
                    </tbody>
                    </table>
                </div>
            </div>
          </div>   
          <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2">Solicitud de autorización</h6><br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-3">
              <b>Servicio solicitado: </b><br></br>
            </div>
            <div className="col-lg-9">  
                <select className="form-control" name="ser" required>
                    <option value="" selected>Servicio solicitado</option>
                    {
                        this.state.serv.map((ser, i) => {
                        return (
                                <option key={ser.idservicios} value={ser.idservicios}>{ser.ser_nombre}</option> 
                            )
                        })
                    }
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <b>Departamento: </b><br></br>
            </div>
            <div className="col-lg-3">
                <select className="form-control"  name="dep" required>
                    <option value="" selected>Departamento</option>
                </select><br></br>
            </div>
            <div className="col-lg-3">
              <b>Municipio: </b><br></br>
            </div>
            <div className="col-lg-3">
                <select className="form-control" name="mun" required>
                    <option value="" selected>Municipio</option>
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <b>Dirección: </b><br></br>
            </div>
            <div className="col-lg-3">
              <input type="text" className="form-control" name="dir" placeholder="Dirección"/><br></br>
            </div>
            <div className="col-lg-3">
              <b>Teléfono: </b><br></br>
            </div>
            <div className="col-lg-3">
              <input type="text" className="form-control" name="tel" placeholder="Teléfono"/><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <b>IPS solicitante: </b><br></br>
            </div>
            <div className="col-lg-3">
                <select className="form-control" name="ips1" required>
                    <option value="" selected>IPS</option>
                </select><br></br>
            </div>
            <div className="col-lg-3">
              <b>Diagnóstico: </b><br></br>
            </div>
            <div className="col-lg-3">
                <select className="form-control" name="dia" required>
                    <option value="" selected>Diagnósticos</option>
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <b>IPS direccionamiento: </b><br></br>
            </div>
            <div className="col-lg-3">
                <select className="form-control" name="ips2" required>
                    <option value="" selected>IPS</option>
                </select><br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6"></div>
            <div className="col-lg-6" align="right"><br></br>
              <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" /><br></br>
            </div>
          </div>


        </div>
      </form>
 
      </div>
    </div>


                <Footer></Footer>
            </div>
        );
    }
}
export default Enfermedad;