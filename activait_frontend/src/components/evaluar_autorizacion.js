import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class evaluar_autorizacion extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    param4 = React.createRef(); 
    param5 = React.createRef(); 
    param6 = React.createRef(); 
    param7 = React.createRef(); 
    param8 = React.createRef(); 
    param9 = React.createRef(); 
    idc = null;
    state = {
        sini:[],
        noa:[],
        ips1:[],
        ips2:[],
        status: null,
        estados: ['APROBADO', 'OBJETADO']
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        axios.get(global.url + "vista_autorizaciones/mostrar/" + this.idc ).then((res) => {
            const sini = res.data;
            this.setState({ sini });
          });
    }

    cambioips = () =>
    {
        const idciu = this.param4.current.value;
        const idser = this.param5.current.value;
        axios.get(global.url+"consultaips/Consulta/"+ idciu + "/" + idser)
        .then(res => {
            const ips1 = res.data;
            const ips2 = res.data;
            this.setState({ips1});
            this.setState({ips2});
        });
    }
 
    crea_reserva = (e) => 
    {
        e.preventDefault();
        var val = this.param9.current.value*0.2;
        val = val.toFixed().toString();
        axios.post(global.url+"reservas/crear", {
            'siniestros_idsiniestos': this.param8.current.value,
            'res_codigo': "CRES",
            'res_valor': val,
            'res_acumulado': val,
            'res_fecha': new Date(),
            'res_autorizacion': '0',
            'res_estado': 'APROBADA',
            'res_fechacreacion': new Date(),
            'res_usucreacion': cookies.get("idusuarios")

        }).then(res =>{
            swal(
                'Reserva Adicionada', 'Se ha adicionado la reserva en un 20%', 'success'
            )
            this.setState({ status: 'Add'})
        })
        .catch((error) => {
            swal("Error al agregar la Reserva", "Hubo un error al agregar la reserva", "error");
            console.log(error);
            this.setState({ status: 'Mal'})
          });
  
    }

    guardar = (e) =>{
      e.preventDefault();
      axios
        .put(global.url + "autorizaciones/Actualizar/" + this.idc , {
            "idautorizaciones": this.idc,
            'siniestros_idsiniestos': this.param8.current.value,
            'servicios_idservicios': this.param6.current.value,
            'diagnosticos_iddiagnosticos': this.param7.current.value,
            'aut_ipssolicitante': this.param2.current.value,
            'aut_ipsdireccionada': this.param3.current.value,
            'aut_cuotamoderadora': '0',
            'aut_copago': '0',
            'aut_valorizacion': '0',
            'aut_codigoservicio': '',
            'aut_valorestimado': '0',
            'aut_estado': this.param1.current.value,
            'aut_fechamodificacion': new Date(),
            'aut_usumodificacion': cookies.get("idusuarios")

        })
        .then((res) => {
          console.log(res);
          swal("Autorización actualizada", "Se ha actualizado la autorización", "success");
          this.setState({ status: 'Ok'})
        })
        .catch((error) => {
          swal("Autorización no actualizada", "Hubo un error al actualizar la autorización", "error");
          console.log(error);
          this.setState({ status: 'Mal'})
        });
    }
    currencyFormat(num) {
      return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    render() {
      if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
      {
          return <Redirect to="./"/>;
      }
      if(this.state.status==="Ok"){
          return <Redirect to="/Revisar_autorizaciones"/>;
      }
      else if(this.state.status==="Add"){
          return <Redirect to="/Revisar_autorizaciones"/>;
      }
      else if(this.state.status==="Mal"){}
      return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
                <div className="am-pagetitle">
                <h5 className="am-title">Evaluar autorización</h5>
                </div>

                <div className="am-mainpanel">
      <div className="am-pagebody">
        <div className="card pd-20 pd-sm-40">
        <div className="row">
            <div className="col-lg-12" align="center">
                <h6 className="titul2">Información de la autorización</h6><br></br>
            </div>
        </div> 
        {
                this.state.sini.map((sin, i) => {
                    return (
        <React.Fragment key={i}>
            <div className="row">
              <div className="col-lg-3 izqq">
                  <input type="hidden" defaultValue={sin.idciudadeps} ref={this.param4}/>
                  <input type="hidden" defaultValue={sin.idservicios} ref={this.param5}/>
                  <input type="hidden" defaultValue={sin.idservicios} ref={this.param6}/>
                  <input type="hidden" defaultValue={sin.iddiagnosticos} ref={this.param7}/>
                  <input type="hidden" defaultValue={sin.idsieniestros} ref={this.param8}/>
                  <input type="hidden" defaultValue={sin.valor_reservas} ref={this.param9}/>
                <b>Primer Apellido: </b>{sin.per_primerapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Apellido: </b>{sin.per_segundoapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Primer Nombre: </b>{sin.per_primernombre}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
              </div>
          </div>   
          <div className="row">
              <div className="col-lg-12 izqq">
                <b>Servicio solicitado: </b><br></br>{sin.ser_descripcion}<br></br><br></br>
              </div>
          </div>
          <div className="row">
              <div className="col-lg-3 izqq">
                <b>IPS direccionada: </b><Moment format="MMM DD">{sin.ips_nombre}</Moment><br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Diagnósticos: </b>{sin.dia_descripcion }<br></br>
              </div>
              {
                  sin.aut_estado==='APROBADO' ? ( 
                    <div className="col-lg-3 izqq verde">
                    <b>Estado: </b>{sin.aut_estado }<br></br><br></br>
                    </div>
                  )
                  : sin.aut_estado==='OBJETADO' ? (
                    <div className="col-lg-3 izqq rojo">
                        <b>Estado: </b>{sin.aut_estado }<br></br><br></br>
                    </div>
                  )
                  : (
                    <div className="col-lg-3 izqq amarillo">
                      <b>Estado: </b> PENDIENTE<br></br><br></br>
                    </div>
                  )
              }          
          </div>    
          <div className="row">
            <div className="col-lg-12">
                <h6 className='titul2'>Valores</h6><br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-6 izqq">
            <form onSubmit={this.crea_reserva} name="form3">
              <input type="submit" className="btn btn-info pd-x-20" value="Agregar reserva"/>
            </form>
              <br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-3 izqq">Valor reservas: </div>
            <div className="col-lg-3 izqq">{ this.currencyFormat(sin.valor_reservas)  }</div>
            <div className="col-lg-3 izqq">Valor autorizaciones: </div>
            <div className="col-lg-3 izqq">{ this.currencyFormat(sin.valor_servicios) }<br></br><br></br></div>
          </div>

          </React.Fragment>    
                    )
                })
            }

          <div className="row">
            <div className="col-lg-12">
                <h6 className='titul2'>Redireccionar</h6><br></br>
            </div>
          </div>    
          <React.Fragment>
          <form name="forma" onSubmit={this.guardar}>
          <div class="row">
            <div class="col-lg-3 izqq">
              <b>Estado: </b><br></br>
            </div>
            <div class="col-lg-3 izqq">  
                <select class="form-control" ref={this.param1} onChange={this.cambioips} name="est" required>
                    <option value="" selected>Estado</option>
                    {
                        this.state.estados.map((est, i) => {
                        return (
                                <option key={i} value={est}>{est}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
            <div class="col-lg-3 izqq">
              <b>IPS solicitante: </b><br></br>
            </div>
            <div class="col-lg-3 izqq">
                <select class="form-control" name="ips1" ref={this.param2}  required>
                    <option value="" selected>IPS</option>
                    {
                        this.state.ips1.map((ips, i) => {
                        return (
                                <option key={ips.idips} value={ips.idips}>{ips.ips_nombre}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 izqq">
              <b>IPS direccionamiento: </b><br></br>
            </div>
            <div class="col-lg-3 izqq">
                <select class="form-control" name="ips2" ref={this.param3}  required>
                    <option value="" selected>IPS</option>
                    {
                        this.state.ips2.map((ips, i) => {
                        return (
                                <option key={ips.idips} value={ips.idips}>{ips.ips_nombre}</option>
                            )
                        })
                    }
                </select><br></br>
            </div>
            <div className="col-lg-6 " align="right">
                <input type="submit" className="btn btn-info pd-x-20" value="Guardar Cambios" /><br></br>
              <br></br>
            </div>
          </div>
          </form>
          </React.Fragment>
        </div>
      </div>
    </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default evaluar_autorizacion;