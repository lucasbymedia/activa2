import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class furat extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    idc = null;
    state = {
        sin:[],
        tes:[],
        estados: ['RESERVADO', 'ACEPTADO', 'OBJETADO'],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        console.log(this.idc);
        axios.get(global.url + "vista_siniestros/Muestra/" + this.idc ).then((res) => {
            const sin = res.data;
            console.log(res.data);
            this.setState({ sin });
          });

        axios.get(global.url + "testigos/Mostrarsin/" + this.idc ).then((res) => {
            if (res.data) {
                const tes = res.data;
                this.setState({ tes });
            }
        });
    }

    currencyFormat(num) {
      return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    render() {
        if(!cookies.get("idroles"))
        {
          return <Redirect to="./"/>;
        }
        if(this.state.status==="Ok"){
            return <Redirect to="/Radicacion"/>;
        }
        else if(this.state.status==="Mal"){}
        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
    <div className="am-pagetitle">
      <h5 className="am-title">Accidente Laboral</h5>
    </div>



    <div className="am-mainpanel">
      <div className="am-pagebody">
      {
                this.state.sin.map((sin, i) => {
                    return (
        <div className="card pd-20 pd-sm-40">
            <div className="row">
              <div className="col-lg-12">
                <h2 className="card-body-title">INFORME DE ACCIDENTE DE TRABAJO DEL EMPLEADOR O CONTRATANTE</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg izqq"><b>Nombre:</b> {sin.per_primernombre} {sin.per_segundonombre} 
              {sin.per_primerapellido} {sin.per_segundoapellido}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>Documento:</b> {sin.per_numeroid}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>Empresa:</b> {sin.emp_nombre}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>NIT:</b> {sin.emp_nit}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                  <h2 className="card-body-title">Afiliaciones</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 izqq">
                <b>EPS del Afiliado:</b> {sin.eps_nombre}
              </div>
              <div className="col-lg-4 izqq">
                <b>ARL del Afiliado Código:</b> {sin.arl_nombre}
              </div>
              <div className="col-lg-4 izqq">
                <b>AFP del Afiliado Código:</b> {sin.afp_nombre}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12"><br></br>
                  <h2 className="card-body-title">Identificación General del empleador, contratante o cooperativa</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Tipo de vinculador laboral: </b>{sin.afi_tipovinculacion}<br></br><br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>SEDE PRINCIPAL</h6>
              </div>
            </div>   
            <div className="row">
              <div className="col-lg-8 izqq">
                <b>Nombre de la actividad económica: </b>{sin.emp_actividad} 
              </div>
              <div className="col-lg-4 izqq">
                <b>Código: </b>{sin.emp_codactividad}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Nombre Razón social: </b>{sin.emp_nombre}
              </div>
              <div className="col-lg-3 izqq">
                <b>Tipo de documento: </b>NIT
              </div>
              <div className="col-lg-4 izqq">
                <b>Número Identificación: </b>{sin.emp_nit}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.emp_direccion}
              </div>
              <div className="col-lg-6 izqq">
                <b>Zona: </b>Urbana<br></br><br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>CENTRO DE TRABAJO DONDE LABORA EL TRABAJADOR</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-9 izqq">
                ¿SON LOS DATOS DEL CENTRO DE TRABAJO LOS MISMOS DE LA SEDE PRINCIPAL? <br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-8 izqq">
                <b>Nombre de la actividad económica del Centro de Trabajo: </b>{sin.emp_actividad}
              </div>
              <div className="col-lg-4 izqq">
                <b>Código de actividad: </b>{sin.emp_codactividad}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.emp_direccion}
              </div>
              <div className="col-lg-2 izqq">
                <b>Zona: </b>Urbana<br></br><br></br>
              </div>
            </div>   
              <h2 className="card-body-title"> II. Información de la persona accidentada</h2>
            <div className="row">
              <div className="col-lg-6 izqq">
                <h6>Tipo de vinculación la laboral</h6>
              </div>
              <div className="col-lg-4 izqq">{sin.per_tipovinculacion}<br></br>
              </div>    
            </div>
            <div className="row">
              <div className="col-lg-3 izqq">
                <b>Primer Apellido: </b>{sin.per_primerapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Apellido: </b>{sin.per_segundoapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Primer Nombre: </b>{sin.per_primernombre}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-3 izqq">
                <b>Tipo identificación: </b>CC<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Documento: </b>{sin.per_numeroid}<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Nacimiento: </b><Moment format="DD MMM YYYY">{sin.per_nacimiento }</Moment><br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Género: </b>{sin.per_genero }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.per_direccion }<br></br>
            </div>
            <div className="col-lg-3 izqq">
                <b>Teléfono: </b>{sin.per_telefono }<br></br>
            </div>
            <div className="col-lg-3 izqq">
                <b>Fax: </b><br></br><br></br>
            </div>
          </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                  <b>Departamento: </b>{sin.dep_nombre }<br></br>
              </div>
              <div className="col-lg-5 izqq">
                  <b>Municipio: </b>{sin.mun_nombre }<br></br>
              </div>
              <div className="col-lg-2 izqq">
                  <b>Zona: </b>Urbana<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                  <b>Cargo que desempeña: </b>{sin.per_cargo }<br></br>
              </div>
              <div className="col-lg-5 izqq">
                  <b>Ocupación Habitual: </b><br></br>
              </div>
              <div className="col-lg-2 izqq">
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Tiempo de ocupación al momento del accidente: </b>{sin.sin_tiempolaborprev }<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Fecha de ingreso: </b><Moment format="DD MMM YYYY">{sin.afi_fechaafiliacion }</Moment><br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Salario u horonarios: </b>{this.currencyFormat(sin.afi_ibc)}<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Jornada: </b>Diurna<br></br><br></br>
              </div>
            </div>   
              <h2 className="card-body-title">III. Información del accidente</h2>                 
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Fecha y hora del accidente: </b><Moment format="DD MMM YYYY">{sin.sin_fecha }</Moment><br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Jornada de trabajo habitual: </b>{sin.sin_jornada }<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>¿Estaba realizando su labor habitual? </b>{sin.sin_laboral }<br></br>
              </div>    
            </div>
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Cúal? </b>{sin.sin_otra }<br></br>
              </div>
              <div className="col-lg-2 izqq">
                <b>Código: </b>{sin.sin_codigo }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-8 izqq">
                <b>Tiempo laborado previo al accidente: </b>{sin.sin_tiempolaborprev }<br></br><br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>Causa del accidente</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Causa del accidente: </b>{sin.sin_causal }<br></br>
              </div>
              <div className="col-lg-6 izqq">
                <b>¿Causó la muerte al trabajador? </b>{sin.sin_muerte }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Departamento: </b>{sin.dep_nombre }<br></br>
              </div>
              <div className="col-lg-5 izqq">
                <b>Municipio: </b>{sin.mun_nombre }<br></br>
              </div>
              <div className="col-lg-2 izqq">
                <b>Municipio: </b>{sin.sin_zona }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                <b>Lugar de ocurrencia: </b>{sin.sin_lugar }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>INDIQUE CUÁL SITIO</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                {sin.sin_sitio }<br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>TIPO DE LESIÓN</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                {sin.sin_tipolesion }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>PARTE DEL CUERPO AFECTADO</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                {sin.sin_parteafectada }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>AGENTES DEL ACCIDENTE</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                {sin.sin_agente }<br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>MECANISMO O FORMA</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-12 izqq">
                {sin.sin_mecanismo }<br></br>
              </div>
            </div>
              <h2 className="card-body-title">IV. Descripción del accidente</h2>
            <div className="row">
              <div className="col-lg-12 izqq">
                <b>DESCRIBA DETALLADAMENTE EL ACCIDENTE. QUÉ LO ORIGINO O CAUSÓ: </b>{sin.sin_descripcion }<br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>PERSONAS QUE PRESENCIARON EL ACCIDENTE</h6>
              </div>
            </div>  
            {
                  this.state.tes.map((te, i) => {
                      return (
                          <div className="row" key={te.idtestigos}>
                              <div className="col-lg-4 izqq">
                              <b>Nombre: </b>{te.tes_nombre}
                              </div>
                              <div className="col-lg-4 izqq">
                              <b>Cargo: </b>{te.tes_cargo}
                              </div>    
                              <div className="col-lg-4 izqq">
                              <b>Documento: </b>{te.tes_tipodoc} {te.tes_documento}<br></br><br></br>
                              </div>
                          </div>
                      )
                  })
            }
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>PERSONA RESPONSABLE DEL INFORME</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Apellidos y Nombres completos: </b>{sin.sin_responsable}
              </div> 
              <div className="col-lg-6 izqq">
                <b>Cargo: </b>{sin.sin_cargoresponsable}
              </div>    
            </div>   
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Documento: </b>{sin.sin_documentoresponsable}<br></br>
              </div>
              <div className="col-lg-6 izqq">
                <b>Fecha de diligenciamiento del informe: </b><Moment format="DD MMM YYYY">{sin.sin_fechadiagnostico}</Moment><br></br>
              </div>    
            </div>
        </div>
            )
        })
    }

      </div>
    </div>
        
                <Footer></Footer>
            </div>
        );
    }
}
export default furat;