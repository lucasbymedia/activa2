import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class furel extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    idc = null;
    state = {
        sin:[],
        fac1: [],
        mon1: [],
        status: null
    };
    componentDidMount() {
        this.idc=this.props.match.params.id;
        console.log(this.idc);
        axios.get(global.url + "vista_siniestros/Muestra/" + this.idc ).then((res) => {
            const sin = res.data;
            console.log(res.data);
            this.setState({ sin });
          });

          axios.get(global.url + "factoresrs/Mostrarsin/" + this.idc).then((res) => {
            if (res.data) {
                const fac1 = res.data;
                this.setState({ fac1 });
            }
        });

        axios.get(global.url + "monitoreos/Mostrarsin/" + this.idc).then((res) => {
            if (res.data) {
                const mon1 = res.data;
                this.setState({ mon1 });
            }
        });
    }

    currencyFormat(num) {
      return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    render() {
      if(!cookies.get("idroles"))
      {
          return <Redirect to="./"/>;
      }

        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
    <div className="am-pagetitle">
      <h5 className="am-title">Enfermedad Laboral</h5>
    </div>



    <div className="am-mainpanel">
      <div className="am-pagebody">
      {
                this.state.sin.map((sin, i) => {
                    return (
        <div className="card pd-20 pd-sm-40">
            <div className="row">
              <div className="col-lg-12">
                <h2 className="card-body-title">INFORME DE ENFERMEDAD LABORAL DEL EMPLEADOR O CONTRATANTE</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg izqq"><b>Nombre:</b> {sin.per_primernombre} {sin.per_segundonombre} 
              {sin.per_primerapellido} {sin.per_segundoapellido}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>Documento:</b> {sin.per_numeroid}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>Empresa:</b> {sin.emp_nombre}</div>
              <div className="col-lg mg-t-10 mg-lg-t-0 izqq"><b>NIT:</b> {sin.emp_nit}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                  <h2 className="card-body-title">Afiliaciones</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4 izqq">
                <b>EPS del Afiliado:</b> {sin.eps_nombre}
              </div>
              <div className="col-lg-4 izqq">
                <b>ARL del Afiliado Código:</b> {sin.arl_nombre}
              </div>
              <div className="col-lg-4 izqq">
                <b>AFP del Afiliado Código:</b> {sin.afp_nombre}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12"><br></br>
                  <h2 className="card-body-title">Identificación General del empleador, contratante o cooperativa</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Tipo de vinculador laboral: </b>{sin.afi_tipovinculacion}<br></br><br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>SEDE PRINCIPAL</h6>
              </div>
            </div>   
            <div className="row">
              <div className="col-lg-8 izqq">
                <b>Nombre de la actividad económica: </b>{sin.emp_actividad} 
              </div>
              <div className="col-lg-4 izqq">
                <b>Código: </b>{sin.emp_codactividad}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Nombre Razón social: </b>{sin.emp_nombre}
              </div>
              <div className="col-lg-3 izqq">
                <b>Tipo de documento: </b>NIT
              </div>
              <div className="col-lg-4 izqq">
                <b>Número Identificación: </b>{sin.emp_nit}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.emp_direccion}
              </div>
              <div className="col-lg-6 izqq">
                <b>Zona: </b>Urbana<br></br><br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <h6 className='titul2'>CENTRO DE TRABAJO DONDE LABORA EL TRABAJADOR</h6>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-9 izqq">
                ¿SON LOS DATOS DEL CENTRO DE TRABAJO LOS MISMOS DE LA SEDE PRINCIPAL? <br></br>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-8 izqq">
                <b>Nombre de la actividad económica del Centro de Trabajo: </b>{sin.emp_actividad}
              </div>
              <div className="col-lg-4 izqq">
                <b>Código de actividad: </b>{sin.emp_codactividad}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.emp_direccion}
              </div>
              <div className="col-lg-2 izqq">
                <b>Zona: </b>Urbana<br></br><br></br>
              </div>
            </div>   
              <h2 className="card-body-title"> II. Información de la persona accidentada</h2>
            <div className="row">
              <div className="col-lg-6 izqq">
                <h6>Tipo de vinculación la laboral</h6>
              </div>
              <div className="col-lg-4 izqq">{sin.per_tipovinculacion}<br></br>
              </div>    
            </div>
            <div className="row">
              <div className="col-lg-3 izqq">
                <b>Primer Apellido: </b>{sin.per_primerapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Apellido: </b>{sin.per_segundoapellido}
              </div>
              <div className="col-lg-3 izqq">
                <b>Primer Nombre: </b>{sin.per_primernombre}
              </div>
              <div className="col-lg-3 izqq">
                <b>Segundo Nombre: </b>{sin.per_segundonombre}<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-3 izqq">
                <b>Tipo identificación: </b>CC<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Documento: </b>{sin.per_numeroid}<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Nacimiento: </b><Moment format="DD MMM YYYY">{sin.per_nacimiento }</Moment><br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Género: </b>{sin.per_genero }<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-6 izqq">
                <b>Dirección: </b>{sin.per_direccion }<br></br>
            </div>
            <div className="col-lg-3 izqq">
                <b>Teléfono: </b>{sin.per_telefono }<br></br>
            </div>
            <div className="col-lg-3 izqq">
                <b>Fax: </b><br></br><br></br>
            </div>
          </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                  <b>Departamento: </b>{sin.dep_nombre }<br></br>
              </div>
              <div className="col-lg-5 izqq">
                  <b>Municipio: </b>{sin.mun_nombre }<br></br>
              </div>
              <div className="col-lg-2 izqq">
                  <b>Zona: </b>Urbana<br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                  <b>Cargo que desempeña: </b>{sin.per_cargo }<br></br>
              </div>
              <div className="col-lg-5 izqq">
                  <b>Ocupación Habitual: </b><br></br>
              </div>
              <div className="col-lg-2 izqq">
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Tiempo de ocupación al momento del accidente: </b>{sin.sin_tiempolaborprev }<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Fecha de ingreso: </b><Moment format="DD MMM YYYY">{sin.afi_fechaafiliacion }</Moment><br></br>
              </div>
            </div>    
            <div className="row">
              <div className="col-lg-5 izqq">
                <b>Salario u horonarios: </b>{this.currencyFormat(sin.afi_ibc)}<br></br>
              </div>
              <div className="col-lg-3 izqq">
                <b>Jornada: </b>Diurna<br></br><br></br>
              </div>
            </div>   
            <h2 className="card-body-title">III. INFORMACIÓN DE LA ENFERMEDAD</h2>  
          <div className="row">
            <div className="col-lg-7 izqq">
              <b>Diagnóstico: </b>{sin.sin_diagnostico }<br></br>
            </div>
            <div className="col-lg-5 izqq">
              <b>Código diagnóstico: </b>{sin.sin_codigo }<br></br>
            </div>  
          </div>
          <div className="row">
            <div className="col-lg-9 izqq">
              <b>Diagnosticada por: </b>{sin.sin_diagnosticadapor }<br></br>
            </div>    
          </div>
          <div className="row">
            <div className="col-lg-4 izqq">
              <b>Apellidos y Nombres: </b>{sin.sin_nombremedico }<br></br>
            </div>
            <div className="col-lg-4 izqq">
              <b>Registro médico: </b>{sin.sin_regmedico }<br></br>
            </div>
            <div className="col-lg-4 izqq">
              <b>Fecha del diagnóstico: </b><Moment format="MMM DD">{sin.sin_fechadiagnostico }</Moment><br></br>
            </div>
          </div>  
          <div className="row">
            <div className="col-lg-4 izqq">
              <b>¿Causó la muerte al trabajador? </b>{sin.sin_muerte }<br></br>
            </div>
            <div className="col-lg-4 izqq">
              <b>Fecha de la muerte: </b><br></br><br></br>
            </div>
          </div>    
            <h2 className="card-body-title">IV. INFORMACIÓN SOBRE FACTORES DE RIESGO ASOCIADOS CON LA ENFERMEDAD</h2>
          <div className="row">
            <div className="col-lg-12"><br></br>
              <table cellspacing="4" cellpadding="4" border="1">
                <tr className="titul2">
                  <td>FACTOR DE RIESGO</td><td>¿CUÁLES FACTORES?</td><td>TIEMPO EXPOSICIÓN ACTUAl</td><td>TIEMPO EXPOSICIÓN ANTERIOR</td>
                </tr>
                {
                  this.state.fac1.map((fac, i) => {
                      return (
                        <tr key={i}>
                        <td>{fac.fac_clase}</td>
                        <td>{fac.fac_descripcion}</td>
                        <td>{fac.fac_tiempoexpactual}</td>
                        <td>{fac.fac_tiempoexpanteri}</td>
                        </tr>
                      )
                  })
                }
              </table>
              <br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 izqq">
              <b>DESCRIPCION DE LA ACTIVIDAD LABORAL DESEMPEÑADA (NEXO LABORAL): </b>{sin.sin_descripcion }<br></br><br></br>
            </div>
          </div>
            <h2 className="card-body-title">V. Información de evaluaciones médicas</h2>
          <div className="row">
            <div className="col-lg-4 izqq">
              <b>¿Evaluación MD Pre-Ocupacional? </b>{sin.sin_evalmedicapre }<br></br>
            </div>
            <div className="col-lg-4 izqq">
              <b>¿Evaluación MD Periódica Ocupacional? </b>{sin.sin_evalmedicaper }<br></br>
            </div>
            <div className="col-lg-4 izqq">
              <b>¿Evaluación de egreso? </b>{sin.sin_evalmedicaegre }<br></br><br></br>
            </div>
          </div>
            <h2 className="card-body-title">VI. Monitoreos relacionados</h2>
          <div className="row">
            <div className="col-lg-12">
              <table cellspacing="4" cellpadding="4" border="1">
                <tr className="titul2">
                  <td>MONITOREO DEL RIESGO</td>
                  <td width="15%">SI / NO</td>
                  <td>TIPO - CUÁLES - RESULTADO</td>
                  <td>FECHA</td>
                </tr>
                {
                  this.state.mon1.map((mon, i) => {
                      return (
                <tr key={i}>
                  {
                    mon.tipos_idtipos_monitoreo===1 ? 
                    ( <td>Mediciones ambientales</td> ) :
                    mon.tipos_idtipos_monitoreo===2 ? 
                    ( <td>Indicadores biológicos</td> ) :
                    mon.tipos_idtipos_monitoreo===3 ? 
                    ( <td>Estudio de puesto de trabajo</td> ) :
                    ( <td>Otros</td> )
                  }
                  <td>{mon.mon_respuesta}</td>
                  <td>{mon.mon_resultado}</td>
                  <td>{mon.mon_fecha}</td>
                </tr>
                      )
                  })
                }
              </table>
              <br></br>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <h6 className="titul2">PERSONA RESPONSABLE DEL INFORME</h6><br></br>
            </div>
          </div>    
          <div className="row">
            <div className="col-lg-6 izqq">
              <b>Apellidos y Nombres completos: </b>{sin.sin_responsable }<br></br>
            </div>    
            <div className="col-lg-6 izqq">
              <b>Cargo: </b>{sin.sin_cargoresponsable }<br></br>
            </div>    
          </div>
          <div className="row">
            <div className="col-lg-6 izqq">
              <b>Documento: </b>{sin.sin_documentoresponsable }<br></br>
            </div>
            <div className="col-lg-6 izqq">
              <b>Fecha de diligenciamiento del informe: </b><Moment format="DD MMM YYYY">{sin.sin_fechadiagnostico}</Moment><br></br>
            </div>    
          </div>


        </div>
            )
        })
    }

      </div>
    </div>
        
                <Footer></Footer>
            </div>
        );
    }
}
export default furel;