import React, { Component } from 'react';
import Header from './Header';
import Menulat from './Menulat';
import Footer from './Footer';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';
import global from '../Global';
import Moment from 'react-moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies(); 

class revisar_autorizaciones extends Component {
    param1 = React.createRef(); 
    param2 = React.createRef(); 
    param3 = React.createRef(); 
    state = {
        tabe:[],
        status: null
    };
    componentDidMount() {
        this.consultas();
    }

    consultas = () => {
        var fec1=this.param1.current.value;
        if(fec1===""){
            fec1 = new Date().toJSON().slice(0,4).replace(/-/g,'-')+"-01-01";
        }
        var fec2=this.param2.current.value;
        if(fec2===""){
            fec2 = new Date().toJSON().slice(0,4).replace(/-/g,'-')+"-12-31";
        }
        var ced=this.param3.current.value;
        if(ced===""){ced="0";}

        axios.get(global.url + "vista_autorizaciones/consulta_fechas"
        + "/" + fec1 + "/" + fec2 + "/" + ced + "/1").then((res) => {
              const tabe = res.data;
            this.setState({ tabe });
        });
    }

    currencyFormat(num) {
        if(num===null){ return '$ 0'; }
        return '$' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }


    render() {
      if(cookies.get("idroles")!=="1" && cookies.get("idroles")!=="3")
      {
          return <Redirect to="./"/>;
      }

        return (
            <div>
                <Header></Header>
                <Menulat></Menulat>
      <div className="am-pagetitle">
      <h5 className="am-title">Autorizaciones</h5>
      <form id="searchBar" className="search-bar" action="inicio">
        <div className="form-control-wrapper">
          <input
            type="search" className="form-control bd-0"
            placeholder="Buscar..."/>
        </div>
        <button id="searchBtn" className="btn btn-orange">
          <i className="fa fa-search" ></i>
        </button>
      </form>
    </div>

    <div className="am-mainpanel">
      <div className="am-pagebody">
        <div className="card pd-20 pd-sm-40">
          <h6 className="card-body-title">Revisión de autorizaciones</h6>

          <form name="forma">
            <div className="row">
                <div className="col-lg-3 izqq">Fecha incial
                    <input ref={this.param1} className="form-control" type="date"/>
                </div>
                <div className="col-lg-3 izqq">Fecha final
                    <input ref={this.param2} className="form-control" type="date"/>
                </div>
                <div className="col-lg-3 izqq">Identificación
                    <input ref={this.param3} className="form-control" type="text"/>
                </div>
                <div className="col-lg-3"><br></br>
                    <input onClick={this.consultas} type="button" className="btn btn-info pd-x-20" value="Consultar" />
                </div>
            </div>
            <br></br>
          </form>

          <div className="table-wrapper" id="tablas">
            <table id="datatable1" className="table display responsive nowrap">
              <thead>
                <tr>
                  <th>Afiliado</th><th className="wd-20p">Servicio</th><th>Fecha</th>
                  <th>Municipio</th><th>IPS</th> <th className="wd-20p">Valor reserva</th>
                  <th className="wd-20p">Valor autorizaciones</th><th>Evaluar</th>
                </tr>
              </thead>
              <tbody>
              {
                this.state.tabe.map((tab, i) => {
                    return (
                        <tr key={tab.idautorizaciones}>
                            <td>{ tab.per_primernombre } { tab.per_primerapellido }</td>
                            <td>{ tab.ser_descripcion }</td><td><Moment format="DD MMM">{ tab.sin_fecha }</Moment></td>
                            <td>{tab.mun_nombre}</td><td>{tab.ips_nombre}</td>
                            <td>{this.currencyFormat(tab.valor_reservas)}</td>
                            <td>{this.currencyFormat(tab.valor_servicios)}</td>
                            <td align="center">
                                {
                                    <NavLink to={"/evaluar_autorizacion/" + tab.idautorizaciones}>Acciones</NavLink>
                                }
                            </td>
                        </tr>
                    )
                })
            }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
                <Footer></Footer>
            </div>
        );
    }
}
export default revisar_autorizaciones;