import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Headercomponent from './components/Headercomponent.vue';
import Errorcomponent from './components/Errorcomponent.vue';
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

import VueSimpleAccordion from 'vue-simple-accordion';
import 'vue-simple-accordion/dist/vue-simple-accordion.css';

import login from './components/login.vue';
import inicio from './components/inicio.vue';
//import store from './store'

import roles from './components/roles.vue';
import departamentos from './components/Departamentos.vue';
import ciudades from './components/Ciudades.vue';
import usuarios from './components/usuarios.vue';
import ips from './components/ips.vue';
import arl from './components/arl.vue';
import eps from './components/eps.vue';
import factores from './components/factores.vue';
import tipos_monitoreo from './components/tipos_monitoreo.vue';
import servicios from './components/servicios.vue';
import diagnosticos from './components/diagnosticos.vue';
import empresas from './components/empresas.vue';
import personas from './components/personas.vue';

import accidente_laboral from './components/accidente_laboral.vue';
import enfermedad_laboral from './components/enfermedad_laboral.vue';
import radicacion from './components/radicacion.vue';
import consulta from './components/consulta.vue';
import evaluacion from './components/evaluacion.vue';

import editarrol from './components/Editarrol.vue';
import editaciudad from './components/EditarCiudad.vue';
import editadpto from './components/editadpto.vue';
import editauser from './components/editauser.vue';
import editaips from './components/editaips.vue';
import editaarl from './components/editaarl.vue';
import editaeps from './components/editaeps.vue';
import editafactores from './components/editafactores.vue';
import editatmonitoreo from './components/editatmonitoreo.vue';
import editaservicios from './components/editatservicios.vue';
import editadiagnosticos from './components/editadiagnosticos.vue';
import editaempresa from './components/editaempresa.vue';
import editapersona from './components/editapersona.vue';

import crearrol from './components/crearrol.vue';
import creardpto from './components/creardpto.vue';
import creaciudad from './components/CrearCiudad.vue';
import crearuser from './components/crearuser.vue';
import crearips from './components/crearips.vue';
import creararl from './components/creararl.vue';
import creareps from './components/creareps.vue';
import crearfactores from './components/crearfactores.vue';
import creartmonitoreo from './components/creartmonitoreo.vue';
import crearservicio from './components/crearservicio.vue';
import creardiagnostico from './components/creardiagnostico.vue';
import crearempresa from './components/crearempresa.vue';
import crearpersona from './components/crearpersona.vue';

Vue.config.productionTip = false

Vue.use(VueSidebarMenu);
Vue.use(VueRouter);
Vue.use(VueSimpleAccordion);

const moment = require('moment');
require('moment/locale/es')
Vue.use(require('vue-moment'), {moment});

const routes = [
  {path:'/home', name:'home', component: Headercomponent, meta: {administrador:true,consulta:true,gestion:true}},
  {path:'/roles', component: roles, meta: {administrador:true}},
  {path:'/ciudades', component: ciudades, meta: {administrador:true}},
  {path:'/departamentos', component: departamentos, meta: {administrador:true}},
  {path:'/usuarios', component: usuarios, meta: {administrador:true}},

  {path:'/ips', component: ips, meta: {administrador:true}},
  {path:'/arl', component: arl, meta: {administrador:true}},
  {path:'/eps', component: eps, meta: {administrador:true}},
  {path:'/factores', component: factores, meta: {administrador:true}},
  {path:'/tipos_monitoreo', component: tipos_monitoreo, meta: {administrador:true}},
  {path:'/servicios', component: servicios, meta: {administrador:true}},
  {path:'/diagnosticos', component: diagnosticos, meta: {administrador:true}},
  {path:'/empresas', component: empresas, meta: {administrador:true}},
  {path:'/personas', component: personas, meta: {administrador:true}},

  {path:'/accidente_laboral', component: accidente_laboral, meta: {administrador:true,gestion:true}},
  {path:'/enfermedad_laboral', component: enfermedad_laboral, meta: {administrador:true,gestion:true}},
  {path:'/radicacion', component: radicacion, meta: {administrador:true,gestion:true}},
  {path:'/consulta', component: consulta, meta: {administrador:true,gestion:true}},
  {path:'/evaluacion', component: evaluacion, meta: {administrador:true,gestion:true}},

  {path:'/editarrol/:id', name:'editarrol', component: editarrol, meta: {administrador:true}},
  {path:'/creaarrol', name:'crearrol', component: crearrol, meta: {administrador:true}},

  {path:'/editaciudad/:id', name:'editaciudad', component: editaciudad, meta: {administrador:true}},
  {path:'/crearciudad', name:'crearciudad', component: creaciudad, meta: {administrador:true}},

  {path:'/editadpto/:id', name:'editadpto', component: editadpto, meta: {administrador:true}},
  {path:'/creardpto', name:'creardpto', component: creardpto, meta: {administrador:true}},

  {path:'/editauser/:id', name:'editauser', component: editauser, meta: {administrador:true}},
  {path:'/crearuser', name:'crearuser', component: crearuser, meta: {administrador:true}},
  
  {path:'/editaips/:id', name:'editaips', component: editaips, meta: {administrador:true}},
  {path:'/crearips', name:'crearips', component: crearips, meta: {administrador:true}},

  {path:'/editaarl/:id', name:'editaarl', component: editaarl, meta: {administrador:true}},
  {path:'/creararl', name:'creararl', component: creararl, meta: {administrador:true}},

  {path:'/editaeps/:id', name:'editaeps', component: editaeps, meta: {administrador:true}},
  {path:'/creareps', name:'creareps', component: creareps, meta: {administrador:true}},

  {path:'/editafactores/:id', name:'editafactores', component: editafactores, meta: {administrador:true}},
  {path:'/crearfactores', name:'crearfactores', component: crearfactores, meta: {administrador:true}},
  
  {path:'/editatmonitoreo/:id', name:'editatmonitoreo', component: editatmonitoreo, meta: {administrador:true}},
  {path:'/creartmonitoreo', name:'creartmonitoreo', component: creartmonitoreo, meta: {administrador:true}},

  {path:'/editaservicios/:id', name:'editaservicios', component: editaservicios, meta: {administrador:true}},
  {path:'/crearservicio', name:'crearservicio', component: crearservicio, meta: {administrador:true}},

  {path:'/editadiagnosticos/:id', name:'editadiagnosticos', component: editadiagnosticos, meta: {administrador:true}},
  {path:'/creardiagnostico', name:'creardiagnostico', component: creardiagnostico, meta: {administrador:true}},

  {path:'/editaempresa/:id', name:'editaempresa', component: editaempresa, meta: {administrador:true}},
  {path:'/crearempresa', name:'crearempresa', component: crearempresa, meta: {administrador:true}},

  {path:'/editapersona/:id', name:'editapersona', component: editapersona, meta:{administrador:true}},
  {path:'/crearpersona', name:'crearpersona', component: crearpersona, meta:{administrador:true}},
    
  {path:'/login', name:'login', component: login, meta:{libre:true}},
  {path:'/inicio',name:'inicio', component: inicio, meta: {administrador:true,consulta:true,gestion:true}},


  {path:'*', component: Errorcomponent}
];

const router = new VueRouter({
  routes,
  mode:"history",
  base: process.env.BASE_URL

});
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

/*
router.beforeEach((to, from, next) => {
/*
  if(to.matched.some(record => record.meta.libre)){
    next()
  }
/*  else if(store.state.usuario && store.state.usuario.roles_idroles=='1')
  {
    if(to.matched.some(record => record.meta.administrador)){
      next()
    }
  } 
  else if(store.state.usuario && store.state.usuario.roles_idroles=='2')
  {
    if(to.matched.some(record => record.meta.consulta)){
      next()
    }
  }
  else if(store.state.usuario && store.state.usuario.roles_idroles=='3')
  {
    if(to.matched.some(record => record.meta.gestion)){
      next()
    }
  }
  else {
    next({name: 'login'})
 }

})*/
