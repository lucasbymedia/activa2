class empresa {
    constructor(ciudades_idciudades, arl_idarl, emp_nombre, emp_nit, emp_email, emp_telefono, 
        emp_representante, emp_direccion, emp_actividad, emp_codactividad) {
        this.ciudades_idciudades = ciudades_idciudades;
        this.arl_idarl = arl_idarl;
        this.emp_nombre = emp_nombre;
        this.emp_nit = emp_nit;
        this.emp_email = emp_email;
        this.emp_telefono = emp_telefono;
        this.emp_representante = emp_representante;
        this.emp_direccion = emp_direccion;
        this.emp_actividad = emp_actividad;
        this.emp_codactividad = emp_codactividad;
    }
}

export default empresa;