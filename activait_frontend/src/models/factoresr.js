class factoresr {
    constructor(factores_idfactores_riesgo, fac_clase, fac_descripcion, fac_tiempoexpactual, fac_tiempoexpanteri, siniestros_idsiniestros ) {
        this.factores_idfactores_riesgo = factores_idfactores_riesgo;
        this.siniestros_idsiniestros = siniestros_idsiniestros;
        this.fac_clase = fac_clase;
        this.fac_descripcion = fac_descripcion;
        this.fac_tiempoexpactual = fac_tiempoexpactual;
        this.fac_tiempoexpanteri = fac_tiempoexpanteri;
    }
}
export default factoresr;