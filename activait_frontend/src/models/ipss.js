class ips {
    constructor(ciudades_idciudades,  ips_nit, ips_nombre, ips_contacto, ips_direccion, ips_telefono, ips_naturaleza ) {
        this.ciudades_idciudades=ciudades_idciudades;
        this.ips_nit =ips_nit;
        this.ips_nombre =ips_nombre;
        this.ips_contacto =ips_contacto;
        this.ips_direccion =ips_direccion;
        this.ips_telefono =ips_telefono;
        this.ips_naturaleza =ips_naturaleza;
    }
}

export default ips;