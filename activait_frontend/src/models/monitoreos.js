class factoresr {
    constructor(siniestros_idsiniestros, tipos_idtipos_monitoreo, mon_respuesta, mon_resultado, mon_fecha ) {
        this.tipos_idtipos_monitoreo = tipos_idtipos_monitoreo;
        this.siniestros_idsiniestros = siniestros_idsiniestros;
        this.mon_respuesta = mon_respuesta;
        this.mon_resultado = mon_resultado;
        this.mon_fecha = mon_fecha;
    }
}
export default factoresr;