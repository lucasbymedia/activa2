class empresa {
    constructor(idpersonas, empresas_idempresas, afps_idafps, per_tipoid, per_numeroid, per_primernombre, per_segundonombre,
        per_primerapellido, per_segundoapellido, per_nacimiento, per_genero) {
        this.idpersonas = idpersonas;
        this.empresas_idempresas = empresas_idempresas;
        this.afps_idafps = afps_idafps;
        this.per_tipoid = per_tipoid;
        this.per_numeroid = per_numeroid;
        this.per_primernombre = per_primernombre;
        this.per_segundonombre = per_segundonombre;
        this.per_primerapellido = per_primerapellido;
        this.per_segundoapellido = per_segundoapellido;
        this.per_nacimiento = per_nacimiento;
        this.per_genero = per_genero;
    }
}

export default empresa;