class serv {
    constructor(ser_codigo, ser_descripcion, ser_cups, ser_cum, ser_estado, ser_prioridad, ser_pbs) {
        this.ser_codigo = ser_codigo;
        this.ser_descripcion = ser_descripcion;
        this.ser_cups = ser_cups;
        this.ser_cum = ser_cum;
        this.ser_estado = ser_estado;
        this.ser_prioridad = ser_prioridad;
        this.ser_pbs = ser_pbs;
    }
}
export default serv;