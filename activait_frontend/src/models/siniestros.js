class siniestros {
    constructor(personas_idpersonas, ciudades_idciudades, sin_tipoatep, sin_fecha,
        sin_jornada, sin_laboral, sin_otra, sin_codigo, sin_tiempolaborprev, sin_causal, sin_muerte, sin_sitio, 
        sin_lugar, sin_tipolesion, sin_parteafectada, sin_agente, sin_mecanismo, sin_descripcion, sin_responsable,
        sin_nombreresponsable, sin_cargoresponsable, sin_documentoresponsable, sin_fechadiagnostico, 
        sin_diagnosticadapor, sin_diagnostico, sin_regmedico, sin_nombremedico, sin_evalmedicapre, 
        sin_evalmedicaper, sin_evalmedicaegre, si1_hors, si1_mins, sin_tiempolaborprev1, sin_zona, sin_estado) {
            
            personas_idpersonas = this.personas_idpersonas;
            ciudades_idciudades = this.ciudades_idciudades;
            sin_fecha = this.sin_fecha;
            sin_tipoatep = this.sin_tipoatep;
            sin_jornada = this.sin_jornada;
            sin_laboral = this.sin_laboral; 
            sin_otra = this.sin_otra;
            sin_codigo = this.sin_codigo;
            sin_tiempolaborprev = this.sin_tiempolaborprev; 
            sin_tiempolaborprev1 = this.sin_tiempolaborprev1; 
            sin_causal = this.sin_causal;            
            sin_muerte= this.sin_muerte; 
            sin_tipolesion= this.sin_tipolesion;
            sin_parteafectada= this.sin_parteafectada;
            sin_agente= this.sin_agente;
            sin_lugar= this.sin_lugar;
            sin_mecanismo= this.sin_mecanismo;
            sin_descripcion= this.sin_descripcion; 
            sin_responsable= this.sin_responsable;
            sin_nombreresponsable= this.sin_nombreresponsable;
            sin_cargoresponsable= this.sin_cargoresponsable;
            sin_documentoresponsable= this.sin_documentoresponsable;
            sin_fechadiagnostico= this.sin_fechadiagnostico;
            sin_diagnosticadapor= this.sin_mecanismo;
            sin_diagnostico= this.sin_diagnostico;
            sin_regmedico= this.sin_regmedico;
            sin_nombremedico= this.sin_nombremedico;
            sin_evalmedicapre= this.sin_evalmedicapre;
            sin_evalmedicaper= this.sin_evalmedicaper;
            sin_evalmedicaegre= this.sin_evalmedicaegre;
            sin_zona=this.sin_zona;

            sin_sitio= this.sin_sitio;
            si1_hors=this.si1_hors;
            si1_mins=this.si1_mins;

            sin_estado = this.sin_estado;

    }
}

export default siniestros;


