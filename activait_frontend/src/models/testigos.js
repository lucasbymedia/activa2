class testigos {
    constructor(siniestros_idsiniestros, tes_tipodoc, tes_documento, tes_nombre, tes_cargo ) {
        this.siniestros_idsiniestros = siniestros_idsiniestros;
        this.tes_tipodoc = tes_tipodoc;
        this.tes_documento = tes_documento;
        this.tes_nombre = tes_nombre;
        this.tes_cargo = tes_cargo;
    }
}
export default testigos;