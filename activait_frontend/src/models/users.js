class user {
    constructor(roles_idroles, usu_nombre,  usu_email, password, usu_documento, usu_telefono ) {
        this.roles_idroles=roles_idroles;
        this.usu_nombre=usu_nombre;
        this.usu_email =usu_email;
        this.usu_documento =usu_documento;
        this.usu_telefono =usu_telefono;
        this.password = password;
    }
}

export default user;