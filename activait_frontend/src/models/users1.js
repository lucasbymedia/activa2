class user {
    constructor(idusuarios, roles_idroles, usu_nombre,  usu_email, password, usu_documento, usu_telefono, act_password ) {
        this.idusuarios=idusuarios;
        this.roles_idroles=roles_idroles;
        this.usu_nombre=usu_nombre;
        this.usu_email =usu_email;
        this.password = password;
        this.usu_documento =usu_documento;
        this.usu_telefono =usu_telefono;
        this.act_password =act_password;
    }
}

export default user;