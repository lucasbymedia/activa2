import Vue from 'vue'
import decode from 'jwt-decode'
import router from 'vue-router'

Vue.use(Vue);

export default new Vue.Store({
    state:{
        token: null,
        usuario: null
    },
    mutations: {
        setToken(state, token){
            state.token=token
        },
        setUsuario(state, usuario){
            state.usuario=usuario
        }
    },
    actions: {
        guardarToken({commit}, token){
            commit("setToken", token)
            commit("setUsuario", decode(token))
            localStorage.setItem("token", token)
        },
        autologin({commit}){
            let token = localStorage.getItem("token")
            if(token){
                commit("setToken", token)
                commit("setUsuario", decode(token))
            }
            router.push({name:'inicio'})

        },
        salir({commit}){
            commit("setToken", null)
            commit("setUsuario", null)
            router.push({name: 'login'})
        }

    }
})