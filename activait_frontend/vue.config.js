module.exports = {
    devServer: {
      proxy: {
        '/api': {
          target: 'https://localhost:4443',
          ws: true,
          changeOrigin: true
        }
      }
    }
  }
  
    